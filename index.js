import http from 'http';
import express from 'express';
import path from 'path';
import multer from 'multer';
import RoutesV1 from './src/routes/RoutesV1';
import CronJobs from './src/utils/CronJobs'
import moment from 'moment';
import { deleteTmpCreateTmp } from './src/utils/FileFunctions';
import mySocket, { getSocketConnectionCount } from './src/utils/SocketActions';
import DbClients from './src/db/mysql/DbClients';
import { sendDBConnectionLost } from './src/utils/Mailer';
import dotenv from 'dotenv';
dotenv.config()


const fs = require("fs");

const cors = require('cors');

// require('dotenv').config();


const hostname = process.env.HOST;
const port = process.env.PORT;

const app = express()

app.use(cors());

app.use('/public', express.static(path.join(__dirname, 'public')));
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/test-email', (req, res) => {
    sendDBConnectionLost('test');
    res.status(200).send("Sent Email")
})

app.post('/uploadtest', (req, res) => { // upload.single('avatar'),
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'public/uploads/')
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    })
    const fileFilter = (req, file, cb) => {
        if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
            cb(null, true);
        } else {
            cb(new Error('Only image files are allowed!'), false);
        }
    }
    const upload = multer({ storage: storage, fileFilter: fileFilter }).single('avatar');
    upload(req, res, function (err) {
        if (req.fileValidationError) {
            return res.send(req.fileValidationError);
        } else if (!req.file) {
            return res.send('Please select an image to upload');
        } else if (err instanceof multer.MulterError) {
            return res.send(err);
        } else if (err) {
            return res.send(err);
        }
        res.send(`You have uploaded this image: <hr/><img src="${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);
    })
})

RoutesV1(app);

app.get("/file", async (req, res) => {
    let path = 'public/tmp/' + req.query.fileName;
    if (fs.existsSync(path)) {
        const file = fs.createReadStream(path);
        file.pipe(res);
    } else {
        res.status(404).send("File not Found")
    }
});

app.get("/delete_tmp", async (req, res) => {
    deleteTmpCreateTmp();
    res.status(401).send("File not Found")
});

const mom = moment().format('DD-MM-YYYY HH:mm');

app.get('/', async (req, res) => {
    let result = await getSocketConnectionCount();
    let resultTimeZone = await DbClients.queryOne({ sidClient: 198,  },
        `SELECT @@system_time_zone AS timeZone, 
    @@session.time_zone AS sessionTimeZone, 
    @@global.time_zone AS globalTimeZone, 
    NOW() as currentTime;`)
    let resultcConCount = await DbClients.queryOne({ sidClient: 198,  },
        `show status where variable_name = 'threads_connected'`)
    res.status(200).send(`
            <!DOCTYPE html>
            <html>
                <head>
                    <style>
                        table, th, td {
                        border: 1px solid black;
                        font-size: 12px;
                        padding: 10px;
                        }
                    </style>
                </head>
                <body>
                    <h3>JSOURCE AccountingV6 Server Is Running...</h3>
                    <table>
                        <tr>
                            <th style="width:400px; aligment: center;" >Description</th>
                            <th style="width:300px;">Value</th>
                        </tr>
                        <tr>
                            <td>
                                Server Start Time
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${mom}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Server Version
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    1.22-07-2022
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Server System TimeZone
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(resultTimeZone.success && resultTimeZone.data.timeZone) || ''}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Server Session TimeZone
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(resultTimeZone.success && resultTimeZone.data.sessionTimeZone) || ''}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Server Global TimeZone
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(resultTimeZone.success && resultTimeZone.data.globalTimeZone) || ''}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Server Current TimeZone
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(resultTimeZone.success && resultTimeZone.data.currentTime) || ''}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Socket Client Connected Count
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(result && result.length) || 0}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                MySQL Connected Count
                            </td>
                            <td style=text-align:center;>
                                <strong>
                                    ${(resultcConCount.success && resultcConCount.data.Value) || 0}
                                </strong>
                            </td>
                        </tr>
                    </table>
                </body>
            </html>
    `)
});

app.use((req, res) => {
    res.status(404).send({ success: false, errMsg: "URL not Found" })
});

CronJobs.runAllTasks();

const server = http.createServer(app);
server.listen(port, hostname, () => {
    console.warn(`Server running at http://${hostname}:${port}/        ${mom}`);
});

mySocket()