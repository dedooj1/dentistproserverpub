const LANGS = [
    "en",
    "hy",
    "ar",
];

export default function getText(userLang,textKey) {
    if (!userLang || !LANGS.includes(userLang)) {
        userLang = "en";
    }
    const langData = module.require(`./${userLang}`).default;
    let str = langData[textKey];
    return str ? str : "<" + textKey + ">";
}