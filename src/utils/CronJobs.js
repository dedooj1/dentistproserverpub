import cron from 'node-cron';
import DbClients from '../db/mysql/DbClients';
import DbUserToken from '../db/mysql/DbUserToken';
import { deleteTmpCreateTmp } from './FileFunctions';

class CronJobs {
    // constructor() {

    // }

    runAllTasks() {
        setTimeout(() => {
            // this.checkAllClientsActiveUntilDate({}); // every day at 00:10
            this.clearExpiredTokens({}); // every day at 00:20
            this.deleteTmpCreateTmp(); // every day at 00:30
            this.backUpDatabases(); // every day at 00:40
        }, 5000);
    }


    // every day at 20:10 UTC / 00:10 GTM +04
    // checkAllClientsActiveUntilDate(params) {
    //     const task = cron.schedule('10 20 * * *', () => {
            // DbClients.checkAllClientsActiveUntilDate(params);
    //     });
    //     task.start();
    // }
    // every day at 20:20 UTC / 00:20 GTM +04
    clearExpiredTokens(params) {
        const task = cron.schedule('20 20 * * *', () => {
            DbUserToken.clearExpiredTokens(params);
        });
        task.start();
    }

    // every day at 20:30 UTC / 00:30 GTM +04
    deleteTmpCreateTmp() {
        const task = cron.schedule('30 20 * * *', () => {
            deleteTmpCreateTmp();
        });
        task.start();
    }

    backUpDatabases() {
        console.warn('Started Cron Database Backup');
        const task = cron.schedule('40 20 * * *', () => {
            // let result = await 
            DbClients.backUpDatabases({});

        });
        task.start();
    }

}
export default new CronJobs;