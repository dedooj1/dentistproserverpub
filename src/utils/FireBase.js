import firebase from 'firebase-admin';

let serviceAccount = require("../../src/utils/ubicrossdelivery-firebase-adminsdk-ei993-5ff245d8db.json");
firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://ubicrossdelivery.firebaseio.com"
});


function firebaseSendToDevice(registrationToken, message){
    const notification_options = {
        priority: "high",
        playSound: true,
        channelId: "UbicrossDelivery",
    };

    firebase.messaging().sendToDevice(registrationToken, message, notification_options)
        .then((response) => {
            //console.warn('Successfully sent message:', response);
        })
        .catch((error) => {
            //console.warn('Error sending message:', error);
        });
}

function firebaseSendMessageMulticast(registrationToken, obj){
    // console.warn(registrationToken);

    const title =  obj.hasOwnProperty("title") ?  obj.title : "";
    const body = obj.hasOwnProperty("body") ? obj.body : "";
    const message = {
        tokens: registrationToken,
        data: {
            title: title, 
            body: body,
        },
        notification: {
            title: title, 
            body: body,
        },
        apns: {
            payload: {
                aps: {
                    sound: "default"
                }
            }
        }
    }
    firebase.messaging().sendMulticast(message)
        .then((response) => {
            console.warn('Successfully sent message:', response);
        })
        .catch((error) => {
            console.warn('Error sending message:', error);
        });
}

function firebaseSendMessageTopic(obj){
    const topic = "Delivery";
    const title =  obj.hasOwnProperty("title") ?  obj.title : "";
    const body = obj.hasOwnProperty("body") ? obj.body : "";
    const message = {
        topic: topic,
        data: {
            title: title, 
            body: body,
        },
        notification: {
            title: title, 
            body: body,
        },
        apns: {
            payload: {
                aps: {
                    sound: "default"
                }
            }
        }
    }
    firebase.messaging().send(message)
        .then((response) => {
            //console.warn('Successfully sent message:', response);
        })
        .catch((error) => {
            //console.warn('Error sending message:', error);
        });
}

export function firebaseSendOrderAssign(registrationTokens, obj){
    let body = `#${obj.orderNumber}`;
    let title = "New Order";
    let message = {
        notification:{
            title: title,
            body: body,
            sound: "default",
        },
        data:{
            title: title,
            body: body,
            sound: "default"
        }
    };
    firebaseSendToDevice(registrationTokens, message);
}

// module.exports.firebase = firebase
// module.exports.firebase = 