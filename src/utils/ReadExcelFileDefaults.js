// import Excel from 'exceljs';
// import DbAccountProfiles from '../db/mysql/DbAccountProfiles';
// import DbAccounts from '../db/mysql/DbAccounts';

// const ReadExcelFileDefaults = {
//     defaultAccountIds: {
//         customers_161: 161,
//         supliers_261: 261,
//         stores_13: 13,
//         box_181 : 181,
//         idCurrencyLocal_1 : 1,
//         idCurrencyDollar_2 : 2,
//         discount_Buy : 4105,
//         discount_Sale : 3601,
//         tax_Buy : 371,
//     },
//     createAccountProfilesCustomer: async (params) => {
//         let saveData = {
//             proFullName: "زبون 1",
//             profileType: DbAccountProfiles.TYPE.CUSTOMER,
//             idAccParent: ReadExcelFileDefaults.defaultAccountIds.customers_161,
//             idCurrency: 1,
//             proTitle: DbAccountProfiles.TITLETYPE.MR,
//         }
//         params.saveData = saveData;
//         return await DbAccountProfiles.insert(params, true);
//     },
//     createAccountProfilesSuplier: async (params) => {
//         let saveData = {
//             proFullName: "مورد 1",
//             profileType: DbAccountProfiles.TYPE.SUPLIER,
//             idAccParent: ReadExcelFileDefaults.defaultAccountIds.supliers_261,
//             idCurrency: 1,
//             proTitle: DbAccountProfiles.TITLETYPE.COMPANY,
//         }
//         params.saveData = saveData;
//         return await DbAccountProfiles.insert(params, true);
//     },
//     createAccountProfilesStore: async (params) => {
//         let saveData = {
//             proFullName: "مخزن 1",
//             profileType: DbAccountProfiles.TYPE.STORE,
//             idAccParent: ReadExcelFileDefaults.defaultAccountIds.stores_13,
//             idCurrency: 1,
//         }
//         params.saveData = saveData;
//         return await DbAccountProfiles.insert(params, true);
//     },

//     importFromFileAccounts: async (params) => {
//         const workbook = new Excel.Workbook();
//         let dir = "public/Defaults/";
//         let fileName = `${dir}${params.fileName}`;
//         const result = await workbook.xlsx.readFile(fileName);
//         const worksheet = result.getWorksheet('Sheet 1');
//         let count = 0;
//         worksheet.eachRow({ includeEmpty: false }, async (row, rowNumber) => {
//             if (rowNumber != 1) {
//                 const obj = {
//                     idAcc: 0,
//                     accName: "",
//                     accNameEn: "",
//                     accNumber: "",
//                     accType: DbAccounts.TYPE.MAIN,
//                     idAccParent: 0,
//                     idCurrency: 0,
//                 };
//                 for (let i = 0; i < row.values.length; i++) {
//                     const val = row.values[i];
//                     switch (i) {
//                         case 1: // idAcc
//                             obj.idAcc = val;
//                             break;
//                         case 2: // accName
//                             obj.accName = val;
//                             break;
//                         case 3: // accNameEn
//                             obj.accNameEn = val || obj.accName || '';
//                             break;
//                         case 4: // idAccParent
//                             obj.idAccParent = val;
//                             break;
//                         case 5: // idCurrency
//                             obj.idCurrency = val;
//                             break;
//                         case 6: // accNumber
//                             obj.accNumber = val;
//                         case 7: // accType
//                             obj.accType = val ? DbAccounts.TYPE.MAIN : DbAccounts.TYPE.CHIELD;
//                         case 8: // accLevel
//                             obj.accLevel = val || 1;
//                         default:
//                             break;
//                     }
//                 }
//                 params.saveData = obj;
//                 count++;
//                 await DbAccounts.insert(params, false);
//             }
//         });
//         return { success: true, totalRows: count };
//     }
// }


// export default ReadExcelFileDefaults;


