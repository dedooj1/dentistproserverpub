import Excel from 'exceljs';
// import DbAccounts from '../db/mysql/DbAccounts';

export async function importFromFile(params) {
    const workbook = new Excel.Workbook();
    const result = await workbook.xlsx.readFile(__dirname + '/' + params.fileName);
    const worksheet = result.getWorksheet('Sheet 1');
    const list = [];
    worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
        if (rowNumber != 1) {
            const obj = {
                acc_id: 0,
                acc_name: '',
                acc_name_en: '',
                acc_parent_id: 0,
                acc_currency_id: 0,
                acc_sale_price_type_index: 0,
                acc_number: '',
                acc_balance_type_md: 0,
                acc_is_main: 0,
                acc_limit_amount: 0,
                acc_limit_type_md: 0,
                acc_limit_day: 0,
                acc_level_policy: 0,
                acc_is_active: 0,
                acc_final_account_id: 0,
                acc_address: '',
                acc_phone: '',
                acc_email: '',
                acc_note1: '',
                acc_note2: '',
                acc_note3: '',
                acc_markaz_id: 0,
                acc_mutabaka_amount: 0,
                acc_mutabaka_date: '',
                acc_discount_per: 0,
                acc_mandoub_id: 0,
                acc_mandoub_per: 0,
                acc_only_cash: 0,
                acc_week_days: 0,
                acc_markaz_ilzami: 0,
                acc_barcode: '',
                acc_budget: 0,
            };
            for (let i = 0; i < row.values.length; i++) {
                const val = row.values[i];
                switch (i) {
                    case 0:
                        break;
                    case 1: // acc_id
                        obj.acc_id = val;
                        break;
                    case 2: // acc_name
                        obj.acc_name = val;
                        break;
                    case 3: // acc_name_en
                        obj.acc_name_en = val ? val : obj.acc_name;
                        break;
                    case 4: // acc_parent_id
                        obj.acc_parent_id = val;
                        break;
                    case 5: // acc_currency_id
                        obj.acc_currency_id = val;
                        break;
                    case 6: // acc_sale_price_type_index
                        obj.acc_sale_price_type_index = val;
                    case 7: // acc_number
                        obj.acc_number = val;
                    case 8: // acc_balance_type_md
                        obj.acc_balance_type_md = val;
                    case 9: // acc_is_main
                        obj.acc_is_main = val;
                    case 10: // acc_limit_amount
                        obj.acc_limit_amount = val;
                    case 11: // acc_limit_type_md
                        obj.acc_limit_type_md = val;
                    case 12: // acc_limit_day
                        obj.acc_limit_day = val;
                    case 13: // acc_level_policy
                        obj.acc_level_policy = val;
                    case 14: // acc_is_active
                        obj.acc_is_active = val;
                    case 15: // acc_final_account_id
                        obj.acc_final_account_id = val;
                    case 16: // acc_address
                        obj.acc_address = val;
                    case 17: // acc_phone
                        obj.acc_phone = val;
                    case 18: // acc_email
                        obj.acc_email = val;
                    case 19: // acc_note1
                        obj.acc_note1 = val;
                    case 20: // acc_note2
                        obj.acc_note2 = val;
                    case 21: // acc_note3
                        obj.acc_note3 = val;
                    case 22: // acc_markaz_id
                        obj.acc_markaz_id = val;
                    case 23: // acc_mutabaka_amount
                        obj.acc_mutabaka_amount = val;
                        break;
                    case 24: // acc_mutabaka_date
                        obj.acc_mutabaka_date = val;
                        break;
                    case 25: // acc_discount_per
                        obj.acc_discount_per = val;
                        break;
                    case 26: // acc_mandoub_id
                        obj.acc_mandoub_id = val;
                        break;
                    case 27: // acc_mandoub_per
                        obj.acc_mandoub_per = val;
                        break;
                    case 28: // acc_only_cash
                        obj.acc_only_cash = val;
                        break;
                    case 29: // acc_week_days
                        obj.acc_week_days = val;
                        break;
                    case 30: // acc_markaz_ilzami
                        obj.acc_markaz_ilzami = val;
                        break;
                    case 31: // acc_barcode
                        obj.acc_barcode = val;
                        break;
                    case 32: // acc_budget
                        obj.acc_budget = val;
                        break;
                    default:
                        break;
                }
            }
            list.push(obj);
        }
    });
    // await DbAccounts.insertAll(list);
    return { success: true, data: list.length };
}


