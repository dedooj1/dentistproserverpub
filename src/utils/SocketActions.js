import DbUser from '../db/mysql/DbUser';

import { Server } from "socket.io";


// const express = require('express');
// const app = express();
// const http = require('http');
// const server = http.createServer(app);

// const { Server } = require("socket.io");
// const io = new Server(server);





// export const websocket = require('socket.io')(process.env.PORT_SOCKET);
// export const websocket = require('socket.io')(5052);
// export const websocket = require('socket.io')(process.env.PORT_SOCKET, {
//     cors: {
//       origin: '*',
//     }
//   });


let websocket = null;


export const disconnectAll = () => {
    websocket.disconnectSockets();
}

const mySocket = () => {
    console.warn('Socket Started on '+process.env.PORT_SOCKET);

    // websocket = require("socket.io")(process.env.PORT_SOCKET, {
    //     cors: {
    //         origin: "http://localhost",
    //         // origin: '*',
    //         methods: ["GET", "POST"],
    //         credentials: true,
    //         transports: ['websocket', 'polling'],
    //     },
    //     allowEIO3: true
    // })

    websocket = new Server(process.env.PORT_SOCKET, {
        cors: {
            origin: '*',
            transports: ['websocket', 'polling'],
        },
        // allowEIO4: true
    });
    websocket.on('connect', (e) => {
        // console.warn('connect> ', e.handshake.auth);
    });
    websocket.on("connection", (socket) => {
        socket.on('disconnect', (reason) => {
            // console.warn('disconnect> ', reason, socket.handshake.auth);
        });
    });

}

export const getSocketConnectionCount = async () => {
    const sockets = await websocket.fetchSockets();
    return sockets;
}

export function sendMessage(param, obj) {
    websocket.emit(`channel_${param.sidKeyClient}`, JSON.stringify(obj));
}

// export function sendMessageMobile(param, obj) {
//     websocket.emit(`channel_mobile_${param.sidKeyClient}`, JSON.stringify(obj));
// }

// export function sendSocketOrderCount(param, obj) {
//     let prop = {
//         type: "ORDERCOUNT",
//         body: obj
//     };
//     sendMessage(param, prop);
// }

// export async function sendSocketDriverInfo(param, idUser) {
//     let resultUser = await DbDriversOrderCount.findByAttributes(param, {
//         atts:{
//             idUser: idUser
//         },
//         select: `idUser, idKey, fullName, isOnline, isDeviceOnline, driverIsOnline, icon, transportTypeName, transportNumber, transportDescription, lastLat, lastLng, lastLocationDate, orderInprogressCount`,
//         view: true
//     })
//     if(resultUser.success){
//         let prop = {
//             type: "DRIVERINFO",
//             body: resultUser.data,
//         };
//         sendMessage(param, prop);
//     }
// }

export async function sendUserSettings(param) {
    let result = await DbUser.getSettings(param);
    if (result.success) {
        let prop = {
            type: "USER_SETTINGS",
            body: result.data,
        };
        sendMessage(param, prop);
    }
}

export async function sendItemsLevelPriceFinished(param, data) {
    let prop = {
        type: "CHANGE_ITEM_AVERAGE_PRICE_ALL",
        body: data,
    };
    sendMessage(param, prop);
}


export default mySocket;