
require('dotenv').config();

export const DEFAULT_TOKEN_VALID_HOUR = 168;
export const DEFAULT_DRIVER_LAST_LOCATION_MINUTE = 10;

export const SELECT_UNSELECTED_VALUE = "none";

export function isMd5(str) {
  return (/[a-fA-F0-9]{32}/).test(str);
}

export const distanceMToKm = (metr) => {
  if (metr) {
      let km = (Math.round(metr / 1000 * 10) / 10);
      if(km <= 0 ){
        km = 0.1;
      }
      return km;
  }
  return  false;
}

export function isLocal() {
  let host = process.env.DB_HOST;
  return Boolean(host.startsWith("192."));
}

export function isLatitude(lat) {
  return isFinite(lat) && Math.abs(lat) <= 90;
}

export function isLongitude(lng) {
  return isFinite(lng) && Math.abs(lng) <= 180;
}

const numberFormat1 = new Intl.NumberFormat("en-US");
// var usedOptions = numberFormat1.resolvedOptions();
export const formatDouble = (value) => {
    if (isNaN(value)) {
        return '';
    }
    try {
        return numberFormat1.format(value);
    } catch (error) {
    }
    return value
}