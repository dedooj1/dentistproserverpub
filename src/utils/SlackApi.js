import { isLocal } from '.';
import DbClients from '../db/mysql/DbClients';
import DbCountries from '../db/mysql/DbCountries';
import DbPackages from '../db/mysql/DbPackages';

const https = require('https');

// const yourWebHookURL = 'https://hooks.slack.com/services/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'; // PUT YOUR WEBHOOK URL HERE


function sendSlackMessage(webhookURL, messageBody) {


    if (isLocal()) return;

    try {
        messageBody = JSON.stringify(messageBody);
    } catch (e) {
        throw new Error('Failed to stringify messageBody', e);
    }


    return new Promise((resolve, reject) => {

        const requestOptions = {
            method: 'POST',
            header: {
                'Content-Type': 'application/json'
            }
        };

        // actual request
        const req = https.request(webhookURL, requestOptions, (res) => {
            let response = '';


            res.on('data', (d) => {
                response += d;
            });

            // response finished, resolve the promise with data
            res.on('end', () => {
                resolve(response);
            })
        });

        // there was an error, reject the promise
        req.on('error', (e) => {
            reject(e);
        });

        // send our message body (was parsed to JSON beforehand)
        req.write(messageBody);
        req.end();
    });
}


export async function sendClientRegistration(params, idClient) {
    let resultClient = await DbClients.findByPk(params, { id: idClient });
    let resultCountry = await DbCountries.findByPk(params, { id: resultClient.data.idCountry });
    let resultPackage = await DbPackages.findByPk(params, { id: resultClient.data.idPackage });
    let obj = {};

    if (resultClient.success) {
        obj = {
            Title: "New client registration",
            ID: resultClient.data.idClient,
            Name: resultClient.data.name,
            Email: resultClient.data.email,
            Country: resultCountry.success ? resultCountry.data.countryName : "",
            Phone: resultClient.data.phoneCode + " " + resultClient.data.phoneNumber,
            Package: resultPackage.success ? resultPackage.data.name : "",

        };
    }
    let blocks = [];
    for (const [key, value] of Object.entries(obj)) {
        blocks.push({
            type: "section",
            text: {
                type: "mrkdwn",
                text: `${key}: ${value}`
            }
        });

    }
    let data = {
        text: "New Client Registration",
        blocks: blocks,
    };

    try {
        const slackResponse = await sendSlackMessage(process.env.Slack_channel, data);
        return slackResponse;
    } catch (e) {

    }
}