import fs from 'fs';
import pathf from 'path';

export const renameFile = async (path, folder) => {
    try {
        if (path) {
            if (path.includes("tmp")) {
                let exist = await fs.existsSync(folder);
                if (!exist) {
                    await createFolderIfNotExist(folder);
                }
                let existTmpFile = await fs.existsSync(path);
                if (existTmpFile) {
                    let newPath = `${folder}/${pathf.basename(path)}`;
                    await fs.renameSync(path, newPath);
                    return newPath;
                }
            } else {
                return path;
            }
        }
        return null;
    } catch (e) {
        return null;
    }
}

export const deleteFileByPath = async (path) => {
    try {
        let exist = await fs.existsSync(path);
        if (exist) {
            fs.rmSync(path, { recursive: true, force: true });
            // await fs.unlinkSync(path)
        }
    } catch (err) {
        console.error(err)
    }
}

export const deleteTmpCreateTmp = async () => {
    let dir = "public/tmp";
    // if (fs.existsSync(dir)) {
    //     try {
    //         fs.rmSync(dir, { recursive: true });
    //     } catch (err) {
    //     }
    // }
    await deleteFileByPath(dir);
    await createFolderIfNotExist(dir);
    return { success: true };
}

export const createFolderIfNotExist = async (dir) => {
    if (!fs.existsSync(dir)) {
        try {
            fs.mkdirSync(dir, { recursive: true });
            // fs.chmodSync(dir, "0755");
            fs.chmodSync(dir, 0o755);
        } catch (error) {
            console.warn('error => ', error);
        }
    }
}