import moment from 'moment';
import { formatDouble } from '.';
import DbPrinter from '../db/mysql/DbPrinter';
import DbSettings from '../db/mysql/DbSettings';

const { exec } = require('child_process');

// const JARName = 'C:\\Users\\Nazarian\\Google Drive\\Java NetBeanV14 Projects\\JSRPrinter\\dist\\JSRPrinter.jar';
const JARName = 'public\\dist\\JSRPrinter.jar';

function parseResult(res) {
    let r = JSON.parse(res);
    let suc = JSON.parse(r.success);
    if (r.data) {
        let da = JSON.parse(r.data);
        return { success: suc ? true : false, data: da };
    }
    return { success: suc ? true : false };
}

function returnError(err) {
    return { success: false, errMsg: err }
}

function prepareJSONStringfy(obj) {
    for (const [key, value] of Object.entries(obj)) {
        if (typeof value === 'object') {
            obj[key] = JSON.stringify(value)
        }
    }
    return JSON.stringify(obj);
}

function replaceJSONChar(str) {
    return str.replace('&', ' ')
}

const Aligment = {
    center: "center",
    left: "left",
    right: "right",
}

const usePrinterJar = {
    getPrinterList: async () => {
        return new Promise((resolve, reject) => {
            exec(`java -cp ${JARName} jsrprinter.Main2`,
                function (error, stdout, stderr) {
                    if (stderr) {
                        console.warn('JAR Log Out = ', stderr);
                    }
                    if (error !== null) {
                        resolve(returnError(error));
                        return
                    }
                    try {
                        resolve(parseResult(stdout));
                    } catch (error) {
                        resolve(returnError("Error JAR"));
                    }
                });
        });
    },
    printToPrinterInvoice: async (obj) => {
        return new Promise((resolve, reject) => {
            if (obj.printer) {
                exec(`java -cp ${JARName} jsrprinter.Main1 ${prepareJSONStringfy(obj)}`,
                    function (error, stdout, stderr) {
                        if (stderr) {
                            console.warn('JAR Log Out = ', stderr);
                        }
                        if (error !== null) {
                            console.warn('error = ', error);
                            resolve(returnError("Error While Printing Invoice"));
                            return;
                        }
                        try {
                            resolve(parseResult(stdout));
                            return;
                        } catch (error) {
                            resolve(returnError("Error While Printing Invoice"));
                            return;
                        }
                    });
            } else {
                resolve(returnError("Error While Printing Invoice"));
            }
        });
    },
    printToPrinterOrder: async (obj) => {
        return new Promise((resolve, reject) => {
            if (obj.printer) {
                exec(`java -cp ${JARName} jsrprinter.Main3 ${prepareJSONStringfy(obj)}`,
                    function (error, stdout, stderr) {
                        if (stderr) {
                            console.warn('JAR Log Out = ', stderr);
                        }
                        if (error !== null) {
                            console.warn('error = ', error);
                            resolve(returnError("Error While Printing Order"));
                            return;
                        }
                        try {
                            resolve(parseResult(stdout));
                            return;
                        } catch (error) {
                            resolve(returnError("Error While Printing Order"));
                            return;
                        }
                    });
            } else {
                resolve(returnError("Error While Printing Invoice"));
            }
        });
    },
    printInvoiceOrders: async (param, invoice) => {
        let lines = invoice.invoiceLines;
        let objPrinterItems = {};
        for (const inl of lines) {
            if (inl.idPrinter) {
                if (!objPrinterItems.hasOwnProperty(inl.idPrinter)) {
                    objPrinterItems[inl.idPrinter] = [];
                }
                objPrinterItems[inl.idPrinter].push({ itemName: replaceJSONChar(inl.itName), quantity: `${inl.inlQuantityInvoiceOut}` });
            }
        }
        let ok = true;
        for (const [key, value] of Object.entries(objPrinterItems)) {
            let resultPrinter = await DbPrinter.findByAttributes(param, {
                atts: { idPrinter: key }
            });
            if (resultPrinter.success) {
                let path = resultPrinter.data.printerPath;
                if (resultPrinter.data.printerPath === "ByCashier") {
                    let resultPrinterCashier = await DbPrinter.findByAttributes(param, {
                        atts: { idPrinter: param.sidPrinter }
                    });
                    if (resultPrinterCashier.success) {
                        path = resultPrinterCashier.data.printerPath;
                    }
                }
                let obj = {
                    printer: `${path}`,
                    printerName: `${resultPrinter.data.printerName}`,
                    copies: `${resultPrinter.data.printerCopies}`,
                    lang: "ar",
                    dateOrder: {
                        date: moment().format("YYYY-MM-DD HH:mm"),
                        fontSize: 12,
                        aligment: Aligment.center,
                    },
                    idInvoice: {
                        id: `رقم الفاتورة : ${invoice.invoiceNumber ? invoice.invoiceNumber : invoice.idInvoicePos ? invoice.idInvoicePos : invoice.idInvoice ? invoice.idInvoice : ''}`,
                        fontSize: 12,
                        aligment: Aligment.center,
                    },
                    invoiceLines: objPrinterItems[key],
                }
                let resultP = await usePrinterJar.printToPrinterOrder(obj);
                // usePrinterJar.printToPrinterOrder(obj);
                ok &= resultP.success;
            }
        }
        return { success: ok }
    },
    printInvoice: async (params, idPrinter, invoice) => {
        let resultPrinter = await DbPrinter.findByAttributes(params, {
            atts: { idPrinter: idPrinter }
        });
        let obj = {
            printer: `${resultPrinter.data.printerPath}`,
            copies: `${resultPrinter.data.printerCopies}`,
            lang: "ar",
            // logo: {
            //     path: "D:\\UBicross\\Node\\AccountingV6Node\\public\\uploads\\usericon\\g2pcbgkwkqi6pv56.png",
            //     width: 50,
            //     height: 50,
            //     aligment: Aligment.center,
            // },
            clientName: {
                name: replaceJSONChar(params.tokenUser.client.name),
                fontSize: 18,
                aligment: Aligment.center,
            },
            userName: {
                name: replaceJSONChar(invoice.userName),
            },
            clientPhone: {
                phone: `(${params.tokenUser.client.phoneCode}) ${params.tokenUser.client.phoneNumber}`,
                fontSize: 10,
                aligment: Aligment.center,
            },
            dateInvoice: {
                date: moment(invoice.invoiceDate).format("YYYY-MM-DD HH:mm"),
                fontSize: 8,
                aligment: Aligment.center,
            },
            idInvoice: {
                // id: `رقم الفاتورة : ${invoice.invoiceNumber}`,
                id: `رقم الفاتورة : ${invoice.invoiceNumber ? invoice.invoiceNumber : invoice.idInvoicePos ? invoice.idInvoicePos : invoice.idInvoice ? invoice.idInvoice : ''}`,
                fontSize: 8,
                aligment: Aligment.center,
            },
            payName: {
                name: invoice.payName,
                fontSize: 8,
                aligment: Aligment.center,
            },
            invoiceLines: invoice.invoiceLines.map((oo) => {
                return {
                    itemName: replaceJSONChar(oo.itName),
                    quantity: `${oo.inlQuantityInvoiceOut}`,
                    lineTotal: `${formatDouble(oo.inlAmountInvoice)}`
                }
            }),
            totalAmount: {
                total: `${formatDouble(invoice.invoiceAmount)}`,
            },
            finalTotalAmount: {
                finalTotal: `الاجمالي   ${formatDouble(invoice.invoiceTotalAmount)}`,
                fontSize: 14,
                aligment: Aligment.center,
            },
        }
        if (invoice.invoiceDiscountAmount) {
            obj.discountAmount = {
                discount: `${formatDouble(invoice.invoiceDiscountAmount)}`,
            }
        }

        let resultSettings = await DbSettings.getOne(params);

        if (resultSettings.success && resultSettings.data.settingsInvoiceOrder && invoice.idAcc !== resultSettings.data.settingsInvoiceOrder.idAccSecond) {
            obj.accName = {
                name: invoice.accName,
                fontSize: 10,
                aligment: Aligment.center,
            }
        }
        let resultP = await usePrinterJar.printToPrinterInvoice(obj);
        return resultP;
    }
}


export default usePrinterJar;