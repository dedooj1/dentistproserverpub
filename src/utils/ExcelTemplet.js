import ExcelJS from 'exceljs';


export default function getExcelWorkSheet(lang = 'en') {

    var workbook = new ExcelJS.Workbook();

    workbook.creator = 'JSRC';
    workbook.lastModifiedBy = 'JSRC';
    workbook.created = new Date();
    workbook.modified = new Date();
    // workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    workbook.rightToLeft = (lang === 'ar');

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    return workbook;
}