import nodemailer from 'nodemailer';
import moment from 'moment';

import dotenv from 'dotenv';
dotenv.config();

export const NODEMAILER_CONFIG = {
  service: process.env.mail_service,
  host: process.env.mail_host,
  port: 465,
  encryption: "ssl",
  auth: {
    user: process.env.mail_username,
    pass: process.env.mail_password,
  },
  from: `JSOURCE <${process.env.mail_from}>`,
  // subMail: "",
  // debugMail: "",
};


function replaceAll(str, search, ch) {
  return str.split(search).join(ch).trim();
}

export function sendEmailUserActivation(to, code, userObj, clientObj) {
  let url = replaceAll(process.env.REACT_APP_URL, '"', "");
  const message = `Dear ${clientObj.name},<br><br>
                    Welcome to JSOURCE.<br><br>
                    You registered a new account, but you're not done yet. Take a minute to confirm this address so we know it belongs to you. Once you do, you can make it your primary address for all your Jsource services.<br><br>
                    <a href="${url}/${userObj.lang}/users/confirm/${code}"> ${url}/${userObj.lang}/users/confirm/${code}</a> <br><br>
                    Sincerely,<br>
                    JSOURCE Team <br><br>
                    Please do not reply to this email because we are not monitoring this inbox.<br><br>
                    Copyright © ${moment().format('YYYY')} Jsource. All rights reserved."`;


  const options = {
    to,
    subject: "Account activation",
    html: message,
  };
  sendMail(options);
}


export function sendEmailForgotPassword(to, code, userObj) {
  let url = replaceAll(process.env.REACT_APP_URL, '"', "");
  const message = `Click <a href="${url}/${userObj.lang}/users/forgot_password/${code}"> ${url}/${userObj.lang}/users/forgot_password/${code} </a> to change your password.`;
  const options = {
    to,
    subject: "JSOURCE AccountingV6 Forgot Password",
    html: message,
  };
  sendMail(options);
}


export function sendMail(options) {
  const transporter = nodemailer.createTransport(NODEMAILER_CONFIG);

  const sendMailOptions = {
    from: NODEMAILER_CONFIG.from,
    // to: (isDebugMode ? NODEMAILER_CONFIG.debugMail : options.to),
    to: options.to,
    subject: options.subject,
    text: options.text,
    html: options.html,
    bcc: process.env.mail_bcc_to,
  }

  transporter.sendMail(sendMailOptions, (err, info) => {
    if (err) {
      console.warn('err Mail : ', err)
    } else {
      console.warn('Email sent: ' + info.response);
    }
  });
  // const str = `
  //   From: ${sendMailOptions.from}<br />
  //   To: ${sendMailOptions.to}<br />
  //   Subject: ${sendMailOptions.subject}<br />
  //   <br />
  //   `;
  // sendMailOptions.text = str + sendMailOptions.text;
  // sendMailOptions.html = str + sendMailOptions.html;
  // sendMailSuperAdmin(sendMailOptions);
}

export function sendBackupFailed(dbName) {
  const options = {
    to: 'alm7aseb.js@gmail.com',
    subject: "BackUpFailer",
    text: 'Backup failed ' + dbName,
  };
  sendMail(options);
}

export function sendDBConnectionLost(error) {
  const options = {
    to: 'nazariank@hotmail.com',
    subject: "DBConnectionLost",
    text: 'Connection Lost ' + error,
  };
  sendMail(options);
}

export function sendEmailUserDublicateItems(to, clientObj, listDublicate) {
  const createTable = () => {
    let str = '';
    listDublicate.forEach((it) => {
      str +=
        `<tr>
          <td>${it.ordering}</td>
          <td>${it.itName}</td>
        </tr>`
    })
    return str;
  }
  const message = `Dear ${clientObj.name},<br/><br/>
                    Report for Import Items From Excel.<br/>
                    here is a list of items name empty or Duplicate<br/><br/> 
                    Count(${listDublicate.length})<br/><br/> 
                    <table>
                      <tr>
                        <th>Index</th>
                        <th>Item Name</th>
                      </tr>
                      ${createTable()}
                    </table><br/><br/><br/>
                    Sincerely,<br/>
                    JSOURCE Team <br/><br/>
                    Please do not reply to this email because we are not monitoring this inbox.<br/><br/>
                    Copyright © ${moment().format('YYYY')} Jsource. All rights reserved."`;


  const options = {
    to,
    // bcc: 'nazariank@hotmail.com, housamkat@gmail.com',
    subject: "Import Item From Excel",
    html: message,
  };
  sendMail(options);
}


// export function sendMailSuperAdmin(options) {

//   const adminOptions = {
//     from: NODEMAILER_CONFIG.from,
//     to: NODEMAILER_CONFIG.subMail,
//   }

//   adminOptions.subject = options.subject;
//   adminOptions.text = options.text;
//   adminOptions.html = options.html;


//   const transporter = nodemailer.createTransport(NODEMAILER_CONFIG);
//   transporter.sendMail(adminOptions, (err, info) => {
//     if (err) {
//       log(err);
//     }
//   });
// }

// export function sendEmailUserActivation(to, code) {
//   const options = {
//     to,
//     subject: "Urbenn Account Activation",
//     text: userActivationCode(code),
//     html: userActivationCode(code),
//   };
//   sendMail(options);
// }

// export function sendPasswordChangeEmail(to, code) {
//   const options = {
//     to,
//     subject: "Urbenn New Password Request",
//     text: userChangePasswordText(code),
//   };
//   sendMail(options);
// }

// export function sendEmailSellerApprove(to) {
//   const options = {
//     to,
//     // subject: "Urbenn Seller Approved",
//     subject: "Добро пожаловать на URBENN!",
//     text: "",
//     html: sellerApproveCode(),
//   };
//   sendMail(options);
// }

// export function sendEmailSellerMessage(to, text) {
//   const options = {
//     to,
//     subject: "Urbenn Seller Message",
//     text: text,
//     html: sellerMessageCode(text),
//   };
//   sendMail(options);
// }

// export function sendBaseMessage(to, subject, text) {
//   const options = {
//     to,
//     subject,
//     text: text,
//     html: sellerMessageCode(text),
//   };
//   sendMail(options);
// }



// function userActivationCode(code) {
//   const link = `${BASE_URL}/users/verify_email?code=${code}`;
//   return (`
//       <html>
//         <head>
//           <meta charset="utf-8">
//           <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
//         </head>
//         <body>
//           <div style="background-color:#f5f5f5">
//             <center>
//               <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color:#f5f5f5">
//                 <tbody>
//                   <tr>
//                     <td align="center" valign="top" style="padding:40px 20px">
//                       <table border="0" cellpadding="0" cellspacing="0" style="width:600px">
//                         <tbody>
//                           <tr>
//                             <td align="center" valign="top">
//                               <a href="https://urbenn.ru" class="logo" target="_blank">
//                                 <img align="center" alt=""  width="200" style="padding-bottom: 0; display: inline !important; vertical-align: bottom;" border="0">
//                               </a>
//                             </td>
//                           </tr>
//                           <tr>
//                             <td align="center" valign="top" style="padding-top:30px;padding-bottom:30px">
//                               <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px">
//                                 <tbody>
//                                   <tr>
//                                     <td align="center" valign="top" style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:150%;padding-top:40px;padding-right:40px;padding-bottom:30px;padding-left:40px;text-align:center">
//                                       <h1 style="color:#355071!important;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:bold;letter-spacing:-1px;line-height:115%;margin:0;padding:0;text-align:center">Title</h1>
//                                       <br>
//                                       content
//                                     </td>
//                                   </tr>
//                                   <tr>
//                                     <td align="center" valign="middle" style="padding-right:40px;padding-bottom:40px;padding-left:40px">
//                                       <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-radius: 9px; background-image: -webkit-linear-gradient(left, #fa584f, #fc6c2f); background-image: -moz-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%);  background-image: -webkit-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%); background-image: -ms-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%); box-shadow: 0 0 5px 0 rgba(255, 88, 49, .3);;border-collapse:separate!important;">
//                                         <tbody>
//                                           <tr>
//                                             <td align="center" valign="middle" style="color:#ffffff;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:bold;line-height:100%;">
//                                               <a href=${link} style="color:#ffffff;text-decoration:none;padding-top:18px;padding-right:25px;padding-bottom:21px;padding-left:25px;display:block " target="_blank">Button Text</a>
//                                             </td>
//                                           </tr>
//                                         </tbody>
//                                       </table>
//                                     </td>
//                                   </tr>
//                                 </tbody>
//                               </table>
//                             </td>
//                           </tr>
//                           <tr>
//                             <td align="center" valign="top">
//                               <table border="0" cellpadding="0" cellspacing="0" width="100%">
//                                 <tbody>
//                                   <tr>
//                                     <td align="center" valign="top" style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:125%">
//                                       &copy; <script>document.write(new Date().getFullYear())</script> Copyright text.
//                                     </td>
//                                   </tr>
//                                 </tbody>
//                               </table>
//                             </td>
//                           </tr>
//                         </tbody>
//                       </table>
//                     </td>
//                   </tr>
//                 </tbody>
//               </table>
//             </center>
//           </div>
//         </body>
//       </html>
//     `);

// }




// function userChangePasswordText(code) {
//   const link = `http://localhost:3000/en/password_change?code=${code}`;
//   return (`
//     <html>
//       <head>
//         <meta charset="utf-8">
//         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
//       </head>
//       <body>
//         <div style="background-color:#f5f5f5">
//           <center>
//             <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color:#f5f5f5">
//               <tbody>
//                 <tr>
//                   <td align="center" valign="top" style="padding:40px 20px">
//                     <table border="0" cellpadding="0" cellspacing="0" style="width:600px">
//                       <tbody>
//                         <tr>
//                           <td align="center" valign="top">
//                             <a href="https://urbenn.ru" class="logo" target="_blank">
//                               <img align="center" alt=""  width="200" style="padding-bottom: 0; display: inline !important; vertical-align: bottom;" border="0">
//                             </a>
//                           </td>
//                         </tr>
//                         <tr>
//                           <td align="center" valign="top" style="padding-top:30px;padding-bottom:30px">
//                             <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px">
//                               <tbody>
//                                 <tr>
//                                   <td align="center" valign="top" style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:150%;padding-top:40px;padding-right:40px;padding-bottom:30px;padding-left:40px;text-align:center">
//                                     <h1 style="color:#355071!important;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:bold;letter-spacing:-1px;line-height:115%;margin:0;padding:0;text-align:center">Title</h1>
//                                     <br>
//                                     content
//                                   </td>
//                                 </tr>
//                                 <tr>
//                                   <td align="center" valign="middle" style="padding-right:40px;padding-bottom:40px;padding-left:40px">
//                                     <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-radius: 9px; background-image: -webkit-linear-gradient(left, #fa584f, #fc6c2f); background-image: -moz-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%);  background-image: -webkit-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%); background-image: -ms-linear-gradient( 0deg, rgb(255,75,65) 0%, rgb(255,100,34) 100%); box-shadow: 0 0 5px 0 rgba(255, 88, 49, .3);;border-collapse:separate!important;">
//                                       <tbody>
//                                         <tr>
//                                           <td align="center" valign="middle" style="color:#ffffff;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:bold;line-height:100%;">
//                                             <a href=${link} style="color:#ffffff;text-decoration:none;padding-top:18px;padding-right:25px;padding-bottom:21px;padding-left:25px;display:block " target="_blank">Button Text</a>
//                                           </td>
//                                         </tr>
//                                       </tbody>
//                                     </table>
//                                   </td>
//                                 </tr>
//                               </tbody>
//                             </table>
//                           </td>
//                         </tr>
//                         <tr>
//                           <td align="center" valign="top">
//                             <table border="0" cellpadding="0" cellspacing="0" width="100%">
//                               <tbody>
//                                 <tr>
//                                   <td align="center" valign="top" style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:125%">
//                                     &copy; <script>document.write(new Date().getFullYear())</script> Copyright text.
//                                   </td>
//                                 </tr>
//                               </tbody>
//                             </table>
//                           </td>
//                         </tr>
//                       </tbody>
//                     </table>
//                   </td>
//                 </tr>
//               </tbody>
//             </table>
//           </center>
//         </div>
//       </body>
//     </html>
//   `);
// }




// function sellerApproveCode() {
//   let baseURLPHP = process.env.APP_PHP_URL;


//   return (
//     `
//     <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
//     <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />

//     <table width="100%" bgcolor="#FBFBFC" style="font-family: 'Roboto', sans-serif; color: #2D3037; padding: 20px 0px;">
//         <tbody>
//             <tr>
//                 <td>
//                     <table style="margin: auto; max-width: 600px; width: 100%">
//                         <tbody>
//                             <tr>
//                                 <td>
//                                     <!-- Header -->
//                                     <table style="width: 100%" bgcolor="#ffffff">
//                                         <tbody>
//                                             <tr>
//                                                 <td style="padding: 20px 20px 20px 20px; display: flex; justify-content: center">
//                                                     <img src="${baseURLPHP}/themes/guest/img/email/urbenn-logo.png" style="height: 40px;">
//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END Header -->

//                                     <!-- Header image -->
//                                     <table style="background-image: url('${baseURLPHP}/themes/guest/img/email/header-image-seller.png'); background-repeat: no-repeat; background-size: cover; width: 100%; background-position: center;">
//                                         <tbody>
//                                             <tr>
//                                                 <td style="color: #fff; font-weight: 300; font-size: 20px; text-align: center; padding: 30px;">
//                                                     Увеличьте продажи ваших товаров за счет <br>стран
//                                                     <span style="font-weight: bold;">ЕАЭС</span>
//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END header image -->

//                                     <!-- Intro -->
//                                     <table style="width: 100%; background-color: #fff; padding: 30px;">
//                                         <tbody>
//                                             <tr>
//                                                 <td style="font-size: 30px; font-weight: bold; text-transform: uppercase; text-align: center; padding-bottom: 20px;">Ваш аккаунт активирован</td>
//                                             </tr>

//                                             <tr>
//                                                 <td style="font-size: 14px; font-weight: 400; text-align: center; color: #6C6A7B; white-space: normal; word-break: break-word;">
//                                                   Поздравляем!
//                                                   Ваша юридическая информация подтверждена.
//                                                   Настройте остальные параметры Вашего магазина
//                                                   и начните продавать уже сегодня.
//                                                 </td>
//                                             </tr>
//                                             <tr>
//                                                 <td style="padding-top: 35px; text-align: center; display: block;">

//                                                         <a href="${baseURLPHP}" style="padding: 15px 69px; border-radius: 5px; color: #fff; text-decoration: none; background: linear-gradient(to right, #FA584F, #FC6C2F);">Вернуться на URBENN</a>

//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END Intro -->

//                                     <!-- After intro -->
//                                     <table style="width: 100%; background-color: #42444B; padding: 10px; text-align: center;">
//                                         <tbody>
//                                             <tr>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/participation-in-tenders-and-wholesale">Участие в тендерах</a>
//                                                 </td>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/doc-categories">Справочник продавца</a>
//                                                 </td>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/contact-us">Обратная связь</a>
//                                                 </td>
//                                             </tr>
//                                             <tr>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/tenders">Все тендеры</a>
//                                                 </td>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/seller-protection">Защита продавца</a>
//                                                 </td>
//                                                 <td>
//                                                     <a style="color: #fff; text-decoration: none; font-size: 12px; font-weight: 500;" href="${baseURLPHP}/blog">Блог</a>
//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END after intro -->

//                                     <!-- For social -->
//                                     <table style="width: 100%; background-color: #fff; padding: 10px; text-align: center;">
//                                         <tbody>
//                                             <tr style="display: flex; justify-content: center; align-items: center;">
//                                                 <td style="color: #777777; font-size: 12px; font-weight: 400;">
//                                                     Присоединяйтесь к нам в соцсетях:
//                                                 </td>
//                                                 <td style="padding-left: 10px;">
//                                                     <a href="https://www.facebook.com/urbenn/" style="background: linear-gradient(to right, #FA584F, #FC6C2F); width: 32px; height: 32px; border-radius: 50%; display: flex; align-items: center; justify-content: center;">
//                                                         <img style="width: 20px; padding: 6px; height: 20px; object-fit: contain;" src="${baseURLPHP}/themes/guest/img/email/fb.png" alt="fb">
//                                                     </a>
//                                                 </td>
//                                                 <td style="padding-left: 10px; display: flex; justify-content: center; align-items: center;">
//                                                     <a href="https://vk.com/urbenn" style="background: linear-gradient(to right, #FA584F, #FC6C2F); min-width: 32px; width: 32px; max-width: 32px; height: 32px; border-radius: 50%; display: flex; align-items: center; justify-content: center;">
//                                                         <img style="width: 20px; padding: 6px; height: 20px; object-fit: contain;" src="${baseURLPHP}/themes/guest/img/email/vk.png" alt="vk">
//                                                     </a>
//                                                 </td>
//                                                 <td style="padding-left: 10px; display: flex; justify-content: center; align-items: center;">
//                                                     <a href="https://www.youtube.com/" style="background: linear-gradient(to right, #FA584F, #FC6C2F); min-width: 32px; width: 32px; max-width: 32px; height: 32px; border-radius: 50%; display: flex; align-items: center; justify-content: center;">
//                                                         <img style="width: 20px; padding: 6px; height: 20px; object-fit: contain;" src="${baseURLPHP}/themes/guest/img/email/youtube.png" alt="youtube">
//                                                     </a>
//                                                 </td>
//                                                 <td style="padding-left: 10px; display: flex; justify-content: center; align-items: center;">
//                                                     <a href="https://telegram.org/" style="background: linear-gradient(to right, #FA584F, #FC6C2F); min-width: 32px; width: 32px; max-width: 32px; height: 32px; border-radius: 50%; display: flex; align-items: center; justify-content: center;">
//                                                         <img style="width: 20px; padding: 6px; height: 20px; object-fit: contain;" src="${baseURLPHP}/themes/guest/img/email/telegram.png" alt="telegram">
//                                                     </a>
//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END Social -->

//                                     <!-- Terms -->
//                                     <table style="width: 100%; background-color: #fff; padding: 10px; text-align: center; border-top: 1px solid #E7E7EC; border-bottom: 1px solid #E7E7EC">
//                                         <tbody>
//                                             <tr>
//                                                 <td style="font-weight: 400; color: #777777; font-size: 12px;">Вы получаете эту рассылку,  потому что зарегистрировались на URBENN - первой торговой платформе для стран ЕАЭС</td>
//                                             </tr>
//                                             <tr>
//                                                 <td style="font-weight: 400; color: #777777; font-size: 12px; padding-top: 10px;">Вы можете самостоятельно настроить получение писем <br>или отписаться от рассылки в <a href="${baseURLPHP}/seller/login" style="text-decoration: none; color: #FF4E10;">личном кабинете</a></td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END Terms -->

//                                     <!-- Copyright -->
//                                     <table style="width: 100%; background-color: #fff; padding: 10px; text-align: center;">
//                                         <tbody>
//                                             <tr>
//                                                 <td style="font-size: 10px; color: #6C6A7B; opacity: 0.4; font-weight: 400;">
//                                                     © <?=date("Y")?> Urbenn. Все права защищены и охраняются законом
//                                                 </td>
//                                             </tr>
//                                         </tbody>
//                                     </table>
//                                     <!-- END Copyright -->
//                                 </td>
//                             </tr>
//                         </tbody>
//                     </table>
//                 </td>
//             </tr>
//         </tbody>
//     </table>`
//   )







// }



// function sellerApproveCode22() {
//   // const link = `http://localhost:3088/users/verify_email?code=${code}`;
//   // const src = urlServerImg("/public/img/urbenn-logo-dark.svg");
//   // const src = urlServerImg("/public/img/urbenn-logo-dark.svg");
//   // const src = "http://arhiva.alo.rs/resources/img/27-06-2014/home_category/1274264-alo.jpg";
//   // const src = "https://urbenn.ru/images/Urben_logo.svg";
//   const src = "";

//   // const src = urlServerImg("/public/img/urbenn-logo.jpg");


//   return (`
//     <html lang="en">

//     <head>
//         <meta charset="UTF-8">
//         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
//     </head>

//     <body>
//         <div style="background-color:#f5f5f5">
//             <center>
//                 <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
//                     style="background-color:#f5f5f5">
//                     <tbody>
//                         <tr>
//                             <td align="center" valign="top" style="padding:40px 20px">
//                                 <table border="0" cellpadding="0" cellspacing="0" style="width:600px">
//                                     <tbody>
//                                         <tr>
//                                             <td align="center" valign="top">
//                                                 <a href="https://urbenn.ru" class="logo" target="_blank">
//                                                     <img align="center" alt="" src="${src}" width="200"
//                                                         style="padding-bottom: 0; display: inline !important; vertical-align: bottom;"
//                                                         border="0">
//                                                 </a>
//                                             </td>
//                                         </tr>
//                                         <tr>
//                                             <td align="center" valign="top" style="padding-top:30px;padding-bottom:30px">
//                                                 <table border="0" cellpadding="0" cellspacing="0" width="100%"
//                                                     style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px">
//                                                     <tbody>
//                                                         <tr>
//                                                             <td align="center" valign="top"
//                                                                 style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:150%;padding-top:40px;padding-right:40px;padding-bottom:30px;padding-left:40px;text-align:center">
//                                                                 <h1
//                                                                     style="color:#355071!important;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:bold;letter-spacing:-1px;line-height:115%;margin:0;padding:0;text-align:center">
//                                                                     Подтверждение аккаунта</h1>
//                                                                 <br>
//                                                                 Ваша юридическая информация подтверждена. Пожалуйста, войдите в вашу админ-панель и продолжите настройку параметров вашего магазина.
//                                                             </td>
//                                                         </tr>
//                                                     </tbody>
//                                                 </table>
//                                             </td>
//                                         </tr>
//                                         <tr>
//                                             <td align="center" valign="top">
//                                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
//                                                     <tbody>
//                                                         <tr>
//                                                             <td align="center" valign="top"
//                                                                 style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:125%">
//                                                                 &copy;
//                                                                 <script>document.write(new Date().getFullYear())</script>
//                                                                 Все права защищены.
//                                                             </td>
//                                                         </tr>
//                                                     </tbody>
//                                                 </table>
//                                             </td>
//                                         </tr>
//                                     </tbody>
//                                 </table>
//                             </td>
//                         </tr>
//                     </tbody>
//                 </table>
//                 <center>
//         </div>
//     </body>

//     </html>

//   `)
// }


// function sellerMessageCode(text) {
//   // const link = `http://localhost:3088/users/verify_email?code=${code}`;
//   // const src = urlServerImg("/public/img/urbenn-logo-dark.svg");
//   // const src = urlServerImg("/public/img/urbenn-logo-dark.svg");
//   // const src = "http://arhiva.alo.rs/resources/img/27-06-2014/home_category/1274264-alo.jpg";
//   // const src = "https://urbenn.ru/images/Urben_logo.svg";
//   const src = "";

//   // const src = urlServerImg("/public/img/urbenn-logo.jpg");


//   return (`
//     <html lang="en">

//     <head>
//         <meta charset="UTF-8">
//         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
//     </head>

//     <body>
//         <div style="background-color:#f5f5f5">
//             <center>
//                 <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
//                     style="background-color:#f5f5f5">
//                     <tbody>
//                         <tr>
//                             <td align="center" valign="top" style="padding:40px 20px">
//                                 <table border="0" cellpadding="0" cellspacing="0" style="width:600px">
//                                     <tbody>
//                                         <tr>
//                                             <td align="center" valign="top">
//                                                 <a href="https://urbenn.ru" class="logo" target="_blank">
//                                                     <img align="center" alt="" src="${src}" width="200"
//                                                         style="padding-bottom: 0; display: inline !important; vertical-align: bottom;"
//                                                         border="0">
//                                                 </a>
//                                             </td>
//                                         </tr>
//                                         <tr>
//                                             <td align="center" valign="top" style="padding-top:30px;padding-bottom:30px">
//                                                 <table border="0" cellpadding="0" cellspacing="0" width="100%"
//                                                     style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px">
//                                                     <tbody>
//                                                         <tr>
//                                                             <td align="center" valign="top"
//                                                                 style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:150%;padding-top:40px;padding-right:40px;padding-bottom:30px;padding-left:40px;text-align:center">
//                                                                 <h1
//                                                                     style="color:#355071!important;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:bold;letter-spacing:-1px;line-height:115%;margin:0;padding:0;text-align:center">
//                                                                     Подтверждение аккаунта</h1>
//                                                                 <br>
//                                                                 Чтобы активировать Ваш профиль в Urbenn, <br> нажмите кнопку
//                                                                 “Подтвердить регистрацию”.
//                                                                 <br><br>
//                                                                 Message:
//                                                                 <br>
//                                                                 <p>${text}</p>
//                                                             </td>
//                                                         </tr>
//                                                     </tbody>
//                                                 </table>
//                                             </td>
//                                         </tr>
//                                         <tr>
//                                             <td align="center" valign="top">
//                                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
//                                                     <tbody>
//                                                         <tr>
//                                                             <td align="center" valign="top"
//                                                                 style="color:#355071;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:125%">
//                                                                 &copy;
//                                                                 <script>document.write(new Date().getFullYear())</script>
//                                                                 Все права защищены.
//                                                             </td>
//                                                         </tr>
//                                                     </tbody>
//                                                 </table>
//                                             </td>
//                                         </tr>
//                                     </tbody>
//                                 </table>
//                             </td>
//                         </tr>
//                     </tbody>
//                 </table>
//                 <center>
//         </div>
//     </body>

//     </html>

//   `)
// }