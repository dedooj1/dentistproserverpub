import { sendUserSettings } from '../../utils/SocketActions';
import DbManagerCustom from './DbManagerCustom';
import DbPlan from './DbPlan';
import DbTeeth from './DbTeeth';
import DbUser from './DbUser';

class DbTreatments extends DbManagerCustom {
  constructor() {
    super("tbl_treatments");
    this.pk = "idTreatments";
    // this.noDublicateCol = ['treatmentsName'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            treatmentsName varchar(255) NOT NULL,
            optionList TEXT NULL,
            optionListType int(1) NULL,
            location int(5) NULL,
            type int(5) NULL,
            price float NULL,
            status int(1) NULL,
            description TEXT DEFAULT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let obj = {
      treatmentsName: sd.treatmentsName,
      optionList: sd.optionList || null,
      optionListType: sd.optionListType || null,
      location: sd.location || null,
      description: sd.description,
      type: sd.type || null,
      price: sd.price || null,
      status: this.STATUS.ACTIVE,
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;

      let obj = {
        idTreatments: result.data.idTreatments,
        treatmentsName: sd.treatmentsName,
        // optionList: `${sd.optionList}` || null,
        optionList: sd.optionList || null,
        optionListType: sd.optionListType || null,
        location: sd.location || null,
        description: sd.description,
        type: sd.type || null,
        price: sd.price || null,
        status: sd.status,
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Treatment Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.optionList = JSON.parse(resultOne.data.optionList);
      return resultOne;
    }
    return this.returnError("Treatment Not Found!");
  }

  async getOneByType(params) {
    let resultOne = await this.findAll(params, {
      condition: `type= ${params.search_id}`,
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.map(val => {
        val.optionList = JSON.parse(val.optionList);
        return val
      })
      return resultOne;
    }
    return this.returnError("Treatment Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["treatmentsName", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(treatmentsName LIKE "%${pd.q.trim()}%" OR description LIKE "%${pd.q.trim()}%" OR optionList LIKE "%${pd.q.trim()}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }

      if (pd.filter.hasOwnProperty("type") && pd.filter.type) {
        conditionList.push(`type = "${pd.filter.type}"`);
      }

      if (pd.filter.hasOwnProperty("status") && pd.filter.status) {
        conditionList.push(`status = "${pd.filter.status}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition });

    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result
  }

  async get_all(params) {
    // let tokenUser = params.tokenUser;
    // let result = await this.findAllByAttributes(params, {
    //   atts: { idTreatments: tokenUser.idUser },
    //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    // });
    let result = await this.findAll(params);
    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result;
  }

  async getCount(params) {
    return await this.countByAttributes(params, {});
  }
  // '[{ "label": "Default Stage", "value": [{ "Treatments": { "idTreatments": 1, "idKey": "85757086a62ff109e44bf1500f32027e", "treatmentsName": "test", "optionList": null, "optionListType": 1, "location": 1, "type": 1, "price": 5000, "status": 1, "description": "dfsfsf", "createDate": "2022-11-13T10:43:16.000Z", "updateDate": "2022-11-13T10:43:16.000Z" }, "optionTreatments": null, "teeth": [12], "observations": "drfgdfg", "price": 56546, "realizationDate": "2022-11-09T10:44:39.374Z", "doctor": { "clientIdKey": "a9fb37c3b135892195e78ba472a8604a", "idKey": "f0c75625485e810b6b41a8d7af1d558a", "idUser": 114, "idClient": 107, "fullName": "JSOURCE_JSRC", "login": "dedooj11@gmail.com", "email": "dedooj11@gmail.com", "phoneCode": "+963", "phoneNumber": "930492567", "roleName": "Admin", "idRole": 2, "icon": null, "status": 1, "idPolicy": 1, "lang": "ar", "createDate": "2022-11-12T15:22:42.000Z", "clientStatus": 2, "clientName": "JSOURCE_JSRC", "clientLegalName": null, "clientPhoneCode": "+963", "clientPhoneNumber": "930492567", "clientTin": null, "clientEmail": "dedooj11@gmail.com", "clientIcon": null, "clientPackage": 1, "clientPackageName": "Micro", "clientidCountry": 208, "clientidCurrency": 208, "clientCountryName": "Syria", "clientCurrencyCode": "SYP", "clientCurrencySymbol": "£", "TeethNameType": 1, "idBranch": 1, "direction": "ltr", "userPolicy": { "idKey": "00cccd3245d277f90b19561d7f03313a", "idUserPolicy": 1, "polName": "صلاحيات مدير", "polNameEn": "Admin Policy", "polValues": { "useCurrency": true, "addCurrency": true, "editCurrency": true, "useCurrencyRate": true, "addCurrencyRate": true, "editCurrencyRate": true, "useCostCenter": true, "addCostCenter": true, "editCostCenter": true, "useAccountGroup": true, "addAccountGroup": true, "editAccountGroup": true, "usePaymentType": true, "addPaymentType": true, "editPaymentType": true, "useTransactionType": true, "addTransactionType": true, "editTransactionType": true, "useAccounts": true, "addAccounts": true, "editAccounts": true, "useProfileCustomers": true, "addProfileCustomers": true, "editProfileCustomers": true, "useProfileSupliers": true, "addProfileSupliers": true, "editProfileSupliers": true, "useAccountStatment": true, "useTrialBalance": true, "useTransactions": true, "addTransactions": true, "editTransactions": true, "useProfileStore": true, "addProfileStore": true, "editProfileStore": true, "useUnit": true, "addUnit": true, "editUnit": true, "useItemGroup": true, "addItemGroup": true, "editItemGroup": true, "useDaftar": true, "addDaftar": true, "editDaftar": true, "usePrinters": true, "addPrinters": true, "editPrinters": true, "useInvoiceType": true, "addInvoiceType": true, "editInvoiceType": true, "useItems": true, "addItems": true, "editItems": true, "useItemsStatment": true, "useInvoiceOrders": true, "addInvoiceOrders": true, "editInvoiceOrders": true, "useInvoiceOrdersSettings": true, "useInvoice": true, "addInvoice": true, "editInvoice": true, "useMuntaj": true, "addMuntaj": true, "editMuntaj": true, "useTasni3": true, "addTasni3": true, "editTasni3": true, "useReports": true, "addReports": true, "editReports": true, "useUser": true, "addUser": true, "editUser": true, "useUsersPolicy": true, "addUsersPolicy": true, "editUsersPolicy": true } }, "accessToken": "dbea5fd31a38073931d5c036a70385d9" }, "StageName": "Default Stage" }] }]'
  async delete(params) {
    if (params.sidRole === DbUser.ROLE.SUPERADMIN || params.sidRole === DbUser.ROLE.ADMIN) {
      let resultGet = await this.get_one_by_idkey(params, params.search_id);
      if (resultGet.success) {
        let idTreatments = resultGet.data.idTreatments;
        let teethName = ''
        let planName = ''
        let resultTeeths = await DbTeeth.get_all(params);
        let resultPlans = await DbPlan.get_all(params);
        if (resultTeeths.success) {
          let inTeeth = false;
          for (let i = 0; i < resultTeeths.rowCount; i++) {
            let tret = resultTeeths.data[i].TreatmentReflectionList
            if (tret !== []) {
              for (let j = 0; j < tret.length; j++) {
                if (tret[j].TreatmentId.title.idTreatments === idTreatments) {
                  inTeeth = true;
                  break;
                }
              }
            }
            if (inTeeth) {
              teethName = resultTeeths.data[i].teethNameNumber
              break
            }
          }
        } else {
          return this.returnError("Treatments Not Found");
        }
        if (resultPlans.success) {
          let inTeeth = false;
          for (let i = 0; i < resultPlans.rowCount; i++) {
            let tret = resultPlans.data[i].planTreatments
            if (tret !== [] && tret !== null) {
              for (let j = 0; j < tret.length; j++) {
                for (let k = 0; k < tret.length; k++) {
                  let val = tret[j].value[k]
                  if (val.Treatments.idTreatments === idTreatments) {
                    inTeeth = true;
                    break;
                  }
                }
              }
            }
            if (inTeeth) {
              planName = resultPlans.data[i].planName
              break
            }
          }
        } else {
          return this.returnError("Treatments Not Found");
        }
        if (!teethName) {
          if (!planName) {
            if (resultGet.data.icon) {
              await deleteFileByPath(resultGet.data.icon);
            }
            await this.deleteByAttributes(params, {
              atts: { idTreatments: idTreatments }
            });
            return this.returnSuccess();

          } else {
            return this.returnError("Please delete treatment from plan " + planName);
          }
        } else {
          return this.returnError("Please delete treatment from Reflection in teeth " + teethName);
        }
      } else {
        return this.returnError("Treatments Not Found");
      }
    }
    return this.returnError("You dont have permission to delete Treatments");
  }

  //   async delete(params) {
  //     let resultGet = await this.get_one_by_idkey(params, params.search_id);
  //     if (resultGet.success) {
  //       let idTreatments = resultGet.data.idTreatments;
  //       if (resultGet.data.icon) {
  //         await deleteFileByPath(resultGet.data.icon);
  //       }
  //       await this.deleteByAttributes(params, {
  //         atts: { idTreatments: idTreatments }
  //       });
  //       return this.returnSuccess();
  //     }
  //     return this.returnError("Diagnosies Not Found");
  //   }
}

export default new DbTreatments;