import DbManagerCustom from './DbManagerCustom';
import DbPatients from './DbPatients';
import DbTeeth from './DbTeeth';
import DbUser from './DbUser';

class DbPlan extends DbManagerCustom {
  constructor() {
    super("tbl_plans", "view_plans");
    this.pk = "idPlan";
    // this.noDublicateCol = ['planName'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            planName varchar(255) NOT NULL,
            idDoctor int(11) NOT NULL,
            idPatient int(11) NOT NULL,
            status int(1) NOT NULL,
            teath TEXT NULL,
            planDiagnosies TEXT NULL,
            planTreatments TEXT NULL,
            steps TEXT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }
  createViewSQL(params) {
    const sql = `CREATE VIEW ${this.viewName(params)} AS (
                    SELECT 
                      p.*,
                      u.fullName AS doctorName,
                      u.medicalSpecialty,
                      u.icon AS doctorIcon,
                      u.idPolicy,
                      u.status AS userStatus,
                      u.idRole,
                      u.login,
                      u.birthDate AS birthDate,
                      u.phoneNumber AS doctorPhoneNumber,
                      u.phoneCode AS doctorPhoneCode,
                      pt.icon AS patientIcon,
                      pt.shareDoctor,
                      pt.description AS patientDescription,
                      pt.Generd,
                      pt.birthDate AS birthDatePatient,
                      pt.phoneNumber AS patientPhoneNumber,
                      pt.phoneCode AS patientPhoneCode,
                      pt.email AS patientEmail,
                      TIMESTAMPDIFF( YEAR, pt.birthDate, now() ) as year, 
                      TIMESTAMPDIFF( MONTH, pt.birthDate, now() ) % 12 as month,
                      pt.patientName,
                      pt.idKey AS patientIdkey
                    FROM ${this.tableName(params)} p
                    LEFT JOIN ${DbUser.tableName(params)} u ON (p.idDoctor = u.idUser)
                    LEFT JOIN ${DbPatients.tableName(params)} pt ON (p.idPatient = pt.idPatient)
                )`;
    return sql;
  }
  async insert(params) {
    let sd = params.saveData;
    let tokenUser = params.tokenUser;
    let obj = {
      planName: sd.planName,
      idPatient: sd.idPatient,
      teath: sd.teath || null,
      planDiagnosies: sd.planDiagnosies || [],
      planTreatments: sd.planTreatments || [],
      steps: sd.steps || null,
      status: this.STATUS.ACTIVE,
      idDoctor: tokenUser.idUser,
    }
    // if(sd.hasOwnProperty("teath")) {
    //   sd.teath.map(val => {
    //     teeth = await DbTeeth.findByPk(params, {
    //       id: val.idTeeth
    //     })
    //   })
    // }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;

      let obj = {
        idPlan: result.data.idPlan,
        planName: sd.planName,
        teath: sd.teath || null,
        planDiagnosies: sd.planDiagnosies || [],
        planTreatments: sd.planTreatments || [],
        steps: sd.steps || null,
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Plan Not Found!");
  }

  async updatePlanDiagnosies(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;
      let planDiagnosies = result.data.planDiagnosies ? JSON.parse(result.data.planDiagnosies) : []
      planDiagnosies.push(sd.planDiagnosies)
      let obj = {
        idPlan: result.data.idPlan,
        planDiagnosies: planDiagnosies || [],
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Plan Not Found!");
  }

  async updatePlanTreatments(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;
      let planTreatments = result.data.planTreatments ? JSON.parse(result.data.planTreatments) : []
      planTreatments.push(sd.planTreatments)
      let obj = {
        idPlan: result.data.idPlan,
        planTreatments: planTreatments || [],
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Plan Not Found!");
  }

  async updatePlanTeeth(params) {
    if (result.success) {
      let sd = params.saveData;
      let obj = {
        idPlan: result.data.idPlan,
        teath: sd.teath || {},
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Plan Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.teath = JSON.parse(resultOne.data.teath);
      resultOne.data.planTreatments = JSON.parse(resultOne.data.planTreatments);
      resultOne.data.planDiagnosies = JSON.parse(resultOne.data.planDiagnosies);
      resultOne.data.steps = JSON.parse(resultOne.data.steps);
      return resultOne;
    }
    return this.returnError("Plan Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["planName", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(planName LIKE "%${pd.q.trim()}%")`);
    }

    if (params.sidRole !== DbUser.ROLE.SUPERADMIN && params.sidRole !== DbUser.ROLE.ADMIN) {
      conditionList.push(`idDoctor = ${tokenUser.idUser} OR shareDoctor LIKE "%${tokenUser.idUser}%"`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate.trim()) {
        conditionList.push(`createDate >= ${this.formatSQLDate(pd.filter.fromDate)}`);
      }
      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= ${this.formatSQLDate(pd.filter.toDate)}`);
      }

      if (pd.filter.hasOwnProperty("idPatient") && pd.filter.idPatient) {
        conditionList.push(`idPatient = "${pd.filter.idPatient}"`);
      }

      if (pd.filter.hasOwnProperty("idDoctor") && pd.filter.idDoctor) {
        conditionList.push(`idDoctor = "${pd.filter.idDoctor}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition, view: true });

    result.data.map(p => {
      p.teath = JSON.parse(p.teath);
      p.planDiagnosies = JSON.parse(p.planDiagnosies);
      p.planTreatments = JSON.parse(p.planTreatments);
      p.steps = JSON.parse(p.steps);
    })
    return result
  }

  async listPlanPatient(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["planName", "createDate", "updateDate"];

    let conditionList = [`idPatient = ${params.search_id}`];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(planName LIKE "%${pd.q.trim()}%")`);
    }

    // if (params.sidRole !== this.ROLE.SUPERADMIN && params.sidRole !== this.ROLE.ADMIN) {
    //   conditionList.push(`idDoctor = ${tokenUser.idUser}`);
    // }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }

      if (pd.filter.hasOwnProperty("idPatient") && pd.filter.idPatient) {
        conditionList.push(`idPatient = "${pd.filter.idPatient}"`);
      }

      if (pd.filter.hasOwnProperty("idDoctor") && pd.filter.idDoctor) {
        conditionList.push(`idDoctor = "${pd.filter.idDoctor}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition, view: true });
    if (!result.success) {
      return this.returnError("Plan is not found")
    }
    result.data.map(p => {
      p.teath = JSON.parse(p.teath);
      p.planDiagnosies = JSON.parse(p.planDiagnosies);
      p.planTreatments = JSON.parse(p.planTreatments);
      p.steps = JSON.parse(p.steps);
    })
    return result
  }

  async listDiagnosies(params) {
    let result = await this.findAllByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
    });
    result.data = JSON.parse(result.data[0].planDiagnosies);
    //result.data = JSON.parse(result.data.teath);
    // result.data = JSON.parse(result.data.planTreatments);

    return result;
  }

  async listTeeth(params) {
    let result = await this.findAllByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
    });
    // result.data = JSON.parse(result.data[0].planDiagnosies);
    result.data = JSON.parse(result.data[0].teath);
    // result.data = JSON.parse(result.data[0].planTreatments);

    return result;
  }

  async listTreatment(params) {
    let result = await this.findAllByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
    });
    // result.data = JSON.parse(result.data[0].planDiagnosies);
    // result.data = JSON.parse(result.data[0].teath);
    result.data = JSON.parse(result.data[0].planTreatments);

    return result;
  }

  async listSteps(params) {
    let result = await this.findAllByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
    });
    // result.data = JSON.parse(result.data[0].planDiagnosies);
    // result.data = JSON.parse(result.data[0].teath);
    result.data = JSON.parse(result.data[0].steps);

    return result;
  }

  async get_all(params) {
    // let tokenUser = params.tokenUser;
    // let result = await this.findAllByAttributes(params, {
    //   atts: { idPlan: tokenUser.idUser },
    //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    // });
    let result = await this.findAll(params);
    result.data.map(p => {
      p.teath = JSON.parse(p.teath);
      p.planDiagnosies = JSON.parse(p.planDiagnosies);
      p.planTreatments = JSON.parse(p.planTreatments);
      p.steps = JSON.parse(p.steps);
    })
    return result;
  }

  async getCount(params, ats = {}) {
    return await this.countByAttributes(params, ats);
  }

  async delete(params) {
    let resultGet = await this.get_one_by_idkey(params, params.search_id);
    if (resultGet.success) {
      if (resultGet.data.idDoctor === params.sidUser || params.sidRole === DbUser.ROLE.SUPERADMIN || params.sidRole === DbUser.ROLE.ADMIN) {
        let idPlan = resultGet.data.idPlan;
        if (resultGet.data.icon) {
          await deleteFileByPath(resultGet.data.icon);
        }
        await this.deleteByAttributes(params, {
          atts: { idPlan: idPlan }
        });
        return this.returnSuccess();
      } else {
        return this.returnError("You don't have permission to delete plan");
      }
    }
    return this.returnError("Plan Not Found");
  }
}

export default new DbPlan;