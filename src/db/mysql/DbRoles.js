import DbManagerMain from './DbManagerMain';

class DbRoles extends DbManagerMain {
    constructor() {
        super("ard_roles");
        this.pk = "idRole";
        this.noDublicateCol = [];
    }

}

export default new DbRoles;