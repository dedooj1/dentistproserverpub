import DbManager from './DbManager';

class DbManagerCustom extends DbManager {

    constructor(tableName, viewName) {
        super(tableName, viewName)
        this.init({});
    }

    selectViewOrTable(params, opt) {
        if (opt.view) {
            return `${process.env.DB_Name_Stracture}_${params.sidClient}.${this.vName}`;
        }
        return `${process.env.DB_Name_Stracture}_${params.sidClient}.${this.tName}`;
    }
}

export default DbManagerCustom;