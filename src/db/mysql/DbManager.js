import moment from 'moment';
import uniqueString from 'unique-string';
import { sendUserSettings } from '../../utils/SocketActions';
import { getDB } from './config';
import { v4 as uuidv4 } from 'uuid';
import backUpDb from './BackUp';

class DbManager {

    constructor(tName, vName) {
        this.tName = tName;
        this.vName = vName ? vName : tName;
        this.defaultSort = " 1 ASC";
        this.noDublicateCol = [];
        this.sortKeys = ["ASC", "DESC"];
        this.STATUS = {
            ACTIVE: 1,
            INACTIVE: 2,
        }
    }

    async init(params) {
        this.noDublicateColumens(params);
    }

    async createTable(params) {
        let delSql = `DROP TABLE IF EXISTS ${this.tableName(params)}`
        await this.query(params, delSql);
        let sql = await this.createTableSQL(params);
        let result1 = await this.query(params, sql);
        if (result1.success) {
            let sql2 = `ALTER TABLE ${this.tableName(params)}
                     ADD PRIMARY KEY (${this.pk});`;
            let result2 = await this.query(params, sql2);
            if (result2.success) {
                let sql3 = `ALTER TABLE ${this.tableName(params)}
                         MODIFY ${this.pk} int(11) NOT NULL AUTO_INCREMENT;`;
                let result3 = await this.query(params, sql3);
                await this.init(params);
                return result3;
            }
            return result2;
        }
        return result1;
    }

    tableName(params) {
        return `${this.selectViewOrTable(params, {})}`;
    }

    viewName(params) {
        return `${this.selectViewOrTable(params, { view: true })}`;
    }

    async dropView(params) {
        let delSql = `DROP VIEW IF EXISTS ${this.viewName(params)}`
        return await this.query(params, delSql);
    }

    async createView(params) {
        await this.dropView(params)
        let sql = await this.createViewSQL(params);
        return await this.query(params, sql);
    }

    formatSQLDate(dat, time = true) {
        let format = time ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD";
        return `'${moment(dat).format(format)}'`;
    }

    replaceAll(str, search, ch) {
        return str.trim().split(search).join(ch).trim();
    }

    formatDate(dat) {
        dat = this.replaceAll(dat, "'", "");
        try {
            return moment(dat).format("YY-MM-DD");
        } catch (error) {
            return '';
        }
    }

    formatDouble(value) {
        const numberFormat1 = new Intl.NumberFormat("en-US", { maximumFractionDigits: 2, });
        if (isNaN(value)) {
            return '';
        }
        try {
            return numberFormat1.format(value);
        } catch (error) {
        }
        return value
    }

    formatSQLDateMovement(dat, time = true) {
        let format = time ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD";
        return `${moment(dat).format(format)}`;
    }

    formatSQLDateFrom(dat, time = true) {
        let format = time ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD 00:00:00";
        return `'${moment(dat).startOf('day').format(format)}'`;
    }

    formatSQLDateTo(dat, time = true) {
        let format = time ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD 23:59:59";
        return `'${moment(dat).endOf('day').format(format)}'`;
    }

    getCurrentDate(time = true) {
        return time ? moment().format("YYYY-MM-DD HH:mm:ss") : moment().format("YYYY-MM-DD");
    }

    momentSubtract(v, t) {
        return moment().subtract(v, t).format("YYYY-MM-DD HH:mm:ss");
    }

    momentFromMonth(month, year) {
        let dat = [];
        let monthS = month;
        if (month < 10) {
            monthS = `0${month}`;
        }
        dat[0] = moment(`${year}-${monthS}-01`);
        dat[1] = moment(dat[0]).endOf('month');
        dat[0] = this.formatSQLDateFrom(dat[0]);
        dat[1] = this.formatSQLDateTo(dat[1]);
        return dat;
    }
    
    momentStartYearUntilNow(month,year) {
        let dat = [];
        let monthS = month;
        if (month < 10) {
            monthS = `0${month}`;
        }
        dat[0] = moment(`${year}-01-01`);
        dat[1] = moment(`${year}-${monthS}-01`).endOf('month');
        dat[0] = this.formatSQLDateFrom(dat[0]);
        dat[1] = this.formatSQLDateTo(dat[1]);
        return dat;
    }
    
    momentStartEndYear(year) {
        let dat = [];
        dat[0] = moment(`${year}-01-01`);
        dat[1] = moment(`${year}-12-01`).endOf('month');
        dat[0] = this.formatSQLDateFrom(dat[0]);
        dat[1] = this.formatSQLDateTo(dat[1]);
        return dat;
    }

    async get_one(params, id) {
        return await this.findByPk(params, {
            id: id
        });
    }

    async get_one_by_idkey(params, idkey) {
        return await this.findByAttributes(params, {
            atts: { idKey: idkey },
        });
    }

    async get_all(params) {
        return await this.findAll(params);
    }

    async noDublicateColumens(params) {
        if (this.noDublicateCol.length) {
            for (const noDublCol of this.noDublicateCol) {
                let key = this.replaceAll(noDublCol, ',', '_');
                key = this.replaceAll(key, ' ', '');
                let sql = `ALTER TABLE ${this.tableName(params)} ADD UNIQUE ${key} (${noDublCol}) USING BTREE;`
                await this.query(params, sql);
            }
        }
    }

    logDB(...text) {
        console.warn("JSRC LOG => ", text);
    }

    async findByPk(params, opt) {
        let select = opt.hasOwnProperty("select") ? opt.select : "*";
        return await this.queryOne(params, `SELECT ${select} FROM ${this.selectViewOrTable(params, opt)}  ${opt.hasOwnProperty('name') ? opt.name : ''} WHERE ${this.pk} = ${opt.id}`);
    }

    async findByAttributes(params, opt) {
        let attCond = "";
        if (opt.hasOwnProperty("atts")) {
            for (const key in opt.atts) {
                if (attCond) attCond += " AND ";
                let isString = (typeof opt.atts[key] == "string");
                attCond += `${key} = ${isString ? '"' : ""}${opt.atts[key]}${isString ? '"' : ""}`;
            }
        }
        if (attCond) attCond = "WHERE " + attCond;
        if (opt.hasOwnProperty("condition")) {
            if (!attCond) attCond = " WHERE ";
            attCond += " " + opt.condition;
        }
        let select = opt.hasOwnProperty("select") ? opt.select : "*";
        let sort = opt.hasOwnProperty("sort") ? " ORDER BY " + opt.sort : "";
        let sql = `SELECT ${select} FROM ${this.selectViewOrTable(params, opt)}  ${opt.hasOwnProperty('name') ? opt.name : ''} ${attCond} ${sort}`;
        console.log('sql1', sql);
        return await this.queryOne(params, sql);
    }

    async findAllByAttributes(params, opt) {
        let attCond = "";
        if (opt.hasOwnProperty("atts")) {
            for (const key in opt.atts) {
                if (attCond) attCond += " AND ";
                let isString = (typeof opt.atts[key] == "string");
                attCond += `${key} = ${isString ? '"' : ""}${opt.atts[key]}${isString ? '"' : ""}`;
            }
        }
        if (attCond) attCond = "WHERE " + attCond;
        if (opt.hasOwnProperty("condition")) {
            if (!attCond) attCond = " WHERE ";
            attCond += " " + opt.condition
        };
        let sort = opt.hasOwnProperty("sort") ? " ORDER BY " + opt.sort : "";
        let select = opt.hasOwnProperty("select") ? opt.select : "*";
        let sql = `SELECT ${select} FROM ${this.selectViewOrTable(params, opt)}  ${opt.hasOwnProperty('name') ? opt.name : ''} ${attCond} ${sort}`;
        console.log('sql2', sql);
        return await this.query(params, sql);
    }

    async find(params, opt = {}) {
        let attCond = "";
        if (opt.hasOwnProperty("condition")) attCond = " WHERE " + opt.condition;
        let select = opt.hasOwnProperty("select") ? opt.select : "*";
        return await this.queryOne(params, `SELECT ${select} FROM ${this.selectViewOrTable(params, opt)}  ${opt.hasOwnProperty('name') ? opt.name : ''} ${attCond}`);
    }

    async findAll(params, opt = {}) {
        let attCond = "";
        if (opt.hasOwnProperty("condition")) attCond = " WHERE " + opt.condition;
        let select = opt.hasOwnProperty("select") ? opt.select : "*";
        let sort = (opt.hasOwnProperty("sortCondition") && opt.sortCondition) ? " ORDER BY " + opt.sortCondition : "";
        let limit = (opt.hasOwnProperty("limitCondition") && opt.limitCondition) ? opt.limitCondition : "";
        let join = (opt.hasOwnProperty("join") && opt.join) ? opt.join : "";
        let groupBy = (opt.hasOwnProperty("groupBy") ? ` GROUP BY ${opt.groupBy}` : '');
        let sql = `SELECT ${select} FROM ${this.selectViewOrTable(params, opt)} ${opt.hasOwnProperty('name') ? opt.name : ''} ${join} ${attCond} ${groupBy} ${sort} ${limit} `;
        console.log('sql3', sql);
        return await this.query(params, sql);
    }

    async countByAttributes(params, opt = {}) {
        let attCond = "";
        if (opt.hasOwnProperty("atts")) {
            for (const key in opt.atts) {
                if (attCond) attCond += " AND ";
                let isString = (typeof opt.atts[key] == "string");
                attCond += `${key} = ${isString ? '"' : ""}${opt.atts[key]}${isString ? '"' : ""}`;
            }
        }
        if (attCond) attCond = " WHERE " + attCond;
        if (opt.hasOwnProperty("condition") && opt.condition) {
            if (attCond) {
                attCond += " " + opt.condition;
            } else {
                attCond = " WHERE " + opt.condition;
            }
        }
        let sum = '';
        if (opt.hasOwnProperty("sum") && opt.sum) {
            for (const key of opt.sum) {
                sum += `,${key}`;
            }
        }
        let join = (opt.hasOwnProperty("join") && opt.join) ? opt.join : "";
        let sql = `SELECT COUNT(1) AS c ${sum} FROM ${this.selectViewOrTable(params, opt)}  ${opt.name || ''} ${join} ${attCond}`;
        console.log('sql4', sql);
        return await this.queryOne(params, sql);
    }

    async updateByAttributes(params, opt) {
        opt.atts.updateDate = this.getCurrentDate();
        let sql = `UPDATE ${this.selectViewOrTable(params, opt)} ${opt.name || ''} SET 
                ${Object.keys(opt.atts).map((key) => {
            let isString = typeof opt.atts[key] === 'string';
            return `${key} = ${isString ? "'" : ''}${opt.atts[key]}${isString ? "'" : ''}`;
        }).join(', ')}`;
        if (opt.hasOwnProperty("condition")) {
            sql += ` WHERE ${opt.condition}`;
        }
        let result = await this.query(params, sql);
        if (result.success) {
            if (this.onchange) {
                let con = {};
                if (opt.hasOwnProperty("condition")) {
                    con.condition = opt.condition
                }
                let resultAll = await this.findAll(params, con);
                if (resultAll.success) {
                    for (const resultOne of resultAll.data) {
                        await this.onchange(params, resultOne);
                    }
                }
            }
        }
        return result;
    }

    async delete(params, opt) {
        if (opt.hasOwnProperty("condition")) {
            let sql = `DELETE FROM ${this.tableName(params)} `;
            sql += ` WHERE ${opt.condition}`;
            return await this.query(params, sql);
        }
        return this.returnError("Condition is required");
    }

    isNewRecord(obj) {
        if (obj[this.pk] > 0) {
            return false;
        } else {
            return true;
        }
    }

    parseArray(field) {
        const parse = (pa) => {
            let val = this.replaceAll(pa, "[", '');
            val = this.replaceAll(val, "]", '');
            val = this.replaceAll(val, `"`, '');
            return val;
        }
        if (field !== '[]') {
            try {
                let rr = field.split(',')
                let list = [];
                if (rr.length) {
                    for (const pa of rr) {
                        list.push(parse(pa));
                    }
                } else {
                    list.push(parse(field));
                }
                if (list.length) {
                    return list;
                }
            } catch (error) {
                console.warn('error ', error);
            }
        }
        return [];
    }

    async save(params, saveData, toReturnOne = true) {
        let sql = '';
        saveData.updateDate = this.getCurrentDate();
        if (this.isNewRecord(saveData)) {
            saveData.idKey = uniqueString();
            saveData.createDate = this.getCurrentDate();
            sql = `INSERT INTO ${this.tableName(params)} ( ${Object.keys(saveData).join(',')} ) VALUES ( 
                ${Object.keys(saveData).map((key) => {
                switch (typeof saveData[key]) {
                    case 'string':
                        return `"${saveData[key] === null ? null : saveData[key] ? saveData[key] : ''}"`
                    case 'object':
                        return `${saveData[key] === null ? null : `'${JSON.stringify(saveData[key] ? saveData[key] : Array.isArray(saveData[key]) ? [] : {})}'`}`
                    default:
                        return saveData[key];
                }
            }).join(', ')} )`;
        } else {
            sql = `UPDATE ${this.tableName(params)} SET 
            ${Object.keys(saveData).filter((key) => {
                return (key !== this.pk)
            }).map((key) => {
                switch (typeof saveData[key]) {
                    case 'string':
                        return `${key} = "${saveData[key] ? saveData[key] : ''}"`
                    case 'object':
                        return `${key} = ${saveData[key] === null ? null : `'${JSON.stringify(saveData[key] ? saveData[key] : Array.isArray(saveData[key]) ? [] : {})}'`}`
                    default:
                        return `${key} = ${saveData[key]}`;
                }
            }).join(', ')}
        WHERE ${this.pk} = ${saveData[this.pk]}`;
        }
        console.log('sql5', sql);
        let result = await this.query(params, sql);
        let conPk;
        if (result.success) {
            if (this.isNewRecord(saveData)) {
                result.message = "Insert successfully";
                conPk = result._id;
            } else {
                result.message = "Update successfully";
                conPk = saveData[this.pk];
            }
            params._id = conPk;
            if ((this.onchange && conPk) || (toReturnOne && conPk)) {
                let con = {};
                con.condition = `${this.pk} = ${conPk}`;
                let resultOne = await this.findByAttributes(params, con);
                if (resultOne && resultOne.success) {
                    if (this.onchange) {
                        await this.onchange(params, resultOne.data);
                    }
                    if (toReturnOne) {
                        return resultOne;
                    }
                }
            }
        }
        return result;
    }

    async deleteByAttributes(params, opt) {
        let attCond = "";
        for (const key in opt.atts) {
            if (attCond) attCond += " AND ";
            let isString = (typeof opt.atts[key] == "string");
            attCond += `${key} = ${isString ? '"' : ""}${opt.atts[key]}${isString ? '"' : ""}`;
        }
        if (opt.hasOwnProperty("condition")) attCond += " " + opt.condition;
        if (attCond) attCond = "WHERE " + attCond;
        let sql = `DELETE FROM ${this.tableName(params)} ${attCond}`;
        return await this.query(params, sql);
    }

    async getPageList(params, opt = { condition: '', sortCondition: '', select: '*' }) {
        let pd = params.paginationData;
        if (!opt.condition) {
            opt.condition += `1`;
        }
        opt.limitCondition = ` LIMIT ${pd.perPage} OFFSET ${pd.perPage * pd.page}`;
        let resultService = await this.findAll(params, opt);
        if (resultService.success) {
            let resultCount = await this.countByAttributes(params, opt);
            var r = {
                success: true,
                totalRows: resultCount.data.c,
                page: pd.page,
                data: resultService.data,
            }
            return r;
        }
        return resultService;
    }

    async getPageListBYSQL(params, opt = { conditionList: [], select: '*' }) {
        let pd = params.paginationData;
        let sortBy = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (sortBy) sortBy += ',';
                sortBy += ` ${key} ${value} `;
            })
        }
        let condition = opt.conditionList.join(" AND ");
        let sql = `SELECT ${opt.select} FROM ${this.selectViewOrTable(params, opt)} ${opt.name || ''}`;
        sql += opt.join || '';
        sql += `  WHERE 1 ${(condition && ` AND ${condition}` || '')}`;
        sql += (sortBy && ` ORDER BY  ${sortBy}`) || '';
        sql += ` LIMIT ${pd.perPage} OFFSET ${pd.perPage * pd.page}`
        let resultData = await this.query(params, sql);
        if (resultData.success) {
            let sum = '';
            if (opt.hasOwnProperty("sum") && opt.sum) {
                for (const key of opt.sum) {
                    sum += `,${key}`;
                }
            }
            let sqlCount = `SELECT COUNT(1) AS c ${sum} FROM ${this.selectViewOrTable(params, opt)} ${opt.name || ''}`;
            sqlCount += opt.join || '';
            sqlCount += `  WHERE 1 ${(condition && ` AND ${condition}` || '')}`;
            let resultCount = await this.queryOne(params, sqlCount);
            if (!resultCount.success) {
                return resultCount;
            }
            var r = {
                success: true,
                totalRows: resultCount.data.c,
                page: pd.page,
                data: resultData.data,
            }
            return r;
        }
        return resultData;
    }

    async ordering(params, opt = { whereField: "idKey", orderField: "ordering" }) {
        let sd = params.saveData;
        let sql = `UPDATE ${this.tableName(params)} SET ${opt.orderField} = CASE`;
        let wherekesy = [];
        for (const [key, value] of Object.entries(sd)) {
            sql += ` WHEN ${opt.whereField} = "${key}" THEN ${value} `;
            wherekesy.push(`"${key}"`);
        }
        sql += `END`;
        sql += ` WHERE ${opt.whereField} IN (${wherekesy.join(',')})`;
        let result = await this.query(params, sql);
        if (result.success) {
            return this.returnSuccess();
        }
        return result;
    }

    toHHMMSS(value) {
        if (value && value !== 0) {
            let isMinus = false;
            if (String(value).startsWith('-')) {
                value = String(value).substring(1);
                isMinus = true;
            }
            var sec_num = parseInt(value, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 60);
            if (hours < 10) { hours = "0" + hours; }
            return String((isMinus ? '-' : '+') + (hours ? hours : '+00') + ':00');
        }
        return "+00:00";
    }

    async query(params, sql) {
        try {
            let connection = null;
            if (params.hasOwnProperty("connection")) {
                connection = params.connection;
            } else {
                connection = await getDB(params);
            }
            let [rows] = await connection.query(sql);
            let result = { success: true, rowCount: rows ? rows.length : 0, data: rows, };
            if (sql.startsWith("INSERT")) {
                result._id = rows.insertId;
            }
            return result;
        } catch (error) {
            console.warn('SQl Error ', error.message, '\n', sql);
            return { success: false, errMsg: error.message }
        }
    }

    async queryOne(params, sql) {
        try {
            let connection = null;
            if (params.hasOwnProperty("connection")) {
                connection = params.connection;
            } else {
                connection = await getDB(params);
            }
            let [rows] = await connection.query(sql);
            let found = (rows && rows.length > 0);
            return { success: found, rowCount: rows ? rows.length : 0, data: rows ? rows[0] : [] }
        } catch (error) {
            console.warn('SQl Error ', error.message, '\n', sql);
            return { success: false, errMsg: error.message }
        }
    }

    returnError(text) {
        // return { success: false, errMsg: text ? text : "Invalid Request Params!" };
        return { success: false, errMsg: text || "Invalid Request Params!" };
    }

    returnSuccess(text, data) {
        return { success: true, message: text ? text : "Success", data: data ? data : {} };
    }

    prepareSearch(field, str) {
        str = str.trim();
        let searchKeword = str.split("+");
        let s = '';
        for (const se of searchKeword) {
            if (se.trim()) {
                s += (s && s.length && ' AND ');
                s += `${field} LIKE '%${se.trim()}%'`;
            }
        }
        let result = s && `(${s})` || '';
        return result;
    }

    generateUuid() {
        return uuidv4();
    }

    generateUuidPDF() {
        return uuidv4() + '.pdf';
    }

    generateUuidXLS() {
        return uuidv4() + '.xlsx';
    }

    isArabic(text) {
        var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
        result = pattern.test(text);
        return result;
    }

    async changeStatusActive(param) {
        let resultGet = await this.findByAttributes(param, {
            atts: { idKey: param.search_id }
        });
        if (resultGet.success) {
            let updateResult = await this.updateByAttributes(param, {
                atts: {
                    status: this.STATUS.ACTIVE
                },
                condition: `${this.pk} = ${resultGet.data[this.pk]}`
            });
            if (updateResult.success) {
                if (this.getOne) {
                    sendUserSettings(param);
                    return await this.getOne(param);
                }
                return this.returnSuccess("Status Changed To Active successfuly.");
            }
            sendUserSettings(param);
            return updateResult;
        }
        return this.returnError("Item Not Found!");
    }

    async changeStatusBlocked(param) {
        let resultGet = await this.findByAttributes(param, {
            atts: { idKey: param.search_id }
        });
        if (resultGet.success) {
            let updateResult = await this.updateByAttributes(param, {
                atts: {
                    status: this.STATUS.INACTIVE
                },
                condition: `${this.pk} = ${resultGet.data[this.pk]}`
            });
            if (updateResult.success) {
                if (this.getOne) {
                    sendUserSettings(param);
                    return await this.getOne(param);
                }
                return this.returnSuccess("Status Changed To InActive successfuly.");
            }
            sendUserSettings(param);
            return updateResult;
        }
        return this.returnError("item Not Found!");
    }

    async insertQuery(params, saveData, toReturnOne = false) {
        let sql = '';
        saveData.updateDate = this.getCurrentDate();
        saveData.idKey = uniqueString();
        saveData.createDate = this.getCurrentDate();
        sql = `INSERT INTO ${this.tableName(params)} ( ${Object.keys(saveData).join(',')} ) VALUES ( 
            ${Object.keys(saveData).map((key) => {
            switch (typeof saveData[key]) {
                case 'string':
                    return `"${saveData[key] ? saveData[key] : ''}"`
                case 'object':
                    return `'${JSON.stringify(saveData[key] ? saveData[key] : {})}'`
                default:
                    return saveData[key];
            }
        }).join(', ')} )`;
        let result = await this.query(params, sql);
        let conPk;
        if (result.success) {
            result.message = "Insert successfully";
            conPk = result._id;
            let resultOne;
            if ((this.onchange && conPk) || (toReturnOne && conPk)) {
                let con = {};
                con.condition = `${this.pk} = ${conPk}`;
                resultOne = await this.findByAttributes(params, con);
            }
            if (resultOne && resultOne.success) {
                if (this.onchange) {
                    await this.onchange(params, resultOne.data);
                }
                if (toReturnOne) {
                    return resultOne;
                }
            }
        }
        return result;
    }

    async getDatabaseList(params) {
        let result = await this.query(params, `SHOW DATABASES`);
        let list = [];
        if (result.success) {
            list = result.data.map((oo) => {
                return oo.Database;
            }).filter((oo) => {
                return oo.toLowerCase().startsWith(process.env.DB_Name_Stracture.toLowerCase());
            })
            return { success: true, data: list };
        }
        return result;
    }

    async backUpDatabasesClient(params) {
        let resultBackup = await this.backUpDatabases(Object.assign({}, params, { search_id: params.sidClient }), false);
        return resultBackup;
    }

    async backUpDatabases(params, zip) {
        let dat = moment().format("DD-MM-YYYY_HH-MM-SS").toString();
        if (params && params.search_id) {
            let db = `${process.env.DB_Name_Stracture}_${params.search_id}`.toLowerCase();
            let resu = await backUpDb(dat, db, zip);
            return resu
        } else {
            let result = await this.getDatabaseList(params);
            if (result.success) {
                let list = result.data;
                let bol = true;
                for (const db of list) {
                    let resu = await backUpDb(dat, db, zip);
                    bol &= resu.success;
                }
                return { success: bol ? true : false, date: dat, data: result.data };
            }
        }
        return this.returnError()
    }

    async canDelete(param) {
        return { success: true, errMsg: "Cant Delete" }
    }

    async deleteByIdKey(param) {
        let resultCanDelete = await this.canDelete(param);
        if (resultCanDelete.success) {
            return await this.deleteByAttributes(param, {
                atts: { idKey: param.search_id },
            });
        }
        return resultCanDelete;
    }
}

export default DbManager;