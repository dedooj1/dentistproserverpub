import DbManagerCustom from './DbManagerCustom';

class DbPrinter extends DbManagerCustom {
    constructor() {
        super("tbl_printer");
        this.pk = "idPrinter";
        this.noDublicateCol = ['printerName'];
    }

    async createTableSQL(params) {
        const sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
                        ${this.pk} int(11) NOT NULL,
                        idKey varchar(50) NOT NULL,
                        printerName varchar(255) NOT NULL,
                        printerPath varchar(255) DEFAULT NULL,
                        printerCopies int(11) NOT NULL DEFAULT 1,
                        createDate timestamp NOT NULL DEFAULT current_timestamp(),
                        updateDate datetime DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
        return sql;
    }

    async insert(params) {
        let sd = params.saveData;
        let obj = {
            printerName: sd.printerName,
            printerPath: sd.printerPath ? sd.printerPath : '',
            printerCopies: sd.printerCopies ? sd.printerCopies : 1,
        }
        let resultInsert = await this.save(params, obj, true);
        return resultInsert;
    }

    async update(params) {
        let result = await this.findByAttributes(params, {
            atts: { idKey: params.search_id }
        });
        if (result.success) {
            let sd = params.saveData;
            let obj = {
                idPrinter: result.data.idPrinter,
                printerName: sd.printerName,
                printerPath: sd.printerPath,
                printerCopies: sd.printerCopies ? sd.printerCopies : 1,
            }
            let resultUpdate = await this.save(params, obj, true);
            return resultUpdate;
        }
        return this.returnError("Printer Not Found!");
    }

    async getOne(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idKey: params.search_id
            },
            select: `*`
        });
        if (resultOne.success) {
            return resultOne;
        }
        return this.returnError("Printer Not Found!");
    }

    async getOneByPk(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idPrinter: params._id ? params._id : params.search_id
            },
            select: `*`,
            view: true,
        });
        if (resultOne.success) {
            return resultOne;
        }
        return this.returnError("Printer Not Found!");
    }

    async list(params) {
        let pd = params.paginationData;
        let allowSortFilds = ["printerName", "createDate"];
        let conditionList = [];
        if (pd.hasOwnProperty("q") && pd.q.trim()) {
            conditionList.push(`(printerName LIKE "%${pd.q.trim()}%")`);
        }
        if (pd.hasOwnProperty("filter")) {
            if (pd.filter.hasOwnProperty("idBranch") && pd.filter.idBranch) {
                conditionList.push(`idBranch = ${pd.filter.idBranch}`);
            }
        }
        let condition = conditionList.join(" AND ");
        let sortCondition = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
                    if (sortCondition) sortCondition += ',';
                    sortCondition += ` ${key} ${value} `;
                }
            })
        }
        let select = `*`;
        return await this.getPageList(params, { select, condition, sortCondition });
    }

    async fillAll(params) {
        return await this.findAll(params, {
            // condition: `status = ${this.STATUS.ACTIVE}`,
        });
    }
}

export default new DbPrinter;