import { renameFile } from '../../utils/FileFunctions';
import DbManagerCustom from './DbManagerCustom';
import DbPlan from './DbPlan';
import DbUser from './DbUser';

class DbPatients extends DbManagerCustom {
  constructor() {
    super("tbl_patient");
    this.pk = "idPatient";
    // this.noDublicateCol = ['patientName'];
    // this.TYPE = {
    //   NOT_SHARED: 1,
    //   SHARED: 2,
    // }
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            patientName varchar(255) NOT NULL,
            email TEXT NULL,
            phoneCode varchar(255) DEFAULT NULL,
            phoneNumber varchar(255) DEFAULT NULL,
            Generd int(1) DEFAULT NULL,
            status int(1) DEFAULT NULL,
            description TEXT DEFAULT NULL,
            idDoctor int(11) DEFAULT NULL,
            shareDoctor TEXT DEFAULT NULL,
            icon varchar(255) DEFAULT NULL,
            birthDate timestamp NULL DEFAULT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let tokenUser = params.tokenUser;
    let obj = {
      patientName: sd.patientName,
      email: sd.email || null,
      phoneCode: sd.phoneCode || "+963",
      phoneNumber: sd.phoneNumber || null,
      Generd: sd.Generd || null,
      idDoctor: sd.idDoctor || tokenUser.idUser,
      shareDoctor: sd.shareDoctor || [],
      status: this.STATUS.ACTIVE,
      description: sd.description || null,
      // birthDate: sd.birthDate ? this.formatSQLDate(sd.birthDate) : null,
      birthDate: sd.birthDate || null,
      icon: '',
      // type: this.TYPE.NOT_SHARED,
    }
    if (sd.hasOwnProperty("icon") && sd.icon) {
      sd.icon = sd.icon.replace('public/tmp/', 'root/asnan/public/tmp/');
      obj.icon = await renameFile(sd.icon, 'root/asnan/public/uploads/usericon');
      obj.icon = obj.icon.replace('root/asnan/', '');
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;
      // let type = this.TYPE.NOT_SHARED;

      // if (sd.shareDoctor) {
      //   if (sd.shareDoctor.length > 0) {
      //     type = this.TYPE.SHARED;
      //   }
      // }

      let obj = {
        idPatient: result.data.idPatient,
        patientName: sd.patientName,
        email: sd.email || null,
        phoneCode: sd.phoneCode || "+963",
        phoneNumber: sd.phoneNumber || null,
        Generd: sd.Generd || null,
        shareDoctor: sd.shareDoctor || [],
        description: sd.description || null,
        // birthDate: sd.birthDate ? this.formatSQLDate(sd.birthDate) : null,
        birthDate: sd.birthDate || null,
        icon: '',
        // type: type,

      }
      if (sd.hasOwnProperty("icon") && sd.icon) {
        sd.icon = sd.icon.replace('public/tmp/', 'root/asnan/public/tmp/');
        obj.icon = await renameFile(sd.icon, 'root/asnan/public/uploads/usericon');
        console.log(sd.icon);
        console.log(obj.icon);
        obj.icon = obj.icon.replace('root/asnan/', '');
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Patient Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*, TIMESTAMPDIFF( YEAR, birthDate, now() ) as year, TIMESTAMPDIFF( MONTH, birthDate, now() ) % 12 as month`
    });
    if (resultOne.success) {
      resultOne.data.shareDoctor = JSON.parse(resultOne.data.shareDoctor);
      let resultPlan = await DbPlan.getCount(params, {
        condition: `idPatient= ${resultOne.data.idPatient}`
      })
      resultOne.data.plansCount = resultPlan.data.c
      return resultOne;
    }
    return this.returnError("Patient Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["patientName", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(patientName LIKE "%${pd.q.trim()}%" OR description LIKE "%${pd.q.trim()}%")`);
    }
    if (params.sidRole !== DbUser.ROLE.SUPERADMIN && params.sidRole !== DbUser.ROLE.ADMIN) {
      conditionList.push(`(idDoctor = ${tokenUser.idUser} OR shareDoctor LIKE "%${tokenUser.idUser}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate.trim()) {
        conditionList.push(`createDate >= ${this.formatSQLDate(pd.filter.fromDate)}`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= ${this.formatSQLDate(pd.filter.toDate)}`);
      }

      if (pd.filter.hasOwnProperty("idDoctor") && pd.filter.idDoctor) {
        conditionList.push(`idDoctor = "${pd.filter.idDoctor}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*, TIMESTAMPDIFF( YEAR, birthDate, now() ) as year, TIMESTAMPDIFF( MONTH, birthDate, now() ) % 12 as month`;
    // let select = `${this.pk}, idKey, patientName, description`;

    const result = await this.getPageList(params, { select, condition, sortCondition });


    let resultPlan;
    for (let val in result.data) {
      resultPlan = await DbPlan.getCount(params, {
        condition: `idPatient= ${result.data[val].idPatient}`
      })
      result.data[val].plansCount = resultPlan.data.c
      result.data[val].shareDoctor = JSON.parse(result.data[val].shareDoctor);
    }
    return result
  }

  async get_all(params) {
    let tokenUser = params.tokenUser;
    let result = await this.findAllByAttributes(params, {
      atts: { idDoctor: tokenUser.idUser },
      select: `*, TIMESTAMPDIFF( YEAR, birthDate, now() ) as year, TIMESTAMPDIFF( MONTH, birthDate, now() ) % 12 as month`,
      condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    });
    result.data.map(p => {
      p.shareDoctor = JSON.parse(p.shareDoctor);
    })
    return result;
  }

  async getCount(params, ats = {}) {
    return await this.countByAttributes(params, ats);
  }
  async delete(params) {
    let resultGet = await this.get_one_by_idkey(params, params.search_id);
    if (resultGet.success) {
      if (resultGet.data.idDoctor === params.sidUser || params.sidRole === DbUser.ROLE.SUPERADMIN || params.sidRole === DbUser.ROLE.ADMIN) {
        let idPatient = resultGet.data.idPatient;
        let resultPlans = await DbPlan.findAllByAttributes(params, {
          atts: { idPatient: idPatient },
        })
        if (resultPlans.success) {
          if (resultPlans.rowCount === 0) {
            if (resultGet.data.icon) {
              await deleteFileByPath(resultGet.data.icon);
            }
            await this.deleteByAttributes(params, {
              atts: { idPatient: idPatient }
            });
            return this.returnSuccess();
          } else {
            return this.returnError("Please Delete Patient from " + resultPlans.data[0].planName);
          }
        } else {
          return this.returnError("Patient Not Found");
        }
      } else {
        return this.returnError("You don't have permission to delete patient");
      }
    }
    return this.returnError("Patient Not Found");
  }
}

export default new DbPatients;