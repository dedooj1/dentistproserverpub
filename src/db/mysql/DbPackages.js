import DbGeoCountries from './DbGeoCountries';
import DbManagerMain from './DbManagerMain';

class DbPackages extends DbManagerMain {
    constructor() {
        super("ard_packages", "view_ard_packages");
        this.pk = "idPackage";
        this.noDublicateCol = [];
    }

    async get_all(params) {
        let idCurrency = 11;
        if (params.hasOwnProperty("ip") && params.ip) {
            let countryResult = await DbGeoCountries.getInfoFromIp(params, params.ip);
            if (countryResult.success) {
                idCurrency = countryResult.data.idCurrency;
            }
        }
        return await this.findAllByAttributes(params, {
            atts: {
                status: 1,
                idCurrency: idCurrency,
            },
            sort: "ordering",
            // view: true,
        });
    }


}
export default new DbPackages;