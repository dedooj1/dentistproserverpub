import DbUser from './DbUser';
import { DEFAULT_TOKEN_VALID_HOUR, isLocal } from '../../utils';
import uniqueString from 'unique-string';
import moment from 'moment';
import DbPackages from './DbPackages';
import DbClients from './DbClients';
import DbCountries from './DbCountries';
import DbManagerMain from './DbManagerMain';

class DbUserToken extends DbManagerMain {
    constructor() {
        super("ard_users_token");
        this.pk = "idToken";
        this.noDublicateCol = [];
    }

    async clearExpiredTokens(params) {
        let sql = `DELETE FROM ${this.tableName(params)} WHERE expireDate < '${this.getCurrentDate()}'`;
        return await this.query(params, sql);
    }

    async clearThisUserTokens(params, idUser, idClient) {
        //let sql = `DELETE FROM ${this.tableName} WHERE idUser = ${idUser} AND idClient = ${idClient}`;
        return await this.delete(params, {
            condition: `idUser = ${idUser} AND idClient = ${idClient}`
        });
        // return await this.query(sql);
    }

    async createUserToken(params, idUser, idClient) {
        if (!isLocal()) {
            // let resultDelete = await this.clearExpiredTokens();
            let resultDeleteToken = await this.clearThisUserTokens(params, idUser, idClient);
            if (!resultDeleteToken.success) {
                // return  this.returnError(resultDeleteToken);
                // return this.returnError("Error While Deleting Tokens");
            }
        }
        let userTokenObj = {
            device: params.device,
            deviceKey: params.device == "mobile" ? params.device_key : null,
            accessToken: uniqueString(),
            idUser: idUser,
            idClient: idClient,
            expireDate: this.formatSQLDateMovement(moment().add(DEFAULT_TOKEN_VALID_HOUR, 'HOUR')),
        }
        let resultToken = await this.save(params, userTokenObj);
        if (!resultToken.success) {
            return resultToken;
        }
        return { success: true, data: userTokenObj.accessToken };
    }

    async expandUserToken(params, accessToken) {
        let resultUpdate = await this.updateByAttributes(params, {
            atts: {
                expireDate: this.formatSQLDateMovement(moment().add(DEFAULT_TOKEN_VALID_HOUR, 'HOUR')),
            },
            condition: `accessToken = '${accessToken}'`
        });
        return resultUpdate;
    }

    async getUserByToken(params, token) {
        let sql = `SELECT 
                    u.idUser, 
                    u.idKey, 
                    u.idClient, 
                    u.fullName, 
                    u.idRole,
                    u.idBranch,
                    u.lang,
                    tok.accessToken
                    FROM ${this.tableName(params)} tok 
                    LEFT JOIN ${DbUser.tableName(params)} u ON u.idUser = tok.idUser
                    WHERE accessToken = '${token}' AND expireDate > '${this.getCurrentDate()}'`;
        let result = await this.queryOne(params, sql);
        if (result.success) {
            result.data.client = {};
            result.data.client.package = { allowUserCount: 1 };
            let resultClient = await DbClients.get_one(params, result.data.idClient);
            if (resultClient.success) {
                result.data.client = resultClient.data;
                let resultPackage = await DbPackages.get_one(params, result.data.client.idPackage);
                if (resultPackage.success) {
                    result.data.client.package = resultPackage.data;
                }
                let resultCountry = await DbCountries.get_one(params, result.data.client.idCountry);
                if (resultCountry.success) {
                    result.data.client.country = resultCountry.data;
                }
            }
        }
        return result;
    }

    /********************************************************** API ********************************************************************** */

    async getApiAccessToken(params, idClient, idUser) {
        let resultAccessToken = await this.findByAttributes(params, {
            atts: {
                idClient: idClient,
                idUser: idUser,
                device: 'api',
            },
        });
        if (!resultAccessToken.success) {
            let userTokenObj = {
                device: 'api',
                deviceKey: null,
                accessToken: uniqueString(),
                idUser: idUser,
                idClient: idClient,
                expireDate: this.formatSQLDate(moment().add(DEFAULT_TOKEN_VALID_HOUR, 'HOUR')),
            }
            let resultToken = await this.save(params, userTokenObj)
            if (resultToken.success) {
                return { success: true, data: userTokenObj.accessToken };
            }
            return resultToken;
        } else {
            await this.updateByAttributes(params, {
                atts: {
                    expireDate: this.formatSQLDate(moment().add(DEFAULT_TOKEN_VALID_HOUR, 'HOUR')),
                },
                condition: `idToken = ${resultAccessToken.data.idToken}`
            })
        }
        return { success: true, data: resultAccessToken.data.accessToken };
    }

    async clearThisDeviceTokens(params, deviceKey, idClient) {
        return await this.delete(params, {
            condition: `deviceKey = '${deviceKey}' AND idClient = ${idClient}`
        });
    }

}

export default new DbUserToken;