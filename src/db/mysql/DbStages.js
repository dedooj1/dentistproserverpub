import DbManagerCustom from './DbManagerCustom';

class DbStages extends DbManagerCustom {
  constructor() {
    super("tbl_stages");
    this.pk = "idStage";
    // this.noDublicateCol = ['stageName'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            stageName varchar(255) NOT NULL,
            description TEXT DEFAULT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let obj = {
      stageName: sd.stageName,
      description: sd.description,
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;

      let obj = {
        idStage: result.data.idStage,
        stageName: sd.stageName,
        description: sd.description,
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Stage Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      return resultOne;
    }
    return this.returnError("Stage Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["stageName", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(stageName LIKE "%${pd.q.trim()}%" OR description LIKE "%${pd.q.trim()}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    return await this.getPageList(params, { select, condition, sortCondition });
  }

  // async get_all(params) {
  //   // let tokenUser = params.tokenUser;
  //   // let result = await this.findAllByAttributes(params, {
  //   //   atts: { idStage: tokenUser.idUser },
  //   //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
  //   // });
  //   let result = await this.findAll(params);
  //   result.data.map(p => {
  //     p.optionList = JSON.parse(p.optionList);
  //   })
  //   return result;
  // }

  async getCount(params) {
    return await this.countByAttributes(params, {});
  }

  async delete(params) {
    let resultGet = await this.get_one_by_idkey(params, params.search_id);
    if (resultGet.success) {
      let idStage = resultGet.data.idStage;
      if (resultGet.data.icon) {
        await deleteFileByPath(resultGet.data.icon);
      }
      await this.deleteByAttributes(params, {
        atts: { idStage: idStage }
      });
      return this.returnSuccess();
    }
    return this.returnError("Stages Not Found");
  }
}

export default new DbStages;