import DbManagerCustom from './DbManagerCustom';

class DbBranchs extends DbManagerCustom {
    constructor() {
        super("tbl_branchs");
        this.pk = "idBranch";
        this.noDublicateCol = ['branchName'];
    }

    async createTableSQL(params) {
        let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            branchName varchar(255) NOT NULL,
            branchDescription TEXT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
        return sql;
    }

    async insert(params) {
        let sd = params.saveData;
        let allowBranchCount = params.tokenUser.client.package.branchLimitCount;
        let getCount = await this.getCount(params);
        let clientBranchCount = getCount.success ? (getCount.data.c) : 1000;
        if (clientBranchCount < allowBranchCount) {
            let obj = {
                branchName: sd.branchName,
                branchDescription: sd.branchDescription,
            }
            let resultInsert = await this.save(params, obj, true);
            return resultInsert;
        }
        return this.returnError(`Can't create new Branch. Please change your package! Your limit is (${+allowBranchCount})`);
    }

    async update(params) {
        let result = await this.findByAttributes(params, {
            atts: { idKey: params.search_id }
        });
        if (result.success) {
            let sd = params.saveData;
            let obj = {
                idBranch: result.data.idBranch,
                branchName: sd.branchName,
                branchDescription: sd.branchDescription,
            }
            let resultUpdate = await this.save(params, obj, true);
            return resultUpdate;
        }
        return this.returnError("Branch Not Found!");
    }

    async getOne(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idKey: params.search_id
            },
            select: `idKey, branchName, branchDescription`
        });
        if (resultOne.success) {
            return resultOne;
        }
        return this.returnError("Branch Not Found!");
    }

    async list(params) {
        let pd = params.paginationData;
        let allowSortFilds = ["branchName", "createDate"];

        let conditionList = [];
        if (pd.hasOwnProperty("q") && pd.q.trim()) {
            conditionList.push(`(branchName LIKE "%${pd.q.trim()}%" OR branchDescription LIKE "%${pd.q.trim()}%")`);
        }
        // if (pd.hasOwnProperty("filter")) {
        //     if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        //         conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
        //     }

        //     if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        //         conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
        //     }
        // }

        let condition = conditionList.join(" AND ");
        let sortCondition = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
                    if (sortCondition) sortCondition += ',';
                    sortCondition += ` ${key} ${value} `;
                }
            })
        }

        let select = `${this.pk}, idKey, branchName, branchDescription`;

        return await this.getPageList(params, { select, condition, sortCondition });
    }

    async getCount(params) {
        return await this.countByAttributes(params, {});
    }

}

export default new DbBranchs;