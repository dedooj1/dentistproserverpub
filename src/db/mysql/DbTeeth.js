
import { renameFile } from '../../utils/FileFunctions';
import { sendUserSettings } from '../../utils/SocketActions';
import DbManagerCustom from './DbManagerCustom';

class DbTeeth extends DbManagerCustom {
  constructor() {
    super("tbl_teeths");
    this.pk = "idTeeths";
    this.noDublicateCol = ['teethNameNumber', 'teethName1', 'teethName2', 'teethName3', 'teethName4', 'teethName5'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            teethNameNumber varchar(255) NOT NULL,
            teethName1 varchar(255) NOT NULL,
            teethName2 varchar(255) NULL,
            teethName3 varchar(255) NULL,
            teethName4 varchar(255) NULL,
            teethName5 varchar(255) NULL,
            DiagnosticReflectionList TEXT NULL,
            TreatmentReflectionList TEXT NULL,
            position TEXT NULL,
            TeethImage varchar(255) DEFAULT NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let obj = {
      teethNameNumber: sd.teethNameNumber || 1,
      teethName1: sd.teethName1 || null,
      teethName2: sd.teethName2 || null,
      teethName3: sd.teethName3 || null,
      teethName4: sd.teethName4 || null,
      teethName5: sd.teethName5 || null,
      DiagnosticReflectionList: sd.DiagnosticReflectionList || [],
      TreatmentReflectionList: sd.TreatmentReflectionList || [],
      position: sd.position || [],
    }

    if (sd.hasOwnProperty("TeethImage") && sd.TeethImage) {
      sd.TeethImage = sd.TeethImage.replace('public/tmp/', 'root/asnan/public/tmp/');
      obj.TeethImage = await renameFile(sd.TeethImage, 'root/asnan/public/uploads/usericon');
      obj.TeethImage = obj.TeethImage.replace('root/asnan/', '');
    }

    if (sd.hasOwnProperty("DiagnosticReflectionList") && sd.DiagnosticReflectionList) {
      for (let dr in sd.DiagnosticReflectionList) {
        if (sd.DiagnosticReflectionList[dr].hasOwnProperty("DiagnosticReflectionImage") && sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage) {
          sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage.replace('public/tmp/', 'root/asnan/public/tmp/');
          sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = await renameFile(sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage, 'root/asnan/public/uploads/usericon');
          sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage.replace('root/asnan/', '');
        }
      }
      obj.DiagnosticReflectionList = sd.DiagnosticReflectionList
    }
    if (sd.hasOwnProperty("TreatmentReflectionList") && sd.TreatmentReflectionList) {
      for (let dr in sd.TreatmentReflectionList) {
        if (sd.TreatmentReflectionList[dr].hasOwnProperty("TreatmentReflectionImage") && sd.TreatmentReflectionList[dr].TreatmentReflectionImage) {
          sd.TreatmentReflectionList[dr].TreatmentReflectionImage = sd.TreatmentReflectionList[dr].TreatmentReflectionImage.replace('public/tmp/', 'root/asnan/public/tmp/');
          sd.TreatmentReflectionList[dr].TreatmentReflectionImage = await renameFile(sd.TreatmentReflectionList[dr].TreatmentReflectionImage, 'root/asnan/public/uploads/usericon');
          sd.TreatmentReflectionList[dr].TreatmentReflectionImage = sd.TreatmentReflectionList[dr].TreatmentReflectionImage.replace('root/asnan/', '');
        }
      }
      obj.TreatmentReflectionList = sd.TreatmentReflectionList
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;
      let obj = {
        idTeeths: result.data.idTeeths,
        teethNameNumber: sd.teethNameNumber || 1,
        teethName1: sd.teethName1 || null,
        teethName2: sd.teethName2 || null,
        teethName3: sd.teethName3 || null,
        teethName4: sd.teethName4 || null,
        teethName5: sd.teethName5 || null,
        DiagnosticReflectionList: sd.DiagnosticReflectionList || [],
        TreatmentReflectionList: sd.TreatmentReflectionList || [],
        position: sd.position || [],
      }
      if (sd.hasOwnProperty("TeethImage") && sd.TeethImage) {
        sd.TeethImage = sd.TeethImage.replace('public/tmp/', 'root/asnan/public/tmp/');
        obj.TeethImage = await renameFile(sd.TeethImage, 'root/asnan/public/uploads/usericon');
        obj.TeethImage = obj.TeethImage.replace('root/asnan/', '');
      }

      if (sd.hasOwnProperty("DiagnosticReflectionList") && sd.DiagnosticReflectionList) {
        for (let dr in sd.DiagnosticReflectionList) {
          if (sd.DiagnosticReflectionList[dr].hasOwnProperty("DiagnosticReflectionImage") && sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage) {
            sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage.replace('public/tmp/', 'root/asnan/public/tmp/');
            sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = await renameFile(sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage, 'root/asnan/public/uploads/usericon');
            sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage = sd.DiagnosticReflectionList[dr].DiagnosticReflectionImage.replace('root/asnan/', '');
          }
        }
        obj.DiagnosticReflectionList = sd.DiagnosticReflectionList
      }
      if (sd.hasOwnProperty("TreatmentReflectionList") && sd.TreatmentReflectionList) {
        console.log('in1');
        for (let dr in sd.TreatmentReflectionList) {
          console.log('in2');
          console.log(dr);
          if (sd.TreatmentReflectionList[dr].hasOwnProperty("TreatmentReflectionImage") && sd.TreatmentReflectionList[dr].TreatmentReflectionImage) {
            console.log('in3');
            sd.TreatmentReflectionList[dr].TreatmentReflectionImage = sd.TreatmentReflectionList[dr].TreatmentReflectionImage.replace('public/tmp/', 'root/asnan/public/tmp/');
            sd.TreatmentReflectionList[dr].TreatmentReflectionImage = await renameFile(sd.TreatmentReflectionList[dr].TreatmentReflectionImage, 'root/asnan/public/uploads/usericon');
            sd.TreatmentReflectionList[dr].TreatmentReflectionImage = sd.TreatmentReflectionList[dr].TreatmentReflectionImage.replace('root/asnan/', '');
            console.log(sd.TreatmentReflectionList[dr].TreatmentReflectionImage);
          }
        }
        obj.TreatmentReflectionList = sd.TreatmentReflectionList
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("teeth Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.DiagnosticReflectionList = JSON.parse(resultOne.data.DiagnosticReflectionList);
      resultOne.data.TreatmentReflectionList = JSON.parse(resultOne.data.TreatmentReflectionList);
      resultOne.data.position = JSON.parse(resultOne.data.position);
      return resultOne;
    }
    return this.returnError("teeth Not Found!");
  }

  async getOneByType(params) {
    let resultOne = await this.findAll(params, {
      condition: `type= ${params.search_id}`,
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.map(val => {
        val.DiagnosticReflectionList = JSON.parse(val.DiagnosticReflectionList);
        val.TreatmentReflectionList = JSON.parse(val.TreatmentReflectionList);
        val.position = JSON.parse(val.position);
        return val
      })
      return resultOne;
    }
    return this.returnError("teeth Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["teethNameNumber", "teethName1", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(teethName1 LIKE "%${pd.q.trim()}%" OR teethName2 LIKE "%${pd.q.trim()}%" OR teethName3 LIKE "%${pd.q.trim()}%" OR teethNameNumber LIKE "%${pd.q.trim()}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }

      if (pd.filter.hasOwnProperty("type") && pd.filter.type) {
        conditionList.push(`type = "${pd.filter.type}"`);
      }

      if (pd.filter.hasOwnProperty("status") && pd.filter.status) {
        conditionList.push(`status = "${pd.filter.status}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition });
    console.log(result);
    result.data.map((p, index) => {
      // console.log('without parse', p.DiagnosticReflectionList);
      // console.log('without parse type', typeof p.DiagnosticReflectionList);
      // console.log('without parse string', p.DiagnosticReflectionList);
      // console.log('without parse string type', typeof p.DiagnosticReflectionList);
      // console.log('with parse', JSON.parse(p.DiagnosticReflectionList));
      // console.log('with parse string', JSON.parse(p.DiagnosticReflectionList));
      result.data[index].DiagnosticReflectionList = JSON.parse(p.DiagnosticReflectionList);
      result.data[index].TreatmentReflectionList = JSON.parse(p.TreatmentReflectionList);
      result.data[index].position = JSON.parse(p.position);
    })
    return result
  }

  async get_all(params) {
    // let tokenUser = params.tokenUser;
    // let result = await this.findAllByAttributes(params, {
    //   atts: { idTeeths: tokenUser.idUser },
    //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    // });
    let result = await this.findAll(params);
    result.data.map(p => {
      p.DiagnosticReflectionList = JSON.parse(p.DiagnosticReflectionList);
      p.TreatmentReflectionList = JSON.parse(p.TreatmentReflectionList);
      p.position = JSON.parse(p.position);
    })
    return result;
  }

  async getCount(params) {
    return await this.countByAttributes(params, {});
  }

  async delete(params) {
    let resultGet = await this.get_one_by_idkey(params, params.search_id);
    if (resultGet.success) {
      let idTeeths = resultGet.data.idTeeths;
      await this.deleteByAttributes(params, {
        atts: { idTeeths: idTeeths }
      });
      return this.returnSuccess();
    }
    return this.returnError("Teeth Not Found");
  }

}
export default new DbTeeth;