import uniqueString from 'unique-string';
import DbClients from './DbClients';
import DbCountries from './DbCountries';
import DbUserToken from './DbUserToken';
import { sendEmailUserActivation, sendEmailForgotPassword } from '../../utils/Mailer';
import { sendClientRegistration } from '../../utils/SlackApi';
import { renameFile } from '../../utils/FileFunctions';
import DbCurrency from './DbCurrency';
import DbSettings from './DbSettings';
import DbUsersPolicy from './DbUsersPolicy';
import DbManagerMain from './DbManagerMain';
import DbPlan from './DbPlan';
import DbPatients from './DbPatients';

class DbUser extends DbManagerMain {
    constructor() {
        super("ard_users", "view_ard_users");
        this.ROLE = {
            SUPERADMIN: 1,
            ADMIN: 2,
            DOCTOR: 3,
            DRIVER: 4,
        }
        this.STATUS = {
            ACTIVE: 1,
            BLOCKED: 2,
        }
        this.DIRECTION = {
            LTR: 'ltr',
            RTL: 'rtl',
        }
        this.pk = "idUser";
        this.noDublicateCol = ['email', 'login'];
    }

    async addAdmin(params) {
        if (params.sidRole == this.ROLE.SUPERADMIN) {
            let sd = params.saveData;
            let resultCountry = await DbCountries.findByPk(params, { id: sd.idCountry });
            if (!resultCountry.success) {
                return this.returnError("Country not found!");
            }
            let clientObj = {
                idCountry: sd.idCountry,
                idCurrency: resultCountry.data.idCurrency,
                phoneCode: sd.phoneCode || '+963',
                phoneNumber: sd.phoneNumber,
                status: DbClients.STATUS.ACTIVE,
                idPackage: sd.hasOwnProperty("idPackage") ? sd.idPackage : 1,
                name: sd.name,
                email: sd.email,
            };
            let resultClient = await DbClients.save(params, clientObj, false);
            if (!resultClient.success) {
                // return this.returnError("Error while creating Client");
                return this.returnError(resultClient.errMsg);
            }
            let userObj = {
                idClient: resultClient._id,
                email: sd.email,
                fullName: sd.fullNameAdmin || sd.name,
                phoneCode: sd.phoneCode,
                phoneNumber: sd.phoneNumber,
                password: sd.password,
                login: sd.email,
                idRole: this.ROLE.ADMIN,
                // userLevel: 5,
                status: this.STATUS.ACTIVE,
                token: uniqueString(),
                lang: resultCountry.data.countryLang || 'en',
                TeethNameType: sd.TeethNameType || 1 ,
                direction: this.DIRECTION.LTR,
                isEmailConfirmed: 1,
                idBranch: 1,
            };
            let resultUser = await this.save(params, userObj);
            if (!resultUser.success) {
                return this.returnError(resultUser.errMsg);
            }
            // await sendEmailUserActivation(userObj.email, userObj.token, userObj, clientObj);
            // return { success: resultUser.success, token: userObj.token };
            // if (!resultUser.data.isEmailConfirmed) {
            params.sidClient = resultUser.data.idClient;
            params.sidUser = resultUser.data.idUser;
            let resultDbCreate = await DbClients.createDb(params);
            if (resultDbCreate.success) {
                let token = uniqueString();
                let updateResultUser = await this.updateByAttributes(params, {
                    atts: {
                        token: token,
                        idBranch: 1,
                        idPolicy: 1,
                    },
                    condition: `idUser = ${resultUser.data.idUser}`
                });
                if (updateResultUser.success) {
                    await DbClients.setDefaultData(params);
                    await sendClientRegistration(params, resultUser.data.idClient);
                    return { success: true, data: { magicToken: token } }
                }
                return resultUser;
            }
            // }
            return this.returnError("Error in create db");
        }
        return this.returnError("You not have permission in this action!");
    }
    // async register(params) {
    //     let sd = params.saveData;
    //     let resultCountry = await DbCountries.findByPk(params, { id: sd.idCountry });
    //     if (!resultCountry.success) {
    //         return this.returnError("Country not found!");
    //     }
    //     let clientObj = {
    //         idCountry: sd.idCountry,
    //         idCurrency: resultCountry.data.idCurrency,
    //         phoneCode: sd.phoneCode,
    //         phoneNumber: sd.phoneNumber,
    //         status: DbClients.STATUS.PENDING,
    //         idPackage: sd.hasOwnProperty("idPackage") ? sd.idPackage : 1,
    //         name: sd.name,
    //         email: sd.email,
    //     };
    //     let resultClient = await DbClients.save(params, clientObj, false);
    //     if (!resultClient.success) {
    //         // return this.returnError("Error while creating Client");
    //         return this.returnError(resultClient.errMsg);
    //     }
    //     let userObj = {
    //         idClient: resultClient._id,
    //         email: sd.email,
    //         fullName: sd.fullNameAdmin || sd.name,
    //         phoneCode: sd.phoneCode,
    //         phoneNumber: sd.phoneNumber,
    //         password: sd.password,
    //         login: sd.email,
    //         idRole: this.ROLE.SUPERADMIN,
    //         // userLevel: 5,
    //         status: this.STATUS.ACTIVE,
    //         token: uniqueString(),
    //         lang: resultCountry.data.countryLang,
    //         direction: this.DIRECTION.LTR,
    //     };
    //     let resultUser = await this.save(params, userObj);
    //     if (!resultUser.success) {
    //         return this.returnError(resultUser.errMsg);
    //     }
    //     await sendEmailUserActivation(userObj.email, userObj.token, userObj, clientObj);
    //     return { success: resultUser.success, token: userObj.token };
    // }

    async confirm(params) {
        let resultUser = await this.findByAttributes(params, { atts: { token: params.search_id } });
        if (resultUser.success) {
            if (!resultUser.data.isEmailConfirmed) {
                params.sidClient = resultUser.data.idClient;
                params.sidUser = resultUser.data.idUser;
                let resultDbCreate = await DbClients.createDb(params);
                if (resultDbCreate.success) {
                    let token = uniqueString();
                    let updateResultUser = await this.updateByAttributes(params, {
                        atts: {
                            token: token,
                            idBranch: 1,
                            idPolicy: 1,
                        },
                        condition: `idUser = ${resultUser.data.idUser}`
                    });
                    if (updateResultUser.success) {
                        await DbClients.setDefaultData(params);
                        await sendClientRegistration(params, resultUser.data.idClient);
                        return { success: true, data: { magicToken: token } }
                    }
                }
                return resultDbCreate;
            }
            return this.returnError("Email already Confirmed!");
        }
        return this.returnError("Invalid Activation Key!");
    }

    async forgotPassword(params) {
        let resultUser = await this.findByAttributes(params, {
            atts: {
                login: params.email,
            }
        });
        if (resultUser.success) {
            if (resultUser.data.isEmailConfirmed) {
                let token = uniqueString();
                let updateResultUser = await this.updateByAttributes(params, {
                    atts: {
                        token: token,
                    },
                    condition: `idUser = ${resultUser.data.idUser}`
                });
                if (updateResultUser.success) {
                    await sendEmailForgotPassword(resultUser.data.email, token, resultUser.data);
                    return this.returnSuccess("Email Sended");
                }
                return this.returnError();
            }
            return this.returnError("Please confirm your email!");
        }
        return this.returnError("This email not registered");
    }

    async changePasswordForgot(params) {
        let resultUser = await this.findByAttributes(params, { atts: { token: params.search_id } });
        if (resultUser.success) {
            let updateResultUser = await this.updateByAttributes(params, {
                atts: {
                    password: params.saveData.password,
                    token: null,
                },
                condition: `idUser = ${resultUser.data.idUser}`
            });
            if (updateResultUser.success) {
                return this.returnSuccess("Password changed");
            }
            return this.returnError();
        }
        return this.returnError("Invalid Token!");
    }

    async magicLogin(params) {
        let resultUser = await this.findByAttributes(params, { atts: { token: params.search_id } });
        if (resultUser.success) {
            params.login = resultUser.data.login;
            params.password = resultUser.data.password;
            let updateResultUser = await this.updateByAttributes(params, {
                atts: {
                    token: null,
                },
                condition: `idUser = ${resultUser.data.idUser}`
            });

            if (updateResultUser.success) {
                return await this.login(params);
            }
            return updateResultUser;
        }
        return this.returnError("Invalid Token!");
    }

    async login(params) {
        let resultUser = await this.findByAttributes(params, {
            atts: {
                login: params.login,
                password: params.password
            },
        });
        if (resultUser.success && resultUser.data.password == params.password) {
            if (resultUser.data.isEmailConfirmed) {
                if (resultUser.data.status == this.STATUS.ACTIVE) {
                    let resultClient = await DbClients.findByPk(params, { id: resultUser.data.idClient });
                    if (resultClient.success && resultClient.data.status != DbClients.STATUS.PENDING && resultClient.data.status != DbClients.STATUS.BLOCKED) {
                        let resultToken = await DbUserToken.createUserToken(params, resultUser.data.idUser, resultClient.data.idClient);
                        if (!resultToken.success) {
                            return resultToken;
                        }
                        let getUserInfo = await this.getUserInfo(params, resultUser.data.idUser);
                        if (getUserInfo.success) {
                            if (resultUser.data.idClient) {
                                await DbClients.updateByAttributes(params, {
                                    atts: { lastLoginDate: this.getCurrentDate() },
                                    condition: `idClient = ${resultUser.data.idClient}`
                                })
                            }
                            getUserInfo.data.accessToken = resultToken.data;
                        }
                        return getUserInfo;
                    }
                    return this.returnError("Your client status is blocked!");
                }
                return this.returnError("Your status is blocked!");
            }
            return this.returnError("Please confirm your email!");
        }
        return this.returnError("Incorrect login or password!");
    }

    async logout(params) {
        return await DbUserToken.delete(params, {
            condition: `accessToken = '${params.accessToken}'`,
        });
    }

    async dropUserClient(params) {

        const idClient = params.search_id;

        let resultDeleteClient = await DbClients.delete(params, {
            condition: `idClient = ${idClient}`
        });
        if (!resultDeleteClient.success) {
            this.returnError("Error While Dlete Client");
        }
        let resultDeleteUser = await this.delete(params, {
            condition: `idClient = ${idClient}`
        });
        if (!resultDeleteUser.success) {
            this.returnError("Error While Dlete User");
        }
        let resultDeleteToken = await DbUserToken.delete(params, {
            condition: `idClient = ${idClient}`
        });
        if (!resultDeleteToken.success) {
            this.returnError("Error While Dlete Token");
        }
        let sql = `DROP DATABASE IF EXISTS ${process.env.DB_Name_Stracture}_${idClient}`;
        let resultDropDatabase = await this.query(params, sql);
        if (!resultDropDatabase.success) {
            this.returnError("Error While Drop Database");
        }
        return this.returnSuccess("Success Drop Client All Records");
    }

    async changeAccountSettings(params) {
        let sd = params.saveData;
        let userObj = {
            fullName: sd.fullName,
            phoneCode: sd.phoneCode || '+963',
            phoneNumber: sd.phoneNumber,
            lang: sd.lang,
            login: sd.login,
            icon: '',
            direction: sd.direction || 'ltr',
            TeethNameType: sd.TeethNameType || 1 ,
        };
        if (sd.hasOwnProperty("icon") && sd.icon) {
            sd.icon = sd.icon.replace('public/tmp/', 'root/asnan/public/tmp/');
            userObj.icon = await renameFile(sd.icon, 'root/asnan/public/uploads/usericon');
            console.log(sd.icon);
            console.log(userObj.icon);
            userObj.icon = userObj.icon.replace('root/asnan/', '');
        }
        let resultUpdate = await this.updateByAttributes(params, {
            atts: userObj,
            condition: `idUser = ${params.sidUser}`
        });
        if (resultUpdate.success) {
            let getUserInfo = await this.getUserInfo(params, params.sidUser);
            return getUserInfo;
        }
        return resultUpdate;
    }

    async changePassword(params) {
        let sd = params.saveData;
        let resultUser = await this.findByPk(params, { id: params.sidUser });
        if (resultUser.success) {
            if (resultUser.data.password == sd.currentPassword) {
                let resultUpdate = await this.updateByAttributes(params, {
                    atts: { password: sd.newPassword },
                    condition: `idUser=${params.sidUser}`
                });
                if (resultUpdate.success) {
                    return this.returnSuccess("Password Changed");
                }
            }
        }
        return this.returnError("Incorect Current Password!");
    }

    async getUserInfo(params, idUser) {
        let resultUser = await this.findByAttributes(params, {
            atts: { idUser: idUser },
            view: true,
            select: `
                clientIdKey,
                idKey,
                idUser,
                idClient,
                fullName,
                login,
                email,
                phoneCode,
                phoneNumber,
                roleName,
                idRole,
                icon,
                status,
                idPolicy,
                birthDate,
                lang,
                createDate,
                clientStatus,
                clientName,
                clientLegalName,
                clientPhoneCode,
                clientPhoneNumber,
                clientTin,
                clientEmail,
                clientIcon,
                clientPackage,
                clientPackageName,
                clientidCountry,
                clientidCurrency,
                clientCountryName,
                clientCurrencyCode,
                clientCurrencySymbol,
                TeethNameType,
                idBranch,
                direction
            `,
        });
        if (resultUser.success) {
            params.sidClient = resultUser.data.idClient;
            params.sidUser = resultUser.data.idUser;
            let resultPlicy = await DbUsersPolicy.findByAttributes(params, {
                atts: { idUserPolicy: resultUser.data.idPolicy },
                select: 'idKey, idUserPolicy, polName,polNameEn, polValues',
            })
            resultUser.data.userPolicy = resultPlicy.success ? Object.assign({}, resultPlicy.data, { polValues: JSON.parse(resultPlicy.data.polValues) }) : {};
        }
        return resultUser;
    }

    async createUser(params) {
        if (params.sidRole == this.ROLE.SUPERADMIN || params.sidRole == this.ROLE.ADMIN) {
            if (!params.tokenUser.client.idPackage) {
                return this.returnError("Please Select Your Package!");
            }
            let sd = params.saveData;
            let allowUserCount = params.tokenUser.client.package.userLimitCount;
            let getClientUsersCount = await DbClients.getClientUsersCount(params, params.sidClient);
            let clientUserCount = getClientUsersCount.success ? (getClientUsersCount.data.c) : 1000;
            if (clientUserCount < allowUserCount) {
                let userObj = {
                    idClient: params.sidClient,
                    fullName: sd.fullName,
                    email: sd.email,
                    password: sd.password,
                    phoneCode: sd.phoneCode || '+963',
                    phoneNumber: sd.phoneNumber,
                    idBranch: sd.idBranch ? sd.idBranch : 1,
                    lang: params.tokenUser.client.country.countryLang,
                    login: sd.email,
                    // themeColor: sd.themeColor || 'blue',
                    idRole: sd.idRole || this.ROLE.DOCTOR,
                    medicalSpecialty: sd.medicalSpecialty || null,
                    idPolicy: sd.idPolicy || 1,
                    status: this.STATUS.ACTIVE,
                    direction: this.DIRECTION.LTR,
                    isEmailConfirmed: 1,
                    birthDate: sd.birthDate || null,
                    icon: '',
                };
                if (sd.hasOwnProperty("icon") && sd.icon) {
                    sd.icon = sd.icon.replace('public/tmp/', 'root/asnan/public/tmp/');
                    userObj.icon = await renameFile(sd.icon, 'root/asnan/public/uploads/usericon');
                    userObj.icon = userObj.icon.replace('root/asnan/', '');
                }
                // if (sd.hasOwnProperty("userLevel") && sd.userLevel) {
                //     userObj.userLevel = sd.userLevel;
                // }
                // if (sd.hasOwnProperty("idPrinter") && sd.idPrinter) {
                //     userObj.idPrinter = sd.idPrinter;
                // }
                let createResult = await this.save(params, userObj, false);
                if (createResult.success) {
                    return await this.getUserInfo(params, createResult._id);
                }
                return createResult;
            }
            return this.returnError("Can't register this user. Please change your package!");
        }
        return this.returnError("You not have permission in this action!");
    }

    async updateUser(params) {
        if (params.sidRole == this.ROLE.SUPERADMIN || params.sidRole == this.ROLE.ADMIN) {
            let condition = params.sidRole !== this.ROLE.SUPERADMIN ? {idClient: params.sidClient} : ''
            let resultUser = await this.findByAttributes(params, {
                atts: Object.assign({idKey: params.search_id}, condition)
            });
            if (resultUser.success) {
                let sd = params.saveData;
                let userObj = {
                    fullName: sd.fullName,
                    email: sd.email,
                    phoneCode: sd.phoneCode || '+963',
                    phoneNumber: sd.phoneNumber,
                    idBranch: sd.idBranch || 1,
                    // idPrinter: sd.idPrinter || 0,
                    // userLevel: sd.userLevel || 1,
                    medicalSpecialty: sd.medicalSpecialty || null,
                    login: sd.email,
                    idRole: sd.idRole || this.ROLE.DOCTOR,
                    direction: sd.direction ? sd.direction : 'ltr',
                    idPolicy: sd.idPolicy || 1,
                    birthDate: sd.birthDate || null,
                    icon: '',
                };
                if (sd.hasOwnProperty("icon") && sd.icon) {
                    sd.icon = sd.icon.replace('public/tmp/', 'root/asnan/public/tmp/');
                    userObj.icon = await renameFile(sd.icon, 'root/asnan/public/uploads/usericon');
                    userObj.icon = userObj.icon.replace('root/asnan/', '');
                }
                if (sd.hasOwnProperty("password")) { userObj.password = sd.password }
                let updateResult = await this.updateByAttributes(params, {
                    atts: userObj,
                    condition: `idUser = ${resultUser.data.idUser}`
                });
                if (updateResult.success) {
                    return await this.getUserInfo(params, resultUser.data.idUser);
                }
                return updateResult;
            }
            return this.returnError("User Not Found!");
        }
        return this.returnError("You not have permission in this action!");
    }

    async changeStatusActive(params) {
        let resultUser = await this.findByAttributes(params, {
            atts: { idKey: params.search_id, idClient: params.sidClient }
        });
        if (resultUser.success && resultUser.data.idUser != params.sidUser) {
            let updateResult = await this.updateByAttributes(params, {
                atts: {
                    status: this.STATUS.ACTIVE
                },
                condition: `idUser = ${resultUser.data.idUser}`
            });
            if (updateResult.success) {
                return await this.getUserInfo(params, resultUser.data.idUser);
            }
            return updateResult;
        }
        return this.returnError("User Not Found!");
    }

    async changeStatusBlocked(params) {
        let resultUser = await this.findByAttributes(params, {
            atts: { idKey: params.search_id, idClient: params.sidClient }
        });
        if (resultUser.success && resultUser.data.idUser != params.sidUser) {
            let updateResult = await this.updateByAttributes(params, {
                atts: {
                    status: this.STATUS.BLOCKED
                },
                condition: `idUser = ${resultUser.data.idUser}`
            });
            if (updateResult.success) {
                return await this.getUserInfo(params, resultUser.data.idUser);
            }
            return updateResult;
        }
        return this.returnError("User Not Found!");
    }

    async deleteUser(params) {
        if (params.sidRole == this.ROLE.SUPERADMIN || params.sidRole == this.ROLE.ADMIN) {
            let resultUser = await this.findByAttributes(params, {
                atts: { idKey: params.search_id, idClient: params.sidClient }
            });
            if (resultUser.success) {
                if (resultUser.data.status != this.ROLE.SUPERADMIN) {
                    return this.returnError("Inch petqa anenq delete-i jamanak parz chi");
                }
                return this.returnError("You can`t delete this user!");
            }
            return this.returnError("User Not Found!");
        }
        return this.returnError("You not have permission in this action!");
    }

    async list(params) {
        let pd = params.paginationData;
        let allowSortFilds = ["fullName", "email", "createDate"];

        let conditionList = [];
        if (pd.hasOwnProperty("q") && pd.q.trim()) {
            conditionList.push(`(fullName LIKE "%${pd.q.trim()}%" OR email LIKE "%${pd.q.trim()}%")`);
        }
        if (pd.hasOwnProperty("filter")) {
            if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate.trim()) {
                conditionList.push(`createDate >= ${this.formatSQLDate(pd.filter.fromDate)}`);
            }
            if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
                conditionList.push(`createDate <= ${this.formatSQLDate(pd.filter.toDate)}`);
            }
            if (pd.filter.hasOwnProperty("idRole") && pd.filter.idRole) {
                conditionList.push(`idRole = ${pd.filter.idRole}`);
            }
            if (pd.filter.hasOwnProperty("idPolicy") && pd.filter.idPolicy) {
                conditionList.push(`idPolicy = ${pd.filter.idPolicy}`);
            }
            if (pd.filter.hasOwnProperty("idBranch") && pd.filter.idBranch) {
                conditionList.push(`idBranch = ${pd.filter.idBranch}`);
            }
            if (pd.filter.hasOwnProperty("status") && pd.filter.status) {
                conditionList.push(`status = ${pd.filter.status}`);
            }
            // if (pd.filter.hasOwnProperty("userLevel") && pd.filter.userLevel) {
            //     conditionList.push(`userLevel = ${pd.filter.userLevel}`);
            // }
        }
        if (params.sidRole !== this.ROLE.SUPERADMIN) {
        conditionList.push(`idClient = ${params.sidClient}`);
        }
        let condition = conditionList.join(" AND ");
        let sortCondition = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
                    if (sortCondition) sortCondition += ',';
                    sortCondition += ` ${key} ${value} `;
                }
            })
        }
        let select = `idUser, idKey, fullName, email, phoneCode, phoneNumber, login, idRole, icon, idPolicy, lang, roleName, idBranch, medicalSpecialty, createDate, TeethNameType, birthDate, status`;
        let resultUser = await this.getPageList(params, { select, condition, sortCondition, view: true });

        let resultPlan;
        let resultPatient;
        for (let val in resultUser.data) {
            resultPlan = await DbPlan.getCount(params, {
                condition:`idDoctor= ${resultUser.data[val].idUser}`
            })
            resultUser.data[val].plansCount = resultPlan.data.c
            resultPatient = await DbPatients.getCount(params, {
                condition:`idDoctor= ${resultUser.data[val].idUser}`
            })
            resultUser.data[val].patientsCount = resultPatient.data.c
        }

        return resultUser
        // return await this.getPageList(params, { select, condition, sortCondition, view: true });
    }

    async getOne(params) {
        let resultUser = await this.findByAttributes(params, {
            atts: {
                idKey: params.search_id,
                idClient: params.sidClient
            },
            select: `idUser, idKey, fullName, email, phoneCode, phoneNumber, login, idRole, icon, idPolicy, lang, roleName, idBranch, direction, medicalSpecialty, TeethNameType, birthDate, status`,
            view: true
        });
        if (resultUser.success) {
            let resultPlan = await DbPlan.getCount(params, {
                condition: `idDoctor= ${resultUser.data.idUser}`
            })
            resultUser.data.plansCount = resultPlan.data.c
            let resultPatients = await DbPatients.getCount(params, {
                condition: `idDoctor= ${resultUser.data.idUser}`
            })
            resultUser.data.patientsCount = resultPatients.data.c
            return resultUser;
        }
        return this.returnError("User Not Found!");
    }


    async getSettings(params) {
        const resultDefaultCurrency = await DbCurrency.getDefaultCurrency(params);
        const resultBranchSettings = await DbSettings.getOne(params);
        let settings = Object.assign({}, resultBranchSettings.data, {
            idCurrency: resultDefaultCurrency.success ? resultDefaultCurrency.data.idCurrency : 1,
            defaultCurrency: resultDefaultCurrency.success ? resultDefaultCurrency.data : {},
            branchSettings: resultBranchSettings.success ? resultBranchSettings.data : {},
            initialTeeth: resultBranchSettings.success ? resultBranchSettings.data ? resultBranchSettings.data.initialTeeth ? resultBranchSettings.data.initialTeeth : {} : {} : {},
        })
        return { success: true, data: settings };
    }

    // async checkToken(params) {
    //     let getUserInfo = await this.getUserInfo(params, params.sidUser);
    //     if (getUserInfo.success) {
    //         getUserInfo.data.accessToken = params.tokenUser.accessToken;
    //     }
    //     return getUserInfo;
    // }

    async fillAll(params) {
        return await this.findAll(params, {
            // atts : {status : this.STATUS.ACTIVE, idClient : params.sidClient}
            condition: `status = ${this.STATUS.ACTIVE} AND idClient = ${params.sidClient}`,
        });
    }
    async fillAllDoctors(params) {
        return await this.findAll(params, {
            // atts : {status : this.STATUS.ACTIVE, idClient : params.sidClient}
            condition: `status = ${this.STATUS.ACTIVE} AND idClient = ${params.sidClient} AND idRole = ${this.ROLE.DOCTOR}`,
        });
    }

}

export default new DbUser;