import { sendUserSettings } from '../../utils/SocketActions';
import DbManagerCustom from './DbManagerCustom';

class DbCurrency extends DbManagerCustom {
    constructor() {
        super("tbl_currency");
        this.pk = "idCurrency";
        this.noDublicateCol = ['currName'];
    }

    async createTableSQL(params) {
        const sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
                        ${this.pk} int(11) NOT NULL,
                        idKey varchar(255) NOT NULL,
                        currName varchar(255) NOT NULL,
                        currNameEn varchar(255) DEFAULT NULL,
                        currSimbol varchar(255) NOT NULL,
                        currIsDefault int(2) NOT NULL DEFAULT 0,
                        status int(11) NOT NULL DEFAULT 1,
                        currencyMoneyCategory TEXT DEFAULT NULL,
                        createDate timestamp NOT NULL DEFAULT current_timestamp(),
                        updateDate datetime DEFAULT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
        return sql;
    }

    async insert(params, returnObj) {
        let sd = params.saveData;
        let obj = {
            currName: sd.currName,
            currNameEn: sd.currNameEn ? sd.currNameEn : sd.currName,
            currSimbol: sd.currSimbol,
            currencyMoneyCategory: sd.currencyMoneyCategory ? sd.currencyMoneyCategory : null,
        }
        let resultInsert = await this.save(params, obj, returnObj);
        sendUserSettings(params);
        return resultInsert;
    }

    async update(params, returnObj) {
        let result = await this.findByAttributes(params, {
            atts: { idKey: params.search_id }
        });
        if (result.success) {
            let sd = params.saveData;
            let obj = {
                idCurrency: result.data.idCurrency,
                currName: sd.currName,
                currNameEn: sd.currNameEn,
                currSimbol: sd.currSimbol,
                currencyMoneyCategory: sd.currencyMoneyCategory ? sd.currencyMoneyCategory : null,
            }
            let resultUpdate = await this.save(params, obj, returnObj);
            sendUserSettings(params);
            return resultUpdate;
        }
        return this.returnError("Currency Not Found!");
    }

    async getOne(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idKey: params.search_id
            },
            select: `*`
        });
        if (resultOne.success) {
            if (resultOne.data.currencyMoneyCategory) {
                resultOne.data.currencyMoneyCategory = JSON.parse(resultOne.data.currencyMoneyCategory);
            }
            return resultOne;
        }
        return this.returnError("Currency Not Found!");
    }

    async getDefaultCurrency(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                currIsDefault: 1,
            },
            select: `*`
        });
        if (resultOne.success) {
            if (resultOne.data.currencyMoneyCategory) {
                resultOne.data.currencyMoneyCategory = JSON.parse(resultOne.data.currencyMoneyCategory);
            }
            return resultOne;
        }
        return this.returnError("Default Currency Not Found!");
    }

    async getOneByPk(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idCurrency: params._id ? params._id : params.search_id
            },
            select: `*`,
        });
        if (resultOne.success) {
            if (resultOne.data.currencyMoneyCategory) {
                resultOne.data.currencyMoneyCategory = JSON.parse(resultOne.data.currencyMoneyCategory);
            }
            return resultOne;
        }
        return this.returnError("Currency Not Found!");
    }

    async list(params) {
        let pd = params.paginationData;
        let allowSortFilds = ["currName", "createDate"];
        let conditionList = [];
        if (pd.hasOwnProperty("q") && pd.q.trim()) {
            conditionList.push(`(currName LIKE "%${pd.q.trim()}%" OR currNameEn LIKE "%${pd.q.trim()}%")`);
        }
        if (pd.hasOwnProperty("filter")) {
            if (pd.filter.hasOwnProperty("status") && pd.filter.status) {
                conditionList.push(`status = ${pd.filter.status}`);
            }
        }
        let condition = conditionList.join(" AND ");
        let sortCondition = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
                    if (sortCondition) sortCondition += ',';
                    sortCondition += ` ${key} ${value} `;
                }
            })
        }
        let select = ` * `;
        return await this.getPageList(params, { select, condition, sortCondition });
    }

    async fillAll(params) {
        return await this.findAll(params, {
            condition: `status = ${this.STATUS.ACTIVE}`,
        });
    }
}

export default new DbCurrency;