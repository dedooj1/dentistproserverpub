
CREATE VIEW view_ard_users  AS  


select 
c.idKey AS clientIdKey,
u.idUser AS idUser,
u.idKey AS idKey,
u.idClient AS idClient,
u.fullName AS fullName,
u.login AS login,
u.email AS email,
u.phoneCode AS phoneCode,
u.phoneNumber AS phoneNumber,
u.idRole AS idRole,
u.icon AS icon,
u.status AS status,
u.idPolicy AS idPolicy,
u.lang AS lang,
u.idBranch AS idBranch,
r.name AS roleName,
c.status AS clientStatus,
c.name AS clientName,
c.nameLegal AS clientLegalName,
c.phoneCode AS clientPhoneCode,
c.phoneNumber AS clientPhoneNumber,
c.tin AS clientTin,
c.email AS clientEmail,
c.icon AS clientIcon,
c.idPackage AS clientPackage,
c.idCountry AS clientidCountry,
c.idCurrency AS clientidCurrency,
cou.countryName AS clientCountryName,
cur.currencyCode AS clientCurrencyCode,
cur.currencySymbol AS clientCurrencySymbol,
p.name AS clientPackageName 

from ard_users u 
left join ard_clients c on(u.idClient = c.idClient)
left join ard_packages p on(p.idPackage = c.idPackage)
left join ard_countries cou on(cou.idCountry = c.idCountry)
left join ard_currency cur on(cur.idCurrency = cou.idCurrency)
left join ard_roles r on(r.idRole = u.idRole)