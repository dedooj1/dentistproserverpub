SELECT 
            it.*,
            itParent.itName AS itParentName, 
            itParent.itNumber AS itParentNumber,
            CONCAT(it.itNumber,'-',it.itName) AS itFullName,
            CONCAT(itParent.itNumber,'-',itParent.itName) AS itParentFullName,
            itgroup.itGrpName,
            
            accBuy.accName AS itAccBuyName, 
            accBuy.accNumber AS itAccBuyNumber,
            CONCAT(accBuy.accNumber,'-',accBuy.accName) AS itAccBuyFullName,
            
            accBuyReturn.accName AS itAccBuyReturnName, 
            accBuyReturn.accNumber AS itAccBuyReturnNumber,
            CONCAT(accBuyReturn.accNumber,'-',accBuyReturn.accName) AS itAccBuyReturnFullName,
            
            accsale.accName AS itAccsaleName, 
            accsale.accNumber AS itAccsaleNumber,
            CONCAT(accsale.accNumber,'-',accsale.accName) AS itAccsaleFullName,
            
            accSaleReturn.accName AS itAccSaleReturnName, 
            accSaleReturn.accNumber AS itAccSaleReturnNumber,
            CONCAT(accSaleReturn.accNumber,'-',accSaleReturn.accName) AS itAccSaleReturnFullName,
            
            accStore.accName AS itAccStoreName, 
            accStore.accNumber AS itAccStoreNumber,
            CONCAT(accStore.accNumber,'-',accStore.accName) AS itAccStoreFullName,
            
            itUnit.unitName AS itUnitName,
            itUnitSmall.unitName AS itUnitNameSmall,
            itUnitLarge.unitName AS itUnitNameLarge    

            FROM tbl_items it
            LEFT JOIN tbl_accounts accBuy ON (it.idAccBuy = accBuy.idAcc)
            LEFT JOIN tbl_accounts accBuyReturn ON (it.idAccBuyReturn = accBuyReturn.idAcc)
            LEFT JOIN tbl_accounts accsale ON (it.idAccSale = accsale.idAcc)
            LEFT JOIN tbl_accounts accSaleReturn ON (it.idAccSaleReturn = accSaleReturn.idAcc)
            LEFT JOIN tbl_accounts accStore ON (it.idAccStore = accStore.idAcc AND it.idAccStore > 0)
            LEFT JOIN tbl_items itParent ON (it.idItParent = itParent.idItem AND it.idItParent > 0)
            LEFT JOIN tbl_item_groups itgroup ON (it.idItGroup = itgroup.idItGrp AND it.idItGroup > 0)
            LEFT JOIN tbl_unit itUnit ON (it.idUnit = itUnit.idUnit AND it.idUnit > 0)
            LEFT JOIN tbl_unit itUnitSmall ON (it.idUnitSmall = itUnitSmall.idUnit AND it.idUnitSmall > 0)
            LEFT JOIN tbl_unit itUnitLarge ON (it.idUnitLarge = itUnitLarge.idUnit AND it.idUnitLarge > 0)