import DbUser from './DbUser';
import moment from 'moment';
import DbBranchs from './DbBranchs';
import { renameFile } from '../../utils/FileFunctions';
import { commit, rollBack, setDBTR } from './config';
import DbCurrency from './DbCurrency';
import DbSettings from './DbSettings';
import uniqueString from 'unique-string';
import DbUsersPolicy from './DbUsersPolicy';
import DbPrinter from './DbPrinter';
import DbManagerMain from './DbManagerMain';
import DbPatients from './DbPatients';
import DbPlan from './DbPlan';
import DbDiagnosies from './DbDiagnosies';
import DbTreatments from './DbTreatments';
import DbStages from './DbStages';
import DbTeeth from './DbTeeth';

class DbClients extends DbManagerMain {
    constructor() {
        super("ard_clients");
        this.STATUS = {
            PENDING: 1,
            TRIAL: 2,
            ACTIVE: 3,
            BLOCKED: 4,
        }
        this.pk = "idClient";
        this.noDublicateCol = ['email'];
        this.TEETH = [
            {idKey: '988c805ed28fb1650f71a910fd108315', teethNameNumber: '11', teethName1: 'Teeth11', teethName2: 'Teeth11', teethName3: 'Teeth11', teethName4: 'Teeth11', teethName5: 'Teeth11', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,26,25,34,33,42,43,51,50]', TeethImage: 'public/uploads/usericon/8d0rghmcla8echan.png',createDate: '2022-11-08 14:28:39', updateDate: '2022-11-08 17:55:23'},
            {idKey: 'cd14cadbe95c93e9308dc5f0d8900e6f', teethNameNumber: '12', teethName1: 'Teeth12', teethName2: 'Teeth12', teethName3: 'Teeth12', teethName4: 'Teeth12', teethName5: 'Teeth12', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,24,25,33,32,34,49,50,51,41,42]', TeethImage: 'public/uploads/usericon/8d0rghmcla8efna6.png',createDate: '2022-11-08 16:00:45', updateDate: '2022-11-08 18:51:00'},
            {idKey: '20f838b1e5f111fa913cf7bf279a8941', teethNameNumber: '13', teethName1: 'Teeth13', teethName2: 'Teeth13', teethName3: 'Teeth13', teethName4: 'Teeth13', teethName5: 'Teeth13', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,48,49,50,40,41,31,32,33,23,24]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eg8k0.png',createDate: '2022-11-08 16:01:11', updateDate: '2022-11-08 18:48:04'},
            {idKey: '3c6ef9a9fdc9fb2b7b9224ede94c7f76', teethNameNumber: '14', teethName1: 'Teeth14', teethName2: 'Teeth14', teethName3: 'Teeth14', teethName4: 'Teeth14', teethName5: 'Teeth14', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,30,31,32,47,48,49,39,40,22,23]', TeethImage: 'public/uploads/usericon/8d0rghmcla8egmba.png',createDate: '2022-11-08 16:01:30', updateDate: '2022-11-08 17:58:55'},
            {idKey: 'ba53180604611584bb6964fe2f02ef17', teethNameNumber: '15', teethName1: 'Teeth15', teethName2: 'Teeth15', teethName3: 'Teeth15', teethName4: 'Teeth15', teethName5: 'Teeth15', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,46,47,48,29,30,31,21,22,38,39]', TeethImage: 'public/uploads/usericon/8d0rghmcla8egydv.png',createDate: '2022-11-08 16:01:45', updateDate: '2022-11-08 18:00:51'},
            {idKey: '4478b805ed56de7702e9e15fc624fa60', teethNameNumber: '16', teethName1: 'Teeth16', teethName2: 'Teeth16', teethName3: 'Teeth16', teethName4: 'Teeth16', teethName5: 'Teeth16', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,28,29,30,45,46,47,37,38,20,21]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ehbs2.png',createDate: '2022-11-08 16:02:03', updateDate: '2022-11-08 18:01:43'},
            {idKey: '7ad470f562ba4d9127036cce842ad724', teethNameNumber: '17', teethName1: 'Teeth17', teethName2: 'Teeth17', teethName3: 'Teeth17', teethName4: 'Teeth17', teethName5: 'Teeth17', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,44,45,46,27,28,29,19,20,36,37]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ehojy.png',createDate: '2022-11-08 16:02:22', updateDate: '2022-11-08 18:02:31'},
            {idKey: '4e16221213076dd8ded8abf11a12d3b6', teethNameNumber: '18', teethName1: 'Teeth18', teethName2: 'Teeth18', teethName3: 'Teeth18', teethName4: 'Teeth18', teethName5: 'Teeth18', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[888,44,45,35,36,27,28,18,19]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ei484.png',createDate: '2022-11-08 16:02:40', updateDate: '2022-11-08 18:03:20'},
            {idKey: '266c5bf174980fed1b2c8b7b26abdde0', teethNameNumber: '21', teethName1: 'Teeth21', teethName2: 'Teeth21', teethName3: 'Teeth21', teethName4: 'Teeth21', teethName5: 'Teeth21', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,95,96,86,87,78,79,69,70]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eij92.png',createDate: '2022-11-08 16:02:59', updateDate: '2022-11-08 18:06:34'},
            {idKey: '6af89e468cabfd3373be2a027c267f38', teethNameNumber: '22', teethName1: 'Teeth22', teethName2: 'Teeth22', teethName3: 'Teeth22', teethName4: 'Teeth22', teethName5: 'Teeth22', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[95,96,97,78,79,80,70,71,87,88,777]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eivyz.png',createDate: '2022-11-08 16:03:16', updateDate: '2022-11-08 18:48:41'},
            {idKey: '79e97e92ec009aa92a81bb73e785a8d7', teethNameNumber: '23', teethName1: 'Teeth23', teethName2: 'Teeth23', teethName3: 'Teeth23', teethName4: 'Teeth23', teethName5: 'Teeth23', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,96,97,98,88,79,80,81,71,89,72]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ejcjo.png',createDate: '2022-11-08 16:03:37', updateDate: '2022-11-08 18:53:57'},
            {idKey: '9a9ac11925395e216fe813b7cd15f23c', teethNameNumber: '24', teethName1: 'Teeth24', teethName2: 'Teeth24', teethName3: 'Teeth24', teethName4: 'Teeth24', teethName5: 'Teeth24', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,97,98,99,89,80,81,82,72,73,90]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ejrzz.png',createDate: '2022-11-08 16:03:57', updateDate: '2022-11-08 18:12:34'},
            {idKey: '50af8bfb0ae2f604d190df2d429bd852', teethNameNumber: '25', teethName1: 'Teeth25', teethName2: 'Teeth25', teethName3: 'Teeth25', teethName4: 'Teeth25', teethName5: 'Teeth25', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,98,99,100,90,91,81,82,83,73,74]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ek5v3.png',createDate: '2022-11-08 16:04:15', updateDate: '2022-11-08 18:13:15'},
            {idKey: 'a8941739ec8f525d4ba93a38ab66cd4c', teethNameNumber: '26', teethName1: 'Teeth26', teethName2: 'Teeth26', teethName3: 'Teeth26', teethName4: 'Teeth26', teethName5: 'Teeth26', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,99,100,101,91,92,82,83,84,74,75]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ekksg.png',createDate: '2022-11-08 16:04:34', updateDate: '2022-11-08 18:14:13'},
            {idKey: '99da2d757235234b404967daf5354887', teethNameNumber: '27', teethName1: 'Teeth27', teethName2: 'Teeth27', teethName3: 'Teeth27', teethName4: 'Teeth27', teethName5: 'Teeth27', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,100,101,102,92,93,83,84,85,75,76]', TeethImage: 'public/uploads/usericon/8d0rghmcla8el48y.png',createDate: '2022-11-08 16:04:59', updateDate: '2022-11-08 18:15:21'},
            {idKey: 'a7dcfad3da15412db91b80dd39cdff74', teethNameNumber: '28', teethName1: 'Teeth28', teethName2: 'Teeth28', teethName3: 'Teeth28', teethName4: 'Teeth28', teethName5: 'Teeth28', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[777,101,102,93,94,84,85,76,77]', TeethImage: 'public/uploads/usericon/8d0rghmcla8elj0a.png',createDate: '2022-11-08 16:05:19', updateDate: '2022-11-08 18:16:17'},
            {idKey: '21cf5c2a80394d437cccb32d3b763387', teethNameNumber: '31', teethName1: 'Teeth31', teethName2: 'Teeth31', teethName3: 'Teeth31', teethName4: 'Teeth31', teethName5: 'Teeth31', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[197,198,188,189,180,181,171,172,555]', TeethImage: 'public/uploads/usericon/8d0rghmcla8encg4.png',createDate: '2022-11-08 16:06:44', updateDate: '2022-11-08 18:31:13'},
            {idKey: 'aa2591ed30a52e19a919319e4c106957', teethNameNumber: '32', teethName1: 'Teeth32', teethName2: 'Teeth32', teethName3: 'Teeth32', teethName4: 'Teeth32', teethName5: 'Teeth32', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[197,198,199,189,190,180,181,182,172,173,555]', TeethImage: 'public/uploads/usericon/8d0rghmcla8enqtf.png',createDate: '2022-11-08 16:07:02', updateDate: '2022-11-08 19:00:20'},
            {idKey: '9ea32dfef18dafd30d9dd5c3e1803ad2', teethNameNumber: '33', teethName1: 'Teeth33', teethName2: 'Teeth33', teethName3: 'Teeth33', teethName4: 'Teeth33', teethName5: 'Teeth33', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,198,199,200,190,191,181,182,183,173,174]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eo60k.png',createDate: '2022-11-08 16:07:22', updateDate: '2022-11-08 18:33:06'},
            {idKey: '290af4727f2eea244056071dc2fcb4b1', teethNameNumber: '34', teethName1: 'Teeth34', teethName2: 'Teeth34', teethName3: 'Teeth34', teethName4: 'Teeth34', teethName5: 'Teeth34', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,199,200,201,191,192,182,183,184,174,175]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eohfb.png',createDate: '2022-11-08 16:07:36', updateDate: '2022-11-08 18:34:42'},
            {idKey: 'bfef6c8447b45f6d458da42e6d4d8d5f', teethNameNumber: '35', teethName1: 'Teeth35', teethName2: 'Teeth35', teethName3: 'Teeth35', teethName4: 'Teeth35', teethName5: 'Teeth35', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,200,201,202,192,193,183,184,185,175,176]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eoso7.png',createDate: '2022-11-08 16:07:51', updateDate: '2022-11-08 18:35:22'},
            {idKey: '444cc3b35e87bd118b64d3f45f111a43', teethNameNumber: '36', teethName1: 'Teeth36', teethName2: 'Teeth36', teethName3: 'Teeth36', teethName4: 'Teeth36', teethName5: 'Teeth36', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,201,202,203,193,194,184,185,186,176,177]', TeethImage: 'public/uploads/usericon/8d0rghmcla8epdtw.png',createDate: '2022-11-08 16:08:18', updateDate: '2022-11-08 18:36:28'},
            {idKey: '9559bfbdb2c4a54f9c0288fe926af1ea', teethNameNumber: '37', teethName1: 'Teeth37', teethName2: 'Teeth37', teethName3: 'Teeth37', teethName4: 'Teeth37', teethName5: 'Teeth37', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,202,203,204,194,195,185,186,187,177,178]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eptz5.png',createDate: '2022-11-08 16:08:39', updateDate: '2022-11-08 18:37:15'},
            {idKey: 'cd3fbfc8850f92e02be52b22e19791b7', teethNameNumber: '38', teethName1: 'Teeth38', teethName2: 'Teeth38', teethName3: 'Teeth38', teethName4: 'Teeth38', teethName5: 'Teeth38', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[555,203,204,195,196,186,187,178,179]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eq63m.png',createDate: '2022-11-08 16:08:55', updateDate: '2022-11-08 18:38:03'},
            {idKey: '5c255ecd5e3266bd6420915adb2c0135', teethNameNumber: '41', teethName1: 'Teeth41', teethName2: 'Teeth41', teethName3: 'Teeth41', teethName4: 'Teeth41', teethName5: 'Teeth41', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[152,153,144,145,135,136,127,128,666]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eqi2j.png',createDate: '2022-11-08 16:09:11', updateDate: '2022-11-08 18:59:36'},
            {idKey: 'c20ad2011ba5d80a3babd050db57fe19', teethNameNumber: '42', teethName1: 'Teeth42', teethName2: 'Teeth42', teethName3: 'Teeth42', teethName4: 'Teeth42', teethName5: 'Teeth42', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,151,152,153,143,144,134,135,136,126,127]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eqsz0.png',createDate: '2022-11-08 16:09:25', updateDate: '2022-11-08 18:43:31'},
            {idKey: '5e441e780f236e04390e99a647ef8f44', teethNameNumber: '43', teethName1: 'Teeth43', teethName2: 'Teeth43', teethName3: 'Teeth43', teethName4: 'Teeth43', teethName5: 'Teeth43', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,150,151,152,142,143,133,134,135,125,126]', TeethImage: 'public/uploads/usericon/8d0rghmcla8eudlr.png',createDate: '2022-11-08 16:09:39', updateDate: '2022-11-08 18:42:56'},
            {idKey: '578c95b8be396e988cde442f70c73e7e', teethNameNumber: '44', teethName1: 'Teeth44', teethName2: 'Teeth44', teethName3: 'Teeth44', teethName4: 'Teeth44', teethName5: 'Teeth44', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,149,150,151,141,142,132,133,134,124,125]', TeethImage: 'public/uploads/usericon/8d0rghmcla8esf34.png',createDate: '2022-11-08 16:10:40', updateDate: '2022-11-08 18:42:12'},
            {idKey: 'c6892aa8a7cb3c6e0b3390ca17c29f28', teethNameNumber: '45', teethName1: 'Teeth45', teethName2: 'Teeth45', teethName3: 'Teeth45', teethName4: 'Teeth45', teethName5: 'Teeth45', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,148,149,150,140,141,131,132,133,123,124]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ev3ez.png',createDate: '2022-11-08 16:12:45', updateDate: '2022-11-08 18:41:21'},
            {idKey: 'c66519f7a716f039f175dfcb9de90632', teethNameNumber: '46', teethName1: 'Teeth46', teethName2: 'Teeth46', teethName3: 'Teeth46', teethName4: 'Teeth46', teethName5: 'Teeth46', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,147,148,149,139,140,130,131,132,122,123]', TeethImage: 'public/uploads/usericon/8d0rghmcla8evlyp.png',createDate: '2022-11-08 16:13:09', updateDate: '2022-11-08 18:40:34'},
            {idKey: '78f79a59024f6bd21eae310c8d2688dc', teethNameNumber: '47', teethName1: 'Teeth47', teethName2: 'Teeth47', teethName3: 'Teeth47', teethName4: 'Teeth47', teethName5: 'Teeth47', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,146,147,148,138,139,129,130,131,121,122]', TeethImage: 'public/uploads/usericon/8d0rghmcla8evxbu.png',createDate: '2022-11-08 16:13:24', updateDate: '2022-11-08 18:40:03'},
            {idKey: 'b926b1e283f5259d214df783a756064c', teethNameNumber: '48', teethName1: 'Teeth48', teethName2: 'Teeth48', teethName3: 'Teeth48', teethName4: 'Teeth48', teethName5: 'Teeth48', DiagnosticReflectionList: '[]', TreatmentReflectionList: '[]', position: '[666,146,147,137,138,129,130,120,121]', TeethImage: 'public/uploads/usericon/8d0rghmcla8ew97k.png',createDate: '2022-11-08 16:13:39', updateDate: '2022-11-08 18:18:25'}
        ]
    }
    async createDb(params) {
        let resultDB = await this.query(params, `CREATE DATABASE IF NOT EXISTS ${process.env.DB_Name_Stracture}_${params.sidClient} DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_general_ci'`);
        if (resultDB.success) {
            let result = await this.createTables(params);
            return result;
        }
        return resultDB;
    }

    async createTables(params) {

        const error = async (...msg) => {
            let sql = `DROP DATABASE IF EXISTS ${process.env.DB_Name_Stracture}_${params.sidClient}`;
            await this.query(params, sql);
            return this.returnError("Error creating table " + msg);
        }

        // Tables

        let resultBranch = await DbBranchs.createTable(params);
        if (!resultBranch.success) return await error("DbBranchs", resultBranch.errMsg);

        let resultTeeth = await DbTeeth.createTable(params);
        if (!resultTeeth.success) return await error("DbTeeth", resultTeeth.errMsg);

        let resultPatient = await DbPatients.createTable(params);
        if (!resultPatient.success) return await error("DbPatients", resultPatient.errMsg);

        let resultPlan = await DbPlan.createTable(params);
        if (!resultPlan.success) return await error("DbPatients", resultPlan.errMsg);

        let resultDiagnosies = await DbDiagnosies.createTable(params);
        if (!resultDiagnosies.success) return await error("DbPatients", resultDiagnosies.errMsg);

        let resultTreatment = await DbTreatments.createTable(params);
        if (!resultTreatment.success) return await error("DbPatients", resultTreatment.errMsg);

        let resultStage = await DbStages.createTable(params);
        if (!resultStage.success) return await error("DbPatients", resultStage.errMsg);

        let resultCurrency = await DbCurrency.createTable(params);
        if (!resultCurrency.success) return await error("DbCurrency", resultCurrency.errMsg);


        let resultSettings = await DbSettings.createTable(params);
        if (!resultSettings.success) return await error("DbSettings", resultSettings.errMsg);

        let resultUserPolicy = await DbUsersPolicy.createTable(params);
        if (!resultUserPolicy.success) return await error("DbUsersPolicy", resultUserPolicy.errMsg);

        let resultPrinter = await DbPrinter.createTable(params);
        if (!resultPrinter.success) return await error("DbPrinter", resultPrinter.errMsg);

        let views = await this.createViews(params);
        if (!views.success) return await error(views.name, views.errMsg);

        let resultClient = await this.updateByAttributes(params, {
            atts: {
                status: this.STATUS.TRIAL,
                activeUntilDate: this.formatSQLDateMovement(moment().add(14, 'days'))
                // activeUntilDate: moment().add(14, 'days')
            },
            condition: `idClient = ${params.sidClient}`
        });
        if (!resultClient.success) return resultClient;
        let resultUser = await DbUser.updateByAttributes(params, {
            atts: {
                isEmailConfirmed: 1,
                token: null,
            },
            condition: `idUser = ${params.sidUser}`
        });
        if (!resultUser.success) return resultUser;
        return { success: true, message: "Success Created Tables" }
    }

    async createViews(params) {

        async function error(name, errMsg) {
            return { success: false, name: name, errMsg: errMsg }
        }

        let resultPlans = await DbPlan.createView(params);
        if (!resultPlans.success) return await error("DbPlan", resultPlans.errMsg);

        return this.returnSuccess();
    }

    // async putColumens() {
    //     let list = [];
    //     // list.push({
    //     //     db: DbItemMuntaj,
    //     //     sql: await DbItemMuntaj.createTableSQL(),
    //     // });
    //     // list.push({
    //     //     db: DbItems,
    //     //     sql: `ALTER TABLE ${DbItems.tableName} ADD COLUMN itDiscountPer double DEFAULT 0 AFTER itPriceJumlaSmall;`
    //     // })

    //     // From Here VINA
    //     // From Here ONLINE
    //     // list.push({
    //     //     db: DbInvoiceOrder,
    //     //     sql: `ALTER TABLE ${DbInvoiceOrder.tName} ADD COLUMN images varchar(255) DEFAULT NULL AFTER invOrderNote;`
    //     // })

    //     return list;
    // }


    async getRegisterWizardClientByToken(params) {
        let resultUser = await DbUser.findByAttributes(params, { atts: { token: params.search_id } });
        if (resultUser.success) {
            let resultClient = await this.findByAttributes(params, { atts: { idClient: resultUser.data.idClient } });
            return resultClient;
        }
        return this.returnError('Client Not Found');
    }

    async alterDatabases(params) {

        // const createIndexUnique = async (dbNameTbl, noDublCol) => {
        //     let name = noDublCol;
        //     let value = noDublCol;
        //     if (Array.isArray(noDublCol)) {
        //         name = noDublCol.join('_').toString();
        //         value = noDublCol.toString();
        //     }

        //     let sqlDrop = `ALTER TABLE ${dbNameTbl} DROP CONSTRAINT IF EXISTS ${name};`
        //     await this.query(params, sqlDrop);

        //     let sql = `ALTER TABLE ${dbNameTbl} ADD CONSTRAINT ${name} UNIQUE KEY(${value});`
        //     return await this.query(params, sql);
        // }

        // const create_noDublicateColumens = async (dbNameTbl, noDublicateCol) => {
        //     if (noDublicateCol.length) {
        //         let error = '';
        //         for (const noDublCol of noDublicateCol) {
        //             let result = await createIndexUnique(dbNameTbl, noDublCol);
        //             if (!result.success) {
        //                 error += result.errMsg;
        //             }
        //         }
        //         if (error) {
        //             return false;
        //         }
        //     }
        //     return true;
        // }

        // const queryAlter = async (alt) => {
        //     let sqlRun = alt.sql.replace(alt.db.tableName, `${alt.db.tableName}`);
        //     let resultPutColumens = await this.query(params, sqlRun);
        //     let bol = resultPutColumens.success;
        //     if (alt.sql.startsWith("CREATE TABLE")) {
        //         let sql2 = `ALTER TABLE ${dbName}.${alt.db.tableName}
        //                     ADD PRIMARY KEY (${alt.db.pk});`;
        //         let result2 = await this.query(params, sql2);
        //         bol &= result2.success;
        //         let sql3 = `ALTER TABLE ${dbName}.${alt.db.tableName}
        //                     MODIFY ${alt.db.pk} int(11) NOT NULL AUTO_INCREMENT;`;
        //         let result3 = await this.query(params, sql3);
        //         bol &= result3.success;
        //         bol &= await create_noDublicateColumens(`${dbName}.${alt.db.tableName}`, alt.db.noDublicateCol);
        //     }
        //     return { success: bol }
        // }

        const getAlterDb = () => {
            return [
                DbSettings,
            ];
        }

        const workOnDb = async (idClient) => {
            let ppaarraamm = { sidClient: idClient };
            let bol = true;
            let ssql = `SHOW DATABASES LIKE '${process.env.DB_Name_Stracture}_${ppaarraamm.sidClient}';`;
            let re = await this.queryOne(ppaarraamm, ssql);
            if (re.success) {

                // let resultBackup = await this.backUpDatabasesClient(ppaarraamm);
                // bol &= resultBackup.success;

                let listColuemnsSQL = getAlterDb();
                for (const db of listColuemnsSQL) {

                    // let resultDropView = await db.dropView(ppaarraamm);
                    // bol &= resultDropView.success;

                    let resultPutColumens = await db.queryAlter(ppaarraamm);
                    bol &= resultPutColumens.success;
                }
                let resultCreateViews = await this.createViews(ppaarraamm);
                bol &= resultCreateViews.success;
                return Boolean(bol);
            }
            return false;
        }

        let bol = true;

        // let resultBackup = await this.backUpDatabases(params);
        // bol &= resultBackup.success;

        let dat = moment().format("DD-MM-YYYY_HH-MM-SS").toString();

        if (params.search_id) {
            bol &= await workOnDb(params.search_id);
            return { success: Boolean(bol), date: dat, data: params.search_id };
        } else {
            let result = await this.findAll({
                select: 'idClient',
                sortCondition: 'idClient'
            });
            if (result.success) {
                let list = result.data;
                for (let i = 0; i < list.length; i++) {
                    let resu = await workOnDb(list[i].idClient);
                    result.data[i].success = resu;
                    bol &= result.data[i].success;
                }
                return { success: Boolean(bol), date: dat, data: result.data };
            }
        }
        return this.returnError()
    }

    async checkAllClientsActiveUntilDate(params) {
        return await this.updateByAttributes(params, {
            atts: {
                status: this.STATUS.BLOCKED,
            },
            condition: `activeUntilDate < '${this.getCurrentDate()}'`
        });
    }

    async changeAccountSettings(params) {
        let sd = params.saveData;
        let clientObj = {
            name: sd.name,
            nameLegal: sd.nameLegal,
            tin: sd.tin,
            phoneCode: sd.phoneCode,
            phoneNumber: sd.phoneNumber,
            idCountry: sd.idCountry,
            idCurrency: sd.idCurrency,
            icon: null,
        };
        if (sd.hasOwnProperty("icon")) {
            clientObj.icon = await renameFile(sd.icon, 'public/uploads/usericon');
        }
        return await this.updateByAttributes(params, {
            atts: clientObj,
            condition: `idClient = ${params.sidClient}`
        });
    }

    async getClientUsersCount(params, idClient) {
        return await DbUser.countByAttributes(params, {
            atts: {
                idClient: idClient
            },
        });
    }

    async setDefaultData(params) {
        await setDBTR(params);
        let error = "";
        const resultBranch = await DbBranchs.save(params, {
            branchName: "Default Branch",
            branchDescription: "",
        }, false);
        if (!resultBranch.success) {
            error &= resultBranch.errMsg;
        }
        params.fileName = "DefaultMainTree_SY.xlsx";

        const resultCurrencyDollar = await DbCurrency.save(params, {
            currName: "دولار",
            currNameEn: "Dollar",
            currSimbol: "$",
            // currencyMoneyCategory: [{"cmcName":1},{"cmcName":5},{"cmcName":10},{"cmcName":50},{"cmcName":100}],
        }, false);
        if (!resultCurrencyDollar.success) {
            error &= resultCurrencyDollar.errMsg;
        }

        for(let i = 0; i < this.TEETH.length; i++){
            let val = this.TEETH[i]
            let resultTeeth = await DbTeeth.save(params, val, false);
            if (!resultTeeth.success) {
                error &= resultTeeth.errMsg;
            }
        }

        const resultSettings = await DbSettings.save(params, {
            idBranch: 1,
            initialTeeth: {"PermanentTeethInitialValues":[{"location":["UpperRightVisibleTeethSeq","Teeth1"],"idTeeth":8},{"location":["UpperRightVisibleTeethSeq","Teeth2"],"idTeeth":7},{"location":["UpperRightVisibleTeethSeq","Teeth3"],"idTeeth":6},{"location":["UpperRightVisibleTeethSeq","Teeth4"],"idTeeth":5},{"location":["UpperRightVisibleTeethSeq","Teeth5"],"idTeeth":4},{"location":["UpperRightVisibleTeethSeq","Teeth6"],"idTeeth":3},{"location":["UpperRightVisibleTeethSeq","Teeth7"],"idTeeth":2},{"location":["UpperRightVisibleTeethSeq","Teeth8"],"idTeeth":1},{"location":["UpperLeftVisibleTeethSeq","Teeth1"],"idTeeth":9},{"location":["UpperLeftVisibleTeethSeq","Teeth2"],"idTeeth":10},{"location":["UpperLeftVisibleTeethSeq","Teeth3"],"idTeeth":11},{"location":["UpperLeftVisibleTeethSeq","Teeth4"],"idTeeth":12},{"location":["UpperLeftVisibleTeethSeq","Teeth5"],"idTeeth":13},{"location":["UpperLeftVisibleTeethSeq","Teeth6"],"idTeeth":14},{"location":["UpperLeftVisibleTeethSeq","Teeth7"],"idTeeth":15},{"location":["UpperLeftVisibleTeethSeq","Teeth8"],"idTeeth":16},{"location":["LoweRightVisibleTeethSeq","Teeth1"],"idTeeth":32},{"location":["LoweRightVisibleTeethSeq","Teeth2"],"idTeeth":31},{"location":["LoweRightVisibleTeethSeq","Teeth3"],"idTeeth":30},{"location":["LoweRightVisibleTeethSeq","Teeth4"],"idTeeth":29},{"location":["LoweRightVisibleTeethSeq","Teeth5"],"idTeeth":28},{"location":["LoweRightVisibleTeethSeq","Teeth6"],"idTeeth":27},{"location":["LoweRightVisibleTeethSeq","Teeth7"],"idTeeth":26},{"location":["LoweRightVisibleTeethSeq","Teeth8"],"idTeeth":25},{"location":["LoweLeftVisibleTeethSeq","Teeth1"],"idTeeth":17},{"location":["LoweLeftVisibleTeethSeq","Teeth2"],"idTeeth":18},{"location":["LoweLeftVisibleTeethSeq","Teeth3"],"idTeeth":19},{"location":["LoweLeftVisibleTeethSeq","Teeth4"],"idTeeth":20},{"location":["LoweLeftVisibleTeethSeq","Teeth5"],"idTeeth":21},{"location":["LoweLeftVisibleTeethSeq","Teeth6"],"idTeeth":22},{"location":["LoweLeftVisibleTeethSeq","Teeth7"],"idTeeth":23},{"location":["LoweLeftVisibleTeethSeq","Teeth8"],"idTeeth":24}],"DeciduousTeethInitialValues":[],"OtherTeethInitialValues":[]},
        }, false);
        if (!resultSettings.success) {
            error &= resultSettings.errMsg;
        }

        const currnectTime = this.getCurrentDate();
        const groupKey = uniqueString();

        const resultUserPolicy = await DbUsersPolicy.save(params, {
            polName: 'صلاحيات مدير',
            polNameEn: 'Admin Policy',
            status: 1,
            polValues: JSON.parse('{"useCurrency":true,"addCurrency":true,"editCurrency":true,"useCurrencyRate":true,"addCurrencyRate":true,"editCurrencyRate":true,"useCostCenter":true,"addCostCenter":true,"editCostCenter":true,"useAccountGroup":true,"addAccountGroup":true,"editAccountGroup":true,"usePaymentType":true,"addPaymentType":true,"editPaymentType":true,"useTransactionType":true,"addTransactionType":true,"editTransactionType":true,"useAccounts":true,"addAccounts":true,"editAccounts":true,"useProfileCustomers":true,"addProfileCustomers":true,"editProfileCustomers":true,"useProfileSupliers":true,"addProfileSupliers":true,"editProfileSupliers":true,"useAccountStatment":true,"useTrialBalance":true,"useTransactions":true,"addTransactions":true,"editTransactions":true,"useProfileStore":true,"addProfileStore":true,"editProfileStore":true,"useUnit":true,"addUnit":true,"editUnit":true,"useItemGroup":true,"addItemGroup":true,"editItemGroup":true,"useDaftar":true,"addDaftar":true,"editDaftar":true,"usePrinters":true,"addPrinters":true,"editPrinters":true,"useInvoiceType":true,"addInvoiceType":true,"editInvoiceType":true,"useItems":true,"addItems":true,"editItems":true,"useItemsStatment":true,"useInvoiceOrders":true,"addInvoiceOrders":true,"editInvoiceOrders":true,"useInvoiceOrdersSettings":true,"useInvoice":true,"addInvoice":true,"editInvoice":true,"useMuntaj":true,"addMuntaj":true,"editMuntaj":true,"useTasni3":true,"addTasni3":true,"editTasni3":true,"useReports":true,"addReports":true,"editReports":true,"useUser":true,"addUser":true,"editUser":true,"useUsersPolicy":true,"addUsersPolicy":true,"editUsersPolicy":true}')
        }, false);
        if (!resultUserPolicy.success) {
            error &= resultUserPolicy.errMsg;
        }
        if (error) {
            await rollBack(params);
            return this.returnError(error);
        } else {
            await commit(params);
            return this.returnSuccess();
        }
    }

}

export default new DbClients;