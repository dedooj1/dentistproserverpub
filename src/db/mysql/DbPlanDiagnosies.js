import DbManagerCustom from './DbManagerCustom';

class DbPlanDiagnosies extends DbManagerCustom {
  constructor() {
    super("idPlanDiagnosies");
    this.pk = "idPlanDiagnosies";
    // this.noDublicateCol = ['condition'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            condition varchar(255) NOT NULL,
            type int(5) NULL,
            condition varchar(255) NOT NULL,
            teeth TEXT DEFAULT NULL,
            observations TEXT DEFAULT NULL,
            order int(11) NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let obj = {
      condition: sd.condition,
      optionList: sd.optionList || null,
      optionListType: sd.optionListType || null,
      type: sd.type || 1,
      description: sd.description,
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;

      let obj = {
        idPlanDiagnosies: result.data.idPlanDiagnosies,
        condition: sd.condition,
        // optionList: `${sd.optionList}` || null,
        optionList: sd.optionList || null,
        optionListType: sd.optionListType || null,
        type: sd.type || 1,
        description: sd.description,
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Diagnosy Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.optionList = JSON.parse(resultOne.data.optionList);
      return resultOne;
    }
    return this.returnError("Diagnosy Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["condition", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(condition LIKE "%${pd.q.trim()}%" OR description LIKE "%${pd.q.trim()}%" OR optionList LIKE "%${pd.q.trim()}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition });

    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result
  }

  async get_all(params) {
    // let tokenUser = params.tokenUser;
    // let result = await this.findAllByAttributes(params, {
    //   atts: { idPlanDiagnosies: tokenUser.idUser },
    //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    // });
    let result = await this.findAll(params);
    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result;
  }

  async getCount(params) {
    return await this.countByAttributes(params, {});
  }

  async delete(params) {
    let resultGet = await this.get_one_by_idkey(params, params.search_id);
    if (resultGet.success) {
      let idPlanDiagnosies = resultGet.data.idPlanDiagnosies;
      if (resultGet.data.icon) {
        await deleteFileByPath(resultGet.data.icon);
      }
      await this.deleteByAttributes(params, {
        atts: { idPlanDiagnosies: idPlanDiagnosies }
      });
      return this.returnSuccess();
    }
    return this.returnError("Diagnosies Not Found");
  }
}

export default new DbPlanDiagnosies;