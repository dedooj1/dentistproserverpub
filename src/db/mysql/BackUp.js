
import mysqldump from 'mysqldump';
// import fs from 'fs';
import { sendBackupFailed } from '../../utils/Mailer';
import { createFolderIfNotExist } from '../../utils/FileFunctions';

require('dotenv').config();

async function backUpDb(key, DbName, zip = false) {
    try {
        let dir = `public/backup/${key}`;
        // if (!fs.existsSync(dir)) {
        //     fs.mkdirSync(dir);
        // }
        await createFolderIfNotExist(dir);
        let fileName = `${dir}/${DbName}.sql`;
        let opt = {
            connection: {
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_password,
                database: DbName,
            },
            dumpToFile: fileName,
            // includeViewData: false,
            // dump: { schema: { table: { dropIfExist: true } } },
            // dump: { data: { includeViewData : false} },
            // dump: { schema: { view: { createOrReplace: false } } },
            // compressFile: zip,
        };
        const result = await mysqldump(opt);
        return { success: Boolean(result.hasOwnProperty('dump')), dumpToFile: fileName };
    } catch (error) {
        console.warn('error = ', error);
        await sendBackupFailed(DbName);
        return { success: false, dumpToFile: '' }
    }
}

export default backUpDb;