import DbManager from './DbManager';

class DbManagerMain extends DbManager {

    constructor(tableName, viewName) {
        super(tableName, viewName)
        this.init({});
    }

    selectViewOrTable(params, opt) {
        if (opt.view) {
            return `${process.env.DB_database}.${this.vName}`;
        }
        return `${process.env.DB_database}.${this.tName}`;
    }
}

export default DbManagerMain;