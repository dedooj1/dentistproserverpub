import DbManagerCustom from './DbManagerCustom';

class DbUsersPolicy extends DbManagerCustom {
    constructor() {
        super("tbl_users_policy");
        this.pk = "idUserPolicy";
        this.noDublicateCol = ['polName', 'polNameEn'];
    }

    async createTableSQL(params) {
        const sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)}  (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            polName varchar(255) NOT NULL,
            polNameEn varchar(255) NOT NULL,
            polValues text DEFAULT NULL,
            status int(1) NOT NULL DEFAULT 1,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate datetime DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
        return sql;
    }

    async insert(params) {
        let sd = params.saveData;
        let obj = {
            polName: sd.polName,
            polNameEn: sd.polNameEn ? sd.polNameEn : sd.polName,
            polValues: sd.polValues,
        }
        let resultInsert = await this.save(params, obj, true);
        return resultInsert;
    }

    async update(params) {
        let result = await this.findByAttributes(params, {
            atts: { idKey: params.search_id }
        });
        if (result.success) {
            let sd = params.saveData;
            let obj = {
                idUserPolicy: result.data.idUserPolicy,
                polName: sd.polName,
                polNameEn: sd.polNameEn,
                polValues: sd.polValues,
            }
            let resultUpdate = await this.save(params, obj, true);
            return resultUpdate;
        }
        return this.returnError("User Policy Not Found!");
    }

    async getOne(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idKey: params.search_id
            },
            select: `*`
        });
        if (resultOne.success) {
            resultOne.data.polValues = JSON.parse(resultOne.data.polValues)
            return resultOne;
        }
        return this.returnError("User Policy Not Found!");
    }

    async getOneByPk(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idUserPolicy: params._id ? params._id : params.search_id
            },
            select: `*`,
            view: true,
        });
        if (resultOne.success) {
            resultOne.data.polValues = JSON.parse(resultOne.data.polValues)
            return resultOne;
        }
        return this.returnError("User Policy Not Found!");
    }

    async list(params) {
        let pd = params.paginationData;
        let allowSortFilds = ["polName", "createDate"];
        let conditionList = [];
        if (pd.hasOwnProperty("q") && pd.q.trim()) {
            conditionList.push(`(polName LIKE "%${pd.q.trim()}%")`);
        }
        if (pd.hasOwnProperty("filter")) {
            // if (pd.filter.hasOwnProperty("idBranch") && pd.filter.idBranch) {
            //     conditionList.push(`idBranch = ${pd.filter.idBranch}`);
            // }
        }
        let condition = conditionList.join(" AND ");
        let sortCondition = ``;
        if (pd.hasOwnProperty("sort")) {
            Object.keys(pd.sort).map((key) => {
                let value = pd.sort[key].toUpperCase();
                if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
                    if (sortCondition) sortCondition += ',';
                    sortCondition += ` ${key} ${value} `;
                }
            })
        }
        let select = `*`;
        return await this.getPageList(params, { select, condition, sortCondition });
    }

    async fillAll(params) {
        return await this.findAll(params, {
            condition: `status = ${this.STATUS.ACTIVE}`,
        });
    }
}

export default new DbUsersPolicy;