import DbManagerMain from './DbManagerMain';

class DbCurrencyMain extends DbManagerMain {
    constructor() {
        super("ard_currency");
        this.pk = "idCurrency";
        this.noDublicateCol = [];
    }

    async get_all(params) {
        return await this.findAllByAttributes(params, {
            atts: { useProject: 1 },
            select: "idCurrency, currencyCode, currencyName, currencySymbol"
        });
    }
}
export default new DbCurrencyMain;