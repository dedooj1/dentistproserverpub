import DbManagerMain from './DbManagerMain';

class DbCountries extends DbManagerMain {
    constructor() {
        super("ard_countries");
        this.pk = "idCountry";
        this.noDublicateCol = [];
    }

    async get_all(params) {
        return await this.findAllByAttributes(params, {
            atts: { useProject: 1 },
            select: "idCountry, countryName, phoneCode, phoneCodeLength, idCurrency, countryLang",
            sort: "ordering,idCountry"
        });
    }

}
export default new DbCountries;