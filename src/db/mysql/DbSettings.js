import { sendUserSettings } from '../../utils/SocketActions';
import DbManagerCustom from './DbManagerCustom';
import DbTeeth from './DbTeeth';

class DbSettings extends DbManagerCustom {
    constructor() {
        super("tbl_settings");
        this.pk = "idSettings";
        this.noDublicateCol = [];
    }

    async createTableSQL(params) {
        const sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)}  (
                        ${this.pk} int(11) NOT NULL,
                        idKey varchar(50) NOT NULL,
                        idBranch int(11) NOT NULL DEFAULT 1,
                        settingsGeneral text DEFAULT NULL,
                        initialTeeth text DEFAULT NULL,
                        createDate timestamp NOT NULL DEFAULT current_timestamp(),
                        updateDate datetime DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
        return sql;
    }

    async queryAlter(params) {
        let bol = true;
        let sqlDrop = [
            // `ALTER TABLE ${this.tableName(params)} DROP COLUMN optDecimal`,
            // `ALTER TABLE ${this.tableName(params)} DROP COLUMN settingsGeneral`,
        ];
        if (sqlDrop.length) {
            let resultDrop = await this.query(params, sqlDrop.join(';'))
            bol &= resultDrop.success;
        }
        let sql = [
            // `ALTER TABLE ${this.tableName(params)} ADD COLUMN settingsGeneral TEXT DEFAULT NULL AFTER settingsInvoiceOrder`,
        ];
        if (sql.length) {
            let resultQ1 = await this.query(params, sql.join(';'))
            bol &= resultQ1.success;
        }
        return { success: Boolean(bol) };
    }

    async insert(params) {
        let sd = params.saveData;
        let obj = {
            idBranch: params.sidBranch,
            initialTeeth: sd.initialTeeth || null,
        }
        if (sd.hasOwnProperty("settingsGeneral") && sd.settingsGeneral.lenght) {
            obj.settingsGeneral = sd.settingsGeneral;
        }
        let resultInsert = await this.save(params, obj, false);
        return resultInsert;
    }

    async update(params) {
        let result = await this.findByAttributes(params, {
            atts: { idBranch: params.sidBranch }
        });
        if (result.success) {
            let sd = params.saveData;
            let obj = {
                idSettings: result.data.idSettings,
                initialTeeth: sd.initialTeeth || null,
                // optDecimal: sd.optDecimal ? sd.optDecimal : 2,
            }
            // if (sd.hasOwnProperty("optDecimal") && sd.optDecimal) {
            //     obj.optDecimal = sd.optDecimal;
            // }
            if (sd.hasOwnProperty("settingsGeneral") && sd.settingsGeneral) {
                obj.settingsGeneral = sd.settingsGeneral;
            }
            let resultUpdate = await this.save(params, obj, false);
            return resultUpdate;
        }
        return this.returnError("Settings Not Found!");
    }

    async saveSettingsBranch(params) {
        let resultFind = await this.findByAttributes(params, {
            atts: { idBranch: params.sidBranch }
        });
        let result;
        if (resultFind.success) {
            result = await this.update(params);
        } else {
            result = await this.insert(params);
        }
        if (result.success) {
            sendUserSettings(params);
            return this.returnSuccess();
        }
        return this.returnError(result.errMsg)
    }

    async getOne(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idBranch: params.sidBranch
            },
            select: ` * `
        });
        if (resultOne.success) {
            resultOne.data.settingsGeneral = resultOne.data.settingsGeneral ? JSON.parse(resultOne.data.settingsGeneral) : {};
            resultOne.data.initialTeeth = resultOne.data.initialTeeth ? JSON.parse(resultOne.data.initialTeeth) : {};
            return resultOne;
        }
        return this.returnError("Settings Not Found!");
    }

    async getInitialTeeth(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idBranch: params.sidBranch
            },
            select: ` * `
        });
        if (resultOne.success) {
            resultOne.data.initialTeeth = resultOne.data.initialTeeth ? JSON.parse(resultOne.data.initialTeeth) : {};
            let error = ''
            if (resultOne.data.initialTeeth) {
                if (resultOne.data.initialTeeth.PermanentTeethInitialValues) {
                    for (let i = 0; i < resultOne.data.initialTeeth.PermanentTeethInitialValues.length; i++) {
                        let val = resultOne.data.initialTeeth.PermanentTeethInitialValues[i]
                        let result = await DbTeeth.findByPk(params, { id: val.idTeeth })
                        if (result.success) {
                            result.data.DiagnosticReflectionList = JSON.parse(result.data.DiagnosticReflectionList);
                            result.data.TreatmentReflectionList = JSON.parse(result.data.TreatmentReflectionList);
                            result.data.position = JSON.parse(result.data.position);
                            resultOne.data.initialTeeth.PermanentTeethInitialValues[i] = Object.assign({ location: val.location }, result.data)
                        } else {
                            error += result.errMsg
                        }
                    }
                }
                if (resultOne.data.initialTeeth.DeciduousTeethInitialValues) {
                    for (let i = 0; i < resultOne.data.initialTeeth.DeciduousTeethInitialValues.length; i++) {
                        let val = resultOne.data.initialTeeth.DeciduousTeethInitialValues[i]
                        let result = await DbTeeth.findByPk(params, { id: val.idTeeth })
                        if (result.success) {
                            result.data.DiagnosticReflectionList = JSON.parse(result.data.DiagnosticReflectionList);
                            result.data.TreatmentReflectionList = JSON.parse(result.data.TreatmentReflectionList);
                            result.data.position = JSON.parse(result.data.position);
                            resultOne.data.initialTeeth.DeciduousTeethInitialValues[i] = Object.assign({ location: val.location }, result.data)
                        } else {
                            error += result.errMsg
                        }
                    }
                }
                if (resultOne.data.initialTeeth.OtherTeethInitialValues) {
                    for (let i = 0; i < resultOne.data.initialTeeth.OtherTeethInitialValues.length; i++) {
                        let val = resultOne.data.initialTeeth.OtherTeethInitialValues[i]
                        let result = await DbTeeth.findByPk(params, { id: val.idTeeth })
                        if (result.success) {
                            result.data.DiagnosticReflectionList = JSON.parse(result.data.DiagnosticReflectionList);
                            result.data.TreatmentReflectionList = JSON.parse(result.data.TreatmentReflectionList);
                            result.data.position = JSON.parse(result.data.position);
                            resultOne.data.initialTeeth.OtherTeethInitialValues[i] = Object.assign({ location: val.location }, result.data)
                        } else {
                            error += result.errMsg
                        }
                    }
                }
            }
            if (!error) {
                return resultOne;
            } else {
                return this.returnError()
            }
        }
        return this.returnError("Settings Not Found!");
    }
    async getPermanentTeethInitialValues(params) {
        let resultOne = await this.findByAttributes(params, {
            atts: {
                idBranch: params.sidBranch
            },
            select: ` * `
        });
        if (resultOne.success) {
            resultOne.data.settingsGeneral = resultOne.data.settingsGeneral ? JSON.parse(resultOne.data.settingsGeneral) : {};
            return resultOne;
        }
        return this.returnError("Settings Not Found!");
    }
}

export default new DbSettings;