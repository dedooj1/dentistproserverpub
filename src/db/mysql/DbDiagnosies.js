import { sendUserSettings } from '../../utils/SocketActions';
import DbManagerCustom from './DbManagerCustom';
import DbPlan from './DbPlan';
import DbTeeth from './DbTeeth';
import DbUser from './DbUser';

class DbDiagnosies extends DbManagerCustom {
  constructor() {
    super("tbl_diagnosies");
    this.pk = "idDiagnosies";
    // this.noDublicateCol = ['diagnosiesName'];
  }

  async createTableSQL(params) {
    let sql = `CREATE TABLE IF NOT EXISTS ${this.tableName(params)} (
            ${this.pk} int(11) NOT NULL,
            idKey varchar(50) NOT NULL,
            diagnosiesName varchar(255) NOT NULL,
            optionList TEXT NULL,
            optionListType int(1) NULL,
            location int(5) NULL,
            type int(5) NULL,
            description TEXT DEFAULT NULL,
            status int(1) NULL,
            createDate timestamp NOT NULL DEFAULT current_timestamp(),
            updateDate timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`;
    return sql;
  }

  async insert(params) {
    let sd = params.saveData;
    let obj = {
      diagnosiesName: sd.diagnosiesName,
      optionList: sd.optionList || null,
      optionListType: sd.optionListType || null,
      location: sd.location || null,
      type: sd.type || 1,
      status: this.STATUS.ACTIVE,
      description: sd.description,
    }
    let resultInsert = await this.save(params, obj, true);
    return resultInsert;
  }

  async update(params) {
    let result = await this.findByAttributes(params, {
      atts: { idKey: params.search_id }
    });
    if (result.success) {
      let sd = params.saveData;

      let obj = {
        idDiagnosies: result.data.idDiagnosies,
        diagnosiesName: sd.diagnosiesName,
        // optionList: `${sd.optionList}` || null,
        optionList: sd.optionList || null,
        optionListType: sd.optionListType || null,
        location: sd.location || null,
        type: sd.type || 1,
        status: sd.status,
        description: sd.description,
      }
      let resultUpdate = await this.save(params, obj, true);
      return resultUpdate;
    }
    return this.returnError("Diagnosy Not Found!");
  }

  async getOne(params) {
    let resultOne = await this.findByAttributes(params, {
      atts: {
        idKey: params.search_id
      },
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.optionList = JSON.parse(resultOne.data.optionList);
      return resultOne;
    }
    return this.returnError("Diagnosy Not Found!");
  }

  async getOneByType(params) {
    let resultOne = await this.findAll(params, {
      condition: `type= ${params.search_id}`,
      select: `*`
    });
    if (resultOne.success) {
      resultOne.data.map(val => {
        val.optionList = JSON.parse(val.optionList);
        return val
      })
      return resultOne;
    }
    return this.returnError("Diagnosy Not Found!");
  }

  async list(params) {
    let pd = params.paginationData;
    let tokenUser = params.tokenUser;
    let allowSortFilds = ["diagnosiesName", "createDate", "updateDate"];

    let conditionList = [];

    if (pd.hasOwnProperty("q") && pd.q.trim()) {
      conditionList.push(`(diagnosiesName LIKE "%${pd.q.trim()}%" OR description LIKE "%${pd.q.trim()}%" OR optionList LIKE "%${pd.q.trim()}%")`);
    }

    if (pd.hasOwnProperty("filter")) {
      if (pd.filter.hasOwnProperty("fromDate") && pd.filter.fromDate) {
        conditionList.push(`createDate >= "${this.formatSQLDate(pd.filter.fromDate)}"`);
      }

      if (pd.filter.hasOwnProperty("toDate") && pd.filter.toDate.trim()) {
        conditionList.push(`createDate <= "${this.formatSQLDate(pd.filter.toDate)}"`);
      }

      if (pd.filter.hasOwnProperty("type") && pd.filter.type) {
        conditionList.push(`type = "${pd.filter.type}"`);
      }

      if (pd.filter.hasOwnProperty("status") && pd.filter.status) {
        conditionList.push(`status = "${pd.filter.status}"`);
      }
    }

    let condition = conditionList.join(" AND ");
    let sortCondition = ``;
    if (pd.hasOwnProperty("sort")) {
      Object.keys(pd.sort).map((key) => {
        let value = pd.sort[key].toUpperCase();
        if (allowSortFilds.includes(key) && this.sortKeys.includes(value)) {
          if (sortCondition) sortCondition += ',';
          sortCondition += ` ${key} ${value} `;
        }
      })
    }

    let select = `*`;

    const result = await this.getPageList(params, { select, condition, sortCondition });

    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result
  }

  async get_all(params) {
    // let tokenUser = params.tokenUser;
    // let result = await this.findAllByAttributes(params, {
    //   atts: { idDiagnosies: tokenUser.idUser },
    //   condition: ` OR shareDoctor LIKE "%${tokenUser.idUser}%"`
    // });
    let result = await this.findAll(params);
    result.data.map(p => {
      p.optionList = JSON.parse(p.optionList);
    })
    return result;
  }

  async getCount(params) {
    return await this.countByAttributes(params, {});
  }
  // [{ "DiagnosticId": { "label": "test", "value": 1, "key": "1", "title": { "label": "test", "value": 1, "idDiagnosies": 1, "idKey": "ac3dce805f69a7f9f11eb582183e6f5a", "diagnosiesName": "test", "optionList": null, "optionListType": 1, "location": 1, "type": 2, "description": "dhnfb", "status": 1, "createDate": "2022-11-12T15:56:36.000Z", "updateDate": "2022-11-12T15:56:36.000Z" } } }]

  // [{"Diagnosies":{"idDiagnosies":3,"idKey":"43e66625b24545d5bed77f1bd822e9cf","diagnosiesName":"test","optionList":null,"optionListType":1,"location":1,"type":2,"description":"rtet","status":1,"createDate":"2022-11-12T17:12:27.000Z","updateDate":"2022-11-12T17:12:27.000Z"},"teeth":[24,23],"observations":"treset"}]

  async delete(params) {
    if (params.sidRole === DbUser.ROLE.SUPERADMIN || params.sidRole === DbUser.ROLE.ADMIN) {
      let resultGet = await this.get_one_by_idkey(params, params.search_id);
      if (resultGet.success) {
        let idDiagnosies = resultGet.data.idDiagnosies;
        let teethName = ''
        let planName = ''
        let resultTeeths = await DbTeeth.get_all(params);
        let resultPlans = await DbPlan.get_all(params);
        if (resultTeeths.success) {
          let inTeeth = false;
          for (let i = 0; i < resultTeeths.rowCount; i++) {
            let diag = resultTeeths.data[i].DiagnosticReflectionList
            if (diag !== []) {
              for (let j = 0; j < diag.length; j++) {
                if (diag[j].DiagnosticId.title.idDiagnosies === idDiagnosies) {
                  inTeeth = true;
                  break;
                }
              }
            }
            if (inTeeth) {
              teethName = resultTeeths.data[i].teethNameNumber
              break
            }
          }
        } else {
          return this.returnError("Diagnosies Not Found");
        }
        if (resultPlans.success) {
          let inTeeth = false;
          for (let i = 0; i < resultPlans.rowCount; i++) {
            let diag = resultPlans.data[i].planDiagnosies
            if (diag !== [] && diag !== null) {
              for (let j = 0; j < diag.length; j++) {
                if (diag[j].Diagnosies.idDiagnosies === idDiagnosies) {
                  inTeeth = true;
                  break;
                }
              }
            }
            if (inTeeth) {
              planName = resultPlans.data[i].planName
              break
            }
          }
        } else {
          return this.returnError("Diagnosies Not Found");
        }
        if (!teethName) {
          if (!planName) {
            if (resultGet.data.icon) {
              await deleteFileByPath(resultGet.data.icon);
            }
            await this.deleteByAttributes(params, {
              atts: { idDiagnosies: idDiagnosies }
            });
            return this.returnSuccess();

          } else {
            return this.returnError("Please delete diagnosies from plan " + planName);
          }
        } else {
          return this.returnError("Please delete diagnosies from Reflection in teeth " + teethName);
        }
      } else {
        return this.returnError("Diagnosies Not Found");
      }
    }
    return this.returnError("You dont have permission to delete Diagnosies");

  }

}

export default new DbDiagnosies;