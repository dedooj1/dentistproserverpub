import { sendDBConnectionLost } from '../../utils/Mailer';

const mysql = require('mysql2/promise');

require('dotenv').config();

const config = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_password,
    database: process.env.DB_database,
    // connectTimeout: 100000,
    connectionLimit: 10,
    // waitForConnections: true,
    // timezone: 'utc',
    multipleStatements: true,
    // debug : true,
    // queueLimit: 0,
}

if (process.env.DB_socketPath && process.env.DB_socketPath.length > 0) {
    config.socketPath = process.env.DB_socketPath;
}

let pool = null;


async function handleDisconnect() {
    console.warn("Connected on MySQL Server : ", config.host, " config : ", config);
    // pool = await mysql.createConnection(config);
    pool = mysql.createPool(config);
    pool = await pool.getConnection();
    // pool.connect();
    pool.on('error', async (err) => {
        pool = null;
        sendDBConnectionLost(err.code);
        // if (err.code === 'PROTOCOL_CONNECTION_LOST') {
        //     await handleDisconnect();
        // } else {
        //     throw err;
        // }
    });
}

async function getDB(params) {
    if (!pool) {
        await handleDisconnect();
    }
    return pool;
}

async function setDBTR(params) {
    params.connection = await getDB(params);
    await params.connection.beginTransaction();
}

async function rollBack(params) {
    if (params.connection) {
        await params.connection.rollback();
    }
}

async function commit(params) {
    if (params.connection) {
        await params.connection.commit();
    }
}

export { getDB, setDBTR, rollBack, commit };