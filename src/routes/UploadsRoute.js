import AppRoute from "./AppRoute";

class UploadsRoute extends AppRoute {

    init() {
        this.POSTFILE('/image', {
            checkToken: true,
            fileField: 'image',
            mimeTypes: ['image/jpeg', 'image/png', 'image/jpg'],
            // mimeTypes: ['image/*'],
            multiple: false,
        }, async (req, res, params) => {
            if (req.file) {     
                req.file.path = req.file.path.replace('root/asnan/', '');
                return { success: true, data: { filePath: req.file.path } };
            }
            return { success: false, errMsg: "File Upload Error!." };
        });

        this.POSTFILE('/images', {
            checkToken: true,
            fileField: 'images',
            mimeTypes: ['image/jpeg', 'image/png', 'image/jpg'],
            multiple: true,
        }, async (req, res, params) => {
            let data = [];
            if (req.files) {
                for (const file of req.files) {
                    file.path = file.path.replace('root/asnan/', '');
                    data.push({ filePath: file.path })
                }
                return { success: true, data: data };
            }
            return { success: false, errMsg: "Files Upload Error!." };
        });

        this.POSTFILE('/excel', {
            checkToken: false,
            fileField: 'excel',
            mimeTypes: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            multiple: false,
        }, async (req, res, params) => {
            if (req.file) {
                return { success: true, data: { filePath: req.file.path } };
            }
            return { success: false, errMsg: "File Upload Error!." };
        });
        this.POSTFILE('/tmp/excel', {
            checkToken: false,
            fileField: 'file',
            mimeTypes: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            multiple: false,
        }, async (req, res, params) => {
            if (req.file) {
                return { success: true, data: { filePath: req.file.path } };
            }
            return { success: false, errMsg: "File Upload Error!." };
        });
        this.POSTFILE('/items/excel', {
            checkToken: true,
            fileField: 'file',
            mimeTypes: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            multiple: false,
        }, async (req, res, params) => {
            if (req.file) {
                return { success: true, data: { filePath: req.file.path } };
            }
            return { success: false, errMsg: "File Upload Error!." };
        });
    }
}

module.exports = new UploadsRoute;