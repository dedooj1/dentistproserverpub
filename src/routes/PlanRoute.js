import AppRoute from "./AppRoute";
import DbPlan from "../db/mysql/DbPlan";

class PlanRoute extends AppRoute {

  init() {

    this.POST('/save', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'planName', required: true, empty: false, minLength: 3, maxLength: 255 },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPlan.insert(params);
    });
    this.POST('/save/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'planName', required: true, empty: false, minLength: 3, maxLength: 255 },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPlan.update(params);
    });
    this.POST('/save/plan_diagnosies/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPlan.updatePlanDiagnosies(params);
    });
    this.POST('/save/plan_tretmants/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPlan.updatePlanTreatments(params);
    });
    this.POST('/save/teeth/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPlan.updatePlanTeeth(params);
    });
    this.POST('/get_one/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.getOne(params);
    });
    this.POST('/get_plan_diagnosies/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.listDiagnosies(params);
    });
    this.POST('/get_teeth/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.listTeeth(params);
    });
    this.POST('/get_plan_treatments/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.listTreatment(params);
    });
    this.POST('/get_one_pk/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.get_one(params);
    });
    this.POST('/list_all', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.get_all(params);
    });
    this.POST('/delete/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.delete(params);
    });
    this.POST('/change_status/active/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.changeStatusActive(params);
    });
    this.POST('/change_status/inactive/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPlan.changeStatusBlocked(params);
    });
    this.POST('/list', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbPlan.list(params);
    });
    this.POST('/list/:id', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbPlan.listPlanPatient(params);
    });

  }
}

module.exports = new PlanRoute;