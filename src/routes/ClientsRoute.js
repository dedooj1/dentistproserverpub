import AppRoute from "./AppRoute";
import DbUser from "../db/mysql/DbUser";
import DbClients from "../db/mysql/DbClients";

class ClientsRoute extends AppRoute {
    init() {
        this.POST('/account_settings', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'name', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'nameLegal', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'tin', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'phoneCode', required: true, empty: false, type: 'text' },
                        { field: 'phoneNumber', required: true, empty: false, type: 'num' },
                        { field: 'idCountry', required: true, empty: false, type: 'int' },
                        { field: 'idCurrency', required: true, empty: false, type: 'int' },
                        { field: 'icon', required: false, empty: true, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            if (params.tokenUser.idRole == DbUser.ROLE.SUPERADMIN) {
                let resultAccountSettings = await DbClients.changeAccountSettings(params);
                if (resultAccountSettings.success) {
                    return await DbUser.getUserInfo(params, params.sidUser);
                }
                return resultAccountSettings;
            }
            return DbClients.returnError("Only Super Admin Can Change Client Account Settings!");
        });
        this.POST('/backup', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbClients.backUpDatabases(params);
        });
        this.POST('/backup/client', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbClients.backUpDatabasesClient(params);
        });
        this.POST('/backup/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbClients.backUpDatabases(params);
        });
        this.POST('/alter', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbClients.alterDatabases(params);
        });
        this.POST('/alter/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbClients.alterDatabases(params);
        });
        this.POST('/getByTokenRegisterWizard/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbClients.getRegisterWizardClientByToken(params);
        });
    }
}

module.exports = new ClientsRoute;