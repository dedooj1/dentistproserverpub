import AppRoute from "./AppRoute";

class DoctorRoute extends AppRoute {

  init() {

    this.POST('/save', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'doctorName', required: true, empty: false, minLength: 3, maxLength: 255 },
            { field: 'email', required: false, empty: true, type: 'text' },
            { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
            { field: 'phoneCode', required: false, empty: true, type: 'text' },
            { field: 'phoneNumber', required: true, empty: true, type: 'num' },
            { field: 'idRole', required: false, empty: true, type: 'int', min: 2, max: 4 },
            { field: 'idPolicy', required: false, empty: true, type: 'int', min: 1 },
            { field: 'medicalSpecialty', required: false, empty: false, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbUser.createUser(params);
    });
    this.POST('/save/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'doctorName', required: true, empty: false, minLength: 3, maxLength: 255 },
            { field: 'email', required: false, empty: true, type: 'text' },
            { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
            { field: 'phoneCode', required: false, empty: true, type: 'text' },
            { field: 'phoneNumber', required: true, empty: true, type: 'num' },
            { field: 'idRole', required: false, empty: true, type: 'int', min: 2, max: 4 },
            { field: 'idPolicy', required: false, empty: true, type: 'int', min: 1 },
            { field: 'medicalSpecialty', required: false, empty: false, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbUser.updateUser(params);
    });
    this.POST('/get_one/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbUser.getOne(params);
    });
    // this.POST('/delete/:id', {
    //   checkToken: true,
    // }, async (req, res, params) => {
    //   return await DbBranchs.deleteBranch(params);
    // });
    this.POST('/list', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbUser.list(params);
    });

  }
}

module.exports = new DoctorRoute;