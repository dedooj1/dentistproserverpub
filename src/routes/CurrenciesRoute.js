import AppRoute from "./AppRoute";
import DbCurrency from "../db/mysql/DbCurrency";

class CurrenciesRoute extends AppRoute {

    init() {
        this.POST('/change_status/active/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.changeStatusActive(params);
        });
        this.POST('/change_status/inactive/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.changeStatusBlocked(params);
        });
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'currName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'currNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'currSimbol', required: true, empty: true, minLength: 1, maxLength: 255 },
                        // { field: 'currIsDefault', required: true, empty: true, type: 'bit' },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbCurrency.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'currName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'currNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'currSimbol', required: true, empty: true, minLength: 1, maxLength: 255 },
                        // { field: 'currIsDefault', required: true, empty: true, type: 'bit' },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbCurrency.update(params);
        });

        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.getOne(params);
        });
        this.POST('/get_one_pk/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.getOneByPk(params);
        });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.fillAll(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbCurrency.deleteByIdKey(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                // { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                                { field: 'status', required: false, empty: false, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbCurrency.list(params);
        });

    }
}

module.exports = new CurrenciesRoute;