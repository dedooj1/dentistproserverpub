import AppRoute from "./AppRoute";
import DbBranchs from "../db/mysql/DbBranchs";

class BranchsRoute extends AppRoute {

    init() {

        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'branchName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'branchDescription', required: true, empty: true, type: 'text' },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbBranchs.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'branchName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'branchDescription', required: true, empty: true, type: 'text' },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbBranchs.update(params);
        });
        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbBranchs.getOne(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbBranchs.deleteBranch(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                                // { field: 'toDate', required: false, empty: true, type: 'date' },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbBranchs.list(params);
        });

    }
}

module.exports = new BranchsRoute;