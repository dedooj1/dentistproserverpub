import AppRoute from "./AppRoute";
import DbUser from "../db/mysql/DbUser";

class UserRoute extends AppRoute {

    init() {
        this.POST('/register', {
            checkToken: false,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'email', required: true, empty: false, type: 'email' },
                        { field: 'name', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'fullName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'idCountry', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'phoneCode', required: true, empty: false, type: 'text' },
                        { field: 'phoneNumber', required: true, empty: false, type: 'num' },
                        { field: 'idPackage', required: false, empty: true, type: 'int', min: 1 },
                        { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.register(params);
        });
        this.POST('/admin/create', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'email', required: true, empty: false, type: 'email' },
                        { field: 'name', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'fullName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'idCountry', required: false, empty: true, type: 'int', min: 1 },
                        { field: 'phoneCode', required: false, empty: true, type: 'text' },
                        { field: 'phoneNumber', required: false, empty: true, type: 'num' },
                        { field: 'idPackage', required: false, empty: true, type: 'int', min: 1 },
                        { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.addAdmin(params);
        });
        this.POST('/confirm/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbUser.confirm(params);
        });
        this.POST('/forgot_password', {
            checkToken: false,
            rules: [{ field: 'email', required: true, empty: false, type: 'email' },],
        }, async (req, res, params) => {
            return await DbUser.forgotPassword(params);
        });
        this.POST('/forgot_password/:id', {
            checkToken: false,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbUser.changePasswordForgot(params);
        });
        this.POST('/login', {
            checkToken: false,
            rules: [
                { field: 'login', required: true, empty: false, type: 'text' },
                { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
            ],
        }, async (req, res, params) => {
            return await DbUser.login(params);
        });
        this.POST('/magic_login/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbUser.magicLogin(params);
        });
        this.POST('/logout', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.logout(params);
        });
        this.POST('/drop_user_client/:id', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbUser.dropUserClient(params);
        });
        this.POST('/account_settings', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'fullName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'phoneCode', required: true, empty: false, type: 'text' },
                        { field: 'phoneNumber', required: true, empty: false, type: 'num' },
                        { field: 'lang', required: true, empty: false, length: 2 },
                        { field: 'login', required: true, empty: false, type: 'text' },
                        { field: 'icon', required: false, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                    ]
                },

            ],
        }, async (req, res, params) => {
            let resultAccountSettings = await DbUser.changeAccountSettings(params);
            if (resultAccountSettings.success) {
                return await DbUser.getUserInfo(params, params.sidUser);
            }
            return resultAccountSettings;
        });
        this.POST('/change_password', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'currentPassword', required: true, empty: false, type: 'text', isMD5: true },
                        { field: 'newPassword', required: true, empty: false, type: 'text', isMD5: true },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.changePassword(params);
        });
        this.POST('/logout', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.logout(params);
        });
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'fullName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'phoneCode', required: true, empty: false, type: 'text' },
                        { field: 'phoneNumber', required: true, empty: false, type: 'num' },
                        { field: 'email', required: true, empty: false, type: 'email' },
                        // { field: 'login', required: true, empty: false, type: 'text', minLength: 4 },
                        { field: 'password', required: true, empty: false, type: 'text', isMD5: true },
                        // { field: 'idRole', required: false, empty: true, type: 'int' },
                        // { field: 'idPolicy', required: false, empty: true, type: 'int', min: 1 },
                        // { field: 'idBranch', required: false, empty: true, type: 'int', min: 1 },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.createUser(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'fullName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'phoneCode', required: true, empty: false, type: 'text' },
                        { field: 'phoneNumber', required: true, empty: false, type: 'num' },
                        { field: 'email', required: true, empty: false, type: 'email' },
                        // { field: 'login', required: true, empty: false, type: 'text', minLength: 4 },
                        { field: 'password', required: false, empty: false, type: 'text', isMD5: true },
                        // { field: 'idRole', required: false, empty: true, type: 'int' },
                        // { field: 'idPolicy', required: false, empty: true, type: 'int', min: 1 },
                        // { field: 'idBranch', required: false, empty: true, type: 'int', min: 1 },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.updateUser(params);
        });
        this.POST('/change_status/active/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.changeStatusActive(params);
        });
        this.POST('/change_status/inactive/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.changeStatusBlocked(params);
        });
        // this.POST('/delete/:id', {
        //     checkToken: true,
        // }, async (req, res, params) => {
        //     return await DbUser.deleteUser(params);
        // });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                { field: 'idRole', required: false, empty: true, type: 'int' },
                                { field: 'idPolicy', required: false, empty: true, type: 'int', min: 1 },
                                { field: 'status', required: false, empty: true, type: 'int', min: 1 },
                                { field: 'idBranch', required: false, empty: true, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUser.list(params);
        });
        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.getOne(params);
        });
        this.POST('/get_settings', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.getSettings(params);
        });
        // this.POST('/check_token', {
        //     checkToken: true,
        //     rules: [],
        // }, async (req, res, params) => {
        //     return await DbUser.checkToken(params)
        // });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.fillAll(params);
        });
        this.POST('/list_all_doctors', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUser.fillAllDoctors(params);
        });
    }
}

module.exports = new UserRoute;