import AppRoute from "./AppRoute";
import DbPatients from "../db/mysql/DbPatients";

class PatientRoute extends AppRoute {

  init() {

    this.POST('/save', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'patientName', required: true, empty: false, minLength: 3, maxLength: 255 },
            { field: 'email', required: false, empty: true, type: 'text' },
            { field: 'phone', required: false, empty: true, type: 'text' },
            { field: 'gerend', required: false, empty: true, type: 'int' },
            { field: 'notes', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPatients.insert(params);
    });
    this.POST('/save/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'patientName', required: true, empty: false, minLength: 3, maxLength: 255 },
            { field: 'email', required: false, empty: true, type: 'text' },
            { field: 'phone', required: false, empty: true, type: 'text' },
            { field: 'gerend', required: false, empty: true, type: 'int' },
            { field: 'notes', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbPatients.update(params);
    });
    this.POST('/get_one/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPatients.getOne(params);
    });
    this.POST('/list_all', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPatients.get_all(params);
    });
    this.POST('/delete/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPatients.delete(params);
    });
    this.POST('/change_status/active/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPatients.changeStatusActive(params);
    });
    this.POST('/change_status/inactive/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbPatients.changeStatusBlocked(params);
    });
    this.POST('/list', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbPatients.list(params);
    });

  }
}

module.exports = new PatientRoute;