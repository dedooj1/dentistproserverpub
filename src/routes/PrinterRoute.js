import AppRoute from "./AppRoute";
import DbPrinter from "../db/mysql/DbPrinter";

class PrinterRoute extends AppRoute {

    init() {
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'printerName', required: true, empty: false, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbPrinter.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'printerName', required: true, empty: false, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbPrinter.update(params);
        });

        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPrinter.getOne(params);
        });
        this.POST('/get_one_pk/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPrinter.getOneByPk(params);
        });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPrinter.fillAll(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPrinter.deleteByIdKey(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                                // { field: 'status', required: false, empty: false, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbPrinter.list(params);
        });

    }
}

module.exports = new PrinterRoute;