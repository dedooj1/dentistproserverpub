import AppRoute from "./AppRoute";
import DbCurrencyMain from "../db/mysql/DbCurrencyMain";
class CurrenciesRouteMain extends AppRoute {
    init() {
        this.POST('/get_all', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbCurrencyMain.get_all(params);
        })
    }
}

module.exports = new CurrenciesRouteMain;