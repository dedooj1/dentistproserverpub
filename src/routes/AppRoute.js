import express from "express";
import DbUserToken from "../db/mysql/DbUserToken";
import validator from 'validator';
import moment from 'moment';
import multer from 'multer';
import uniqid from 'uniqid';
import pathf from 'path';
import { isLatitude, isLongitude } from "../utils";
import { createFolderIfNotExist } from "../utils/FileFunctions";


class AppRoute {

    // 401 UNAuthorization
    // 400 Fields Is required
    // 404 url not found
    // 403 permission denied
    // 200 OK

    constructor() {
        this.route = express.Router();
        this.init();
    }


    async checkRuleValidation(rule, item, parent = "") {
        let value = item[rule.field];
        try {
            value = value.trim();
        } catch (error) {

        }
        if (rule.required) {
            if (!item.hasOwnProperty(rule.field)) {
                return " Field Name { " + parent + " " + rule.field + " } is required";
            }
            if (!rule.empty) {
                switch (rule.type) {
                    case 'array':
                        if (!value.length) {
                            return "Field Name Array { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                    case 'object':
                        if (!value || !Object.keys(value).length) {
                            return "Field Name Object { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                    case 'int':
                        if (!value.toString().length) {
                            return "Field Name Int { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                    case 'num':
                        if (!value.toString().length) {
                            return "Field Name Numeric { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                    case 'bool':
                        if (!value.toString().length) {
                            return "Field Name Boolean { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                    default:
                        if (!value) {
                            return "Field Name Value { " + parent + " " + rule.field + " } is not empty";
                        }
                        break;
                }
            }
        }
        if (value && value.toString().length) {
            switch (rule.type) {
                case 'array':
                    if (!Array.isArray(value)) {
                        return "Field Name { " + parent + " " + rule.field + " } is not Valid array";
                    }
                    break;
                case 'object':
                    if ((typeof value) !== 'object') {
                        return "Field Name { " + parent + " " + rule.field + " } is not Valid object";
                    }
                    break;
                case 'email':
                    if (!validator.isEmail(value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Email";
                    }
                    break;
                case 'enum':
                    if (!rule.enums.includes(value)) {
                        return "Field Name { " + parent + " " + rule.field + " } Must be On Of (" + rule.enums + ")";
                    }
                    break;
                case 'num':
                    if (!validator.isNumeric('' + value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Number";
                    }
                    break;
                case 'text':
                    if (typeof value !== "string") {
                        return "Field Name { " + parent + " " + rule.field + " } is not Valid String";
                    }
                    break;
                case 'int':
                    if (!validator.isInt('' + value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Integer";
                    }
                    break;
                case 'bool':
                    if (!validator.isBoolean('' + value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Boolean";
                    }
                    break;
                case 'date':
                    if (!moment(value).isValid()) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Date Format MM/DD/YYYY,MM-DD-YYYY,MM.DD.YYYY";
                    }
                    break;
                case 'lat':
                    if (!isLatitude(value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Latitude";
                    }
                    break;
                case 'long':
                    if (!isLongitude(value)) {
                        return " Field Name { " + parent + " " + rule.field + " } is not Valid Longitude";
                    }
                    break;
                default:
                    break;
            }
            if (rule.hasOwnProperty("length") && value.length != rule.length) {
                return " Field Name { " + parent + " " + rule.field + " } length {" + rule.minLength + "}";
            }
            if (rule.hasOwnProperty("minLength") && value.length < rule.minLength) {
                return " Field Name { " + parent + " " + rule.field + " } min length {" + rule.minLength + "}";
            }
            if (rule.hasOwnProperty("maxLength") && value.length > rule.maxLength) {
                return " Field Name { " + parent + " " + rule.field + " } max length {" + rule.maxLength + "}";
            }
            if (rule.hasOwnProperty("min") && value < rule.min) {
                return " Field Name { " + parent + " " + rule.field + " } min value {" + rule.min + "}";
            }
            if (rule.hasOwnProperty("max") && value > rule.max) {
                return " Field Name { " + parent + " " + rule.field + " } max value {" + rule.max + "}";
            }
            if (rule.hasOwnProperty("isMD5") && rule.isMD5 && !validator.isMD5(value)) {
                return " Field Name { " + parent + " " + rule.field + " } is not a MD5";
            }
            if (rule.hasOwnProperty("isURL") && rule.isURL && !validator.isURL(value)) {
                return " Field Name { " + parent + " " + rule.field + " } is not a URL";
            }
        }
        return '';
    }

    async checkRule(rules, data, parent) {
        if (!Array.isArray(data)) {
            data = [data];
        }
        for (const dat of data) {
            for (const rule of rules) {

                let errMsg = await this.checkRuleValidation(rule, dat, parent);
                if (errMsg) {
                    return errMsg;
                }
                if (rule.hasOwnProperty("rules")) {
                    let errMsg = await this.checkRule(rule.rules, dat[rule.field], rule.field);
                    if (errMsg) {
                        return errMsg;
                    }
                }
            }
        }
        return '';
    }

    async authenticateRequest(req, res, next, options) {
        if (req.method == 'GET') {
            req.body = req.query;
        }

        if (req.params.id) {
            req.body.search_id = req.params.id;
        }
        if (req.params.key) {
            req.body.search_key = req.params.key;
        }

        let ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        if (ip) {
            req.body.ip = ip;
        }
        let auth = (options && options.checkToken);
        let rules = options.rules ? [...options.rules] : [];

        rules.push({ field: 'device', required: true, empty: false, type: 'enum', enums: ["web", "mobile", "api"] });
        if (req.body.device === "mobile") {
            rules.push({ field: 'device_key', required: true, empty: false, type: 'text' });
            rules.push({ field: 'device_os', required: true, empty: false, type: 'enum', enums: ["ios", "android"] });
        }

        if (rules.length) {
            let errMsg = await this.checkRule(rules, req.body);
            if (errMsg) {
                return res.status(400).send({ success: false, errMsg });
            }
        }
        if (auth) {
            let paaaaram = {};
            let token;
            const authHeader = req.headers['authorization'];
            if (!authHeader || !authHeader.startsWith("Bearer")) return res.status(401).send({ success: false, errMsg: "UNAuthorization" }); // if there isn't any token
            token = authHeader && authHeader.split(' ')[1];
            if (token == null) return res.status(401).send({ success: false, errMsg: "UNAuthorization" }); // if there isn't any token
            let resultUserFromToken = await DbUserToken.getUserByToken(paaaaram, token);
            if (!resultUserFromToken.success) return res.status(401).send({ success: false, errMsg: "UNAuthorization" }); // if there isn't any token
             let resultExpandUserToken = await DbUserToken.expandUserToken(paaaaram, token);
            if (!resultExpandUserToken.success) return res.status(401).send({ success: false, errMsg: "UNAuthorization" }); // if there isn't any token
            req.body.tokenUser = resultUserFromToken.data;
            req.body.sidClient = resultUserFromToken.data.idClient;
            req.body.sidKeyClient = resultUserFromToken.data.client.idKey;
            req.body.sidUser = resultUserFromToken.data.idUser;
            req.body.sidKeyUser = resultUserFromToken.data.idKey;
            req.body.sidRole = resultUserFromToken.data.idRole;
            req.body.accessToken = resultUserFromToken.data.accessToken;
            req.body.sidBranch = resultUserFromToken.data.idBranch;
            req.body.sidPrinter = resultUserFromToken.data.idPrinter;
            req.body.sUserLevel = resultUserFromToken.data.userLevel;
            req.body.slang = resultUserFromToken.data.lang;
        }

        let allowRoles = (options && options.allowRoles);
        if (allowRoles && !allowRoles.includes(req.body.sidRole)) {
            return res.status(403).send({ success: false, errMsg: "You don't have permission for this. (Permission Denied)" }); // Permission Denied
        }

        if (req.body.hasOwnProperty("paginationData") && req.body.paginationData.hasOwnProperty("filter")) {
            if (req.body.paginationData.filter.hasOwnProperty("fromDate") && req.body.paginationData.filter.fromDate.trim()) {
                // req.body.paginationData.filter.fromDate = DbUserToken.formatSQLDate(req.body.paginationData.filter.fromDate, false) + " 00:00:00";
                req.body.paginationData.filter.fromDate = DbUserToken.formatSQLDateMovement(req.body.paginationData.filter.fromDate, false) + " 00:00:00";
            }
            if (req.body.paginationData.filter.hasOwnProperty("toDate") && req.body.paginationData.filter.toDate.trim()) {
                // req.body.paginationData.filter.toDate = DbUserToken.formatSQLDate(req.body.paginationData.filter.toDate, false) + " 23:59:59";
                req.body.paginationData.filter.toDate = DbUserToken.formatSQLDateMovement(req.body.paginationData.filter.toDate, false) + " 23:59:59";
            }
        }
        next && await next();
    }

    getRouter() {
        return this.route;
    }

    GET(path, options, cb) {
        this.route.get(
            path,
            (rq, rs, next) => {
                this.authenticateRequest(rq, rs, next, options)
            },
            async (req, res) => {
                return this.send(res, await cb(req, res));
            });
    }

    POST(path, options, cb) {
        this.route.post(
            path,
            async (rq, rs, next) => await this.authenticateRequest(rq, rs, next, options),
            async (req, res) => {
                return await this.send(res, await cb(req, res, req.body));
            });
    }

    POSTFILE(path, options, cb) {
        this.route.post(
            path,
            async (rq, rs, next) => {
                rq.body.device = 'web';
                await this.authenticateRequest(rq, rs, next, options);
            },
            async (req, res) => {
                const multiple = options.hasOwnProperty("multiple") ? options.multiple : false;
                createFolderIfNotExist('root/asnan/public/tmp');
                var storage = multer.diskStorage({
                    destination: function (req, file, cbm) {
                        cbm(null, 'root/asnan/public/tmp');
                    },
                    filename: function (req, file, cbm) {
                        cbm(null, uniqid() + pathf.extname(file.originalname).toLowerCase());
                    }
                })

                const fileFilter = (req, file, cbm) => {
                    if (options.mimeTypes.includes(file.mimetype)) {
                        cbm(null, true);
                    } else {
                        req.fileValidationError = `Only ${options.mimeTypes.join(', ')} files are allowed!`;
                        cbm(req.fileValidationError, false);
                    }
                }
                let upload;
                if (multiple) {
                    upload = multer({
                        storage: storage,
                        fileFilter: fileFilter
                    }).array(options.fileField, 10);
                } else {
                    upload = multer({
                        storage: storage,
                        fileFilter: fileFilter
                    }).single(options.fileField);
                }

                await upload(req, res, async (err) => {
                    let reqfile = multiple ? req.files : req.file
                    if (req.fileValidationError) {
                        return await this.send(res, { success: false, errMsg: req.fileValidationError });
                    } else if (err) {
                        return await this.send(res, { success: false, errMsg: 'Please select an file to upload' });
                    } else if (!reqfile) {
                        return await this.send(res, { success: false, errMsg: 'Please select an file to upload' });
                    }
                    return await this.send(res, await cb(req, res, req.body));
                });
            })
    }

    // POSTFILES(path, options, cb) {
    //     this.route.post(
    //         path,
    //         async (rq, rs, next) => {
    //             rq.body.device = 'web';
    //             await this.authenticateRequest(rq, rs, next, options);
    //         },
    //         async (req, res) => {
    //             var storage = multer.diskStorage({
    //                 destination: function (req, file, cbm) {
    //                     cbm(null, 'public/tmp2/');
    //                 },
    //                 filename: function (req, file, cbm) {
    //                     cbm(null, uniqid()+pathf.extname(file.originalname).toLowerCase());
    //                 }
    //             })

    //             const fileFilter = (req, file, cbm) => {
    //                 if (options.mimeTypes.includes(file.mimetype)) {
    //                     cbm(null, true);
    //                 } else {
    //                     req.fileValidationError = `Only ${options.mimeTypes.join(', ')} files are allowed!`;
    //                     cbm(req.fileValidationError, false);
    //                 }
    //             }
    //             let upload = multer({
    //                 storage: storage,
    //                 fileFilter: fileFilter
    //             }).array(options.fileField, 10);

    //             await upload(req, res, async (err) => {
    //                 if (req.fileValidationError) {
    //                     return await this.send(res, { success: false, errMsg: req.fileValidationError });
    //                 } else if (err) {
    //                     return await this.send(res, { success: false, errMsg: 'Please select an file to upload' });
    //                 } else if (!req.files) {
    //                     return await this.send(res, { success: false, errMsg: 'Please select an file to upload' });
    //                 }
    //                 return await this.send(res, await cb(req, res, req.body));
    //             });
    //         })
    // }


    PUT(path, options, cb) {
        this.route.put(
            path,
            (rq, rs, next) => this.authenticateRequest(rq, rs, next, options),
            async (req, res) => {
                return this.send(res, await cb(req, res, req.body));
            });
    }

    PATCH(path, options, cb) {
        this.route.put(
            path,
            (rq, rs, next) => this.authenticateRequest(rq, rs, next, options),
            async (req, res) => {
                return this.send(res, await cb(req, res, req.body));
            });
    }

    DELETE(path, options, cb) {
        this.route.put(
            path,
            (rq, rs, next) => this.authenticateRequest(rq, rs, next, options),
            async (req, res) => {
                return this.send(res, await cb(req, res, req.body));
            });
    }

    async send(res, result) {
        if (result.success) {
            return res.status(200).json(result);
        } else {
            return res.status(400).json(result);
        }
    }

    log(...text) {
        console.warn("JSRC LOG => ", text);
    }

}

export default AppRoute;