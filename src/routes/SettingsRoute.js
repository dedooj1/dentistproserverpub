import AppRoute from "./AppRoute";
import DbSettings from "../db/mysql/DbSettings";

class SettingsRoute extends AppRoute {

    init() {
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        // { field: 'accGrpName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'accGrpNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            // return await DbSettings.insert(params);
            return await DbSettings.saveSettingsBranch(params);
        });
        this.POST('/get_one', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbSettings.getOne(params);
        });
        this.POST('/get_initialTeeth', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbSettings.getInitialTeeth(params);
        });
        // this.POST('/get_one_pk/:id', {
        //     checkToken: true,
        // }, async (req, res, params) => {
        //     return await DbSettings.getOneByPk(params);
        // });
    }
}

module.exports = new SettingsRoute;