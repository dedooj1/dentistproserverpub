import DbUnit from "../db/mysql/DbUnit";
import AppRoute from "./AppRoute";

class UnitRoute extends AppRoute {

    init() {
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'unitName', required: true, empty: false, minLength: 2, maxLength: 255 },
                        // { field: 'unitNameEn', required: true, empty: true, minLength: 2, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbUnit.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'unitName', required: true, empty: false, minLength: 2, maxLength: 255 },
                        { field: 'unitNameEn', required: true, empty: true, minLength: 2, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbUnit.update(params);
        });

        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUnit.getOne(params);
        });
        this.POST('/get_one_pk/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUnit.getOneByPk(params);
        });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUnit.fillAll(params);
        });
        this.POST('/list_by_iditem/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUnit.fillByidItem(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUnit.deleteByIdKey(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                // { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                                // { field: 'status', required: false, empty: false, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUnit.list(params);
        });

    }
}

module.exports = new UnitRoute;