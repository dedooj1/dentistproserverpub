import AppRoute from "./AppRoute";
import DbPackages from "../db/mysql/DbPackages";

class PackagesRoute extends AppRoute {
    init() {
        this.POST('/get_all', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbPackages.get_all(params);
        });
    }
}

module.exports = new PackagesRoute;