import DbUsersPolicy from "../db/mysql/DbUsersPolicy";
import AppRoute from "./AppRoute";

class UsersPolicyRoute extends AppRoute {

    init() {
        this.POST('/change_status/active/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.changeStatusActive(params);
        });
        this.POST('/change_status/inactive/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.changeStatusBlocked(params);
        });
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'polName', required: true, empty: false, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbUsersPolicy.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'polName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'polNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbUsersPolicy.update(params);
        });

        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.getOne(params);
        });
        this.POST('/get_one_pk/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.getOneByPk(params);
        });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.fillAll(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbUsersPolicy.deleteByIdKey(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                // { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                                { field: 'status', required: false, empty: false, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbUsersPolicy.list(params);
        });

    }
}

module.exports = new UsersPolicyRoute;