import AppRoute from "./AppRoute";
import DbTreatments from "../db/mysql/DbTreatments";

class TreatmentRoute extends AppRoute {

  init() {

    this.POST('/save', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'treatmentsName', required: true, empty: false, minLength: 3, maxLength: 255 },
            // { field: 'optionList', required: false, empty: true, type: 'text' },
            { field: 'optionListType', required: false, empty: true, type: 'int' },
            { field: 'type', required: false, empty: true, type: 'int' },
            { field: 'description', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbTreatments.insert(params);
    });
    this.POST('/save/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'treatmentsName', required: true, empty: false, minLength: 3, maxLength: 255 },
            // { field: 'optionList', required: false, empty: true, type: 'text' },
            { field: 'optionListType', required: false, empty: true, type: 'int' },
            { field: 'type', required: false, empty: true, type: 'int' },
            { field: 'description', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbTreatments.update(params);
    });
    this.POST('/get_one/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.getOne(params);
    });
    this.POST('/get_by_type/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.getOneByType(params);
    });
    this.POST('/list_all', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.get_all(params);
    });
    this.POST('/delete/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.delete(params);
    });
    this.POST('/change_status/active/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.changeStatusActive(params);
    });
    this.POST('/change_status/inactive/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTreatments.changeStatusBlocked(params);
    });
    this.POST('/list', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbTreatments.list(params);
    });

  }
}

module.exports = new TreatmentRoute;