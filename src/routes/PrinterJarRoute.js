import AppRoute from "./AppRoute";
import usePrinterJar from "../utils/usePrinterJar";
// import moment from "moment";
// import { formatDouble } from "../utils";

class PrinterJarRoute extends AppRoute {

    init() {
        this.POST('/get_syatem_printer_list', {
            checkToken: true,
        }, async (req, res, params) => {
            let result = await usePrinterJar.getPrinterList();
            result.data = result.data.map((oo) => {
                return { printerName: oo }
            })
            return result;
        });
    }
}

module.exports = new PrinterJarRoute;