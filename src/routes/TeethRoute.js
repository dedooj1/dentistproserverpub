import AppRoute from "./AppRoute";
import DbTeeth from "../db/mysql/DbTeeth";

class TeethRoute extends AppRoute {

  init() {

    this.POST('/save', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'teethNameNumber', required: false, empty: false, maxLength: 255 },
            { field: 'teethName1', required: true, empty: false, maxLength: 255 },
            { field: 'teethName2', required: false, empty: false, maxLength: 255 },
            { field: 'teethName3', required: false, empty: false, maxLength: 255 },
            { field: 'teethName4', required: false, empty: false, maxLength: 255 },
            { field: 'teethName5', required: false, empty: false, maxLength: 255 },
            { field: 'TeethImage', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbTeeth.insert(params);
    });
    this.POST('/save/:id', {
      checkToken: true,
      rules: [
        {
          field: 'saveData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'teethNameNumber', required: false, empty: false, maxLength: 255 },
            { field: 'teethName1', required: true, empty: false, maxLength: 255 },
            { field: 'teethName2', required: false, empty: false, maxLength: 255 },
            { field: 'teethName3', required: false, empty: false, maxLength: 255 },
            { field: 'teethName4', required: false, empty: false, maxLength: 255 },
            { field: 'teethName5', required: false, empty: false, maxLength: 255 },
            { field: 'TeethImage', required: false, empty: true, type: 'text' },
          ]
        },
      ],
    }, async (req, res, params) => {
      return await DbTeeth.update(params);
    });
    this.POST('/get_one/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.getOne(params);
    });
    this.POST('/get_by_type/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.getOneByType(params);
    });
    this.POST('/list_all', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.get_all(params);
    });
    this.POST('/delete/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.delete(params);
    });
    this.POST('/change_status/active/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.changeStatusActive(params);
    });
    this.POST('/change_status/inactive/:id', {
      checkToken: true,
    }, async (req, res, params) => {
      return await DbTeeth.changeStatusBlocked(params);
    });
    this.POST('/list', {
      checkToken: true,
      rules: [
        {
          field: 'paginationData',
          required: true,
          empty: false,
          type: 'object',
          rules: [
            { field: 'page', required: true, empty: false, type: 'int', min: 0 },
            { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
            { field: 'q', required: true, empty: true, type: 'text' },
            {
              field: 'filter',
              required: true,
              empty: true,
              type: 'object',
              rules: [
                // { field: 'fromDate', required: false, empty: true, type: 'date' },
                // { field: 'toDate', required: false, empty: true, type: 'date' },
              ]
            },
            {
              field: 'sort',
              required: true,
              empty: true,
              type: 'object',
              rules: [

              ]
            },
          ]
        },

      ],
    }, async (req, res, params) => {
      return await DbTeeth.list(params);
    });

  }
}

module.exports = new TeethRoute;