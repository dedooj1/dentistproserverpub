import AppRoute from "./AppRoute";
import DbCountries from "../db/mysql/DbCountries";
class CountriesRoute extends AppRoute {
    init() {
        this.POST('/get_all', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbCountries.get_all(params);
        });
    }
}

module.exports = new CountriesRoute;