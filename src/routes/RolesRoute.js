import AppRoute from "./AppRoute";
import DbRoles from "../db/mysql/DbRoles";

class RolesRoute extends AppRoute {
    init() {
        this.POST('/get_all', {
            checkToken: false,
        }, async (req, res, params) => {
            return await DbRoles.get_all(params);
        });
    }
}

module.exports = new RolesRoute;