import UserRoute from "./UserRoute";
import ClientsRoute from "./ClientsRoute";
import CountriesRoute from "./CountriesRoute";
import CurrenciesRouteMain from "./CurrenciesRouteMain";
import PackagesRoute from "./PackagesRoute";
import RolesRoute from "./RolesRoute";
import UploadsRoute from "./UploadsRoute";
import BranchsRoute from "./BranchsRoute";
import CurrenciesRoute from "./CurrenciesRoute";
import SettingsRoute from "./SettingsRoute";
import UsersPolicyRoute from "./UsersPolicyRoute";
import PrinterRoute from "./PrinterRoute";
import PrinterJarRoute from "./PrinterJarRoute";
import PatientRoute from "./PatientRoute";
import DiagnosiesRoute from "./DiagnosiesRoute";
import TreatmentRoute from "./TreatmentRoute";
import StageRoute from "./StageRoute";
import PlanRoute from "./PlanRoute";
import TeethRoute from "./TeethRoute";

const version1 = '/api/v1/';


export default (app) => {

    function appUse(path, router) {
        app.use(version1 + path, router);
    }

    appUse('users', UserRoute.getRouter());
    appUse('clients', ClientsRoute.getRouter());
    appUse('countries', CountriesRoute.getRouter());
    appUse('currencies', CurrenciesRouteMain.getRouter());
    appUse('packages', PackagesRoute.getRouter());
    appUse('roles', RolesRoute.getRouter());
    appUse('uploads', UploadsRoute.getRouter());

    // Custom Db
    appUse('branchs', BranchsRoute.getRouter());
    appUse('patients', PatientRoute.getRouter());
    appUse('diagnosieis', DiagnosiesRoute.getRouter());
    appUse('treatments', TreatmentRoute.getRouter());
    appUse('stages', StageRoute.getRouter());
    appUse('teeths', TeethRoute.getRouter());
    appUse('plans', PlanRoute.getRouter());
    appUse('currency', CurrenciesRoute.getRouter());
    appUse('settings', SettingsRoute.getRouter());
    appUse('users/policy', UsersPolicyRoute.getRouter());
    appUse('printer', PrinterRoute.getRouter());
    appUse('system/printer', PrinterJarRoute.getRouter());
    
};