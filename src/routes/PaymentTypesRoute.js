import AppRoute from "./AppRoute";
import DbPaymentTypes from "../db/mysql/DbPaymentTypes";

class PaymentTypesRoute extends AppRoute {

    init() {

        this.POST('/change_status/active/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.changeStatusActive(params);
        });
        this.POST('/change_status/inactive/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.changeStatusBlocked(params);
        });
        this.POST('/list_all', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.fillAll(params);
        });
        this.POST('/save', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'payName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        // { field: 'payNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'payTypeModel', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'paySettings', required: true, empty: true, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbPaymentTypes.insert(params);
        });
        this.POST('/save/:id', {
            checkToken: true,
            rules: [
                {
                    field: 'saveData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'payName', required: true, empty: false, minLength: 3, maxLength: 255 },
                        { field: 'payNameEn', required: true, empty: true, minLength: 3, maxLength: 255 },
                        { field: 'payTypeModel', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'paySettings', required: true, empty: true, minLength: 3, maxLength: 255 },
                    ]
                },
            ],
        }, async (req, res, params) => {
            return await DbPaymentTypes.update(params);
        });

        this.POST('/get_one/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.getOne(params);
        });
        this.POST('/get_one_pk/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.getOneByPk(params);
        });
        this.POST('/delete/:id', {
            checkToken: true,
        }, async (req, res, params) => {
            return await DbPaymentTypes.deleteByIdKey(params);
        });
        this.POST('/list', {
            checkToken: true,
            rules: [
                {
                    field: 'paginationData',
                    required: true,
                    empty: false,
                    type: 'object',
                    rules: [
                        { field: 'page', required: true, empty: false, type: 'int', min: 0 },
                        { field: 'perPage', required: true, empty: false, type: 'int', min: 1 },
                        { field: 'q', required: true, empty: true, type: 'text' },
                        {
                            field: 'filter',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [
                                { field: 'idBranch', required: false, empty: false, type: 'int', min: 1 },
                                { field: 'status', required: false, empty: false, type: 'int', min: 1 },
                            ]
                        },
                        {
                            field: 'sort',
                            required: true,
                            empty: true,
                            type: 'object',
                            rules: [

                            ]
                        },
                    ]
                },

            ],
        }, async (req, res, params) => {
            return await DbPaymentTypes.list(params);
        });

    }
}

module.exports = new PaymentTypesRoute;