-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2021 at 10:14 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbaccountingv6_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `ard_clients`
--

CREATE TABLE `ard_clients` (
  `idClient` int(11) NOT NULL,
  `idKey` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nameLegal` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `idCountry` int(11) NOT NULL,
  `idCurrency` int(11) NOT NULL,
  `phoneCode` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `status` int(3) NOT NULL COMMENT '1-pending 2 trial 3 active 4 blocked',
  `idPackage` int(11) NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `updateDate` timestamp NULL DEFAULT NULL,
  `paidUntilDate` timestamp NULL DEFAULT NULL,
  `activeUntilDate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_clients`
--

INSERT INTO `ard_clients` (`idClient`, `idKey`, `name`, `nameLegal`, `tin`, `email`, `icon`, `idCountry`, `idCurrency`, `phoneCode`, `phoneNumber`, `status`, `idPackage`, `createDate`, `updateDate`, `paidUntilDate`, `activeUntilDate`) VALUES
(60, '80a50286aa22caf995ef1676f2ce353b', 'JSOURCE_JSRC', NULL, NULL, 'nazariank@yahoo.com', NULL, 11, 11, '+374', '94775309', 2, 1, '2021-04-12 21:39:44', '2021-04-27 17:10:00', NULL, '2021-07-26 21:39:54');

-- --------------------------------------------------------

--
-- Table structure for table `ard_countries`
--

CREATE TABLE `ard_countries` (
  `idCountry` int(3) UNSIGNED NOT NULL,
  `countryName` varchar(200) DEFAULT NULL,
  `countryNameRu` varchar(255) DEFAULT NULL,
  `countryCode` varchar(2) DEFAULT NULL,
  `useProject` int(1) DEFAULT 0,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `flag` varchar(6) DEFAULT NULL,
  `idCurrency` varchar(20) DEFAULT NULL,
  `countryLang` varchar(20) DEFAULT 'en',
  `phoneCode` varchar(10) DEFAULT NULL,
  `phoneCodeLength` int(2) DEFAULT 1,
  `ordering` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_countries`
--

INSERT INTO `ard_countries` (`idCountry`, `countryName`, `countryNameRu`, `countryCode`, `useProject`, `iso_alpha3`, `iso_numeric`, `flag`, `idCurrency`, `countryLang`, `phoneCode`, `phoneCodeLength`, `ordering`) VALUES
(1, 'Afghanistan', 'Афганистан', 'AF', 0, 'AFG', 4, 'AF.png', '1', 'en', '+93', 1, 1000),
(2, 'Albania', 'Албания', 'AL', 0, 'ALB', 8, 'AL.png', '2', 'en', '+355', 1, 1000),
(3, 'Algeria', 'Алжир', 'DZ', 0, 'DZA', 12, 'DZ.png', '3', 'en', '+213', 1, 1000),
(4, 'American Samoa', 'Американский Самоа\r\n', 'AS', 0, 'ASM', 16, 'AS.png', '4', 'en', '+1684', 1, 1000),
(5, 'Andorra', 'Андорра', 'AD', 0, 'AND', 20, 'AD.png', '5', 'en', '+376', 1, 1000),
(6, 'Angola', 'Ангола', 'AO', 0, 'AGO', 24, 'AO.png', '6', 'en', '+244', 1, 1000),
(8, 'Antarctica', 'Антарктида', 'AQ', 0, 'ATA', 10, 'AQ.png', '8', 'en', '+672', 1, 1000),
(9, 'Antigua and Barbuda', 'Антигуа и Барбуда', 'AG', 0, 'ATG', 28, 'AG.png', '7', 'en', '+1268', 1, 1000),
(10, 'Argentina', 'Аргентина', 'AR', 0, 'ARG', 32, 'AR.png', '10', 'en', '+54', 1, 1000),
(11, 'Armenia', 'Армения', 'AM', 1, 'ARM', 51, 'AM.png', '11', 'en', '+374', 8, 1000),
(12, 'Aruba', 'Аруба', 'AW', 0, 'ABW', 533, 'AW.png', '12', 'en', '+297', 1, 1000),
(13, 'Australia', 'Австралия', 'AU', 0, 'AUS', 36, 'AU.png', '13', 'en', '+61', 1, 1000),
(14, 'Austria', 'Австрия', 'AT', 0, 'AUT', 40, 'AT.png', '5', 'en', '+43', 1, 1000),
(16, 'Bahamas', 'Багамские Острова', 'BS', 0, 'BHS', 44, 'BS.png', '16', 'en', '+1242', 1, 1000),
(17, 'Bahrain', 'Бахрейн', 'BH', 0, 'BHR', 48, 'BH.png', '17', 'en', '+973', 1, 1000),
(18, 'Bangladesh', 'Бангладеш', 'BD', 0, 'BGD', 50, 'BD.png', '18', 'en', '+880', 1, 1000),
(19, 'Barbados', 'Барбадос', 'BB', 0, 'BRB', 52, 'BB.png', '19', 'en', '+1246', 1, 1000),
(20, 'Belarus', 'Беларусь', 'BY', 0, 'BLR', 112, 'BY.png', '20', 'en', '+375', 1, 1000),
(21, 'Belgium', 'Бельгия', 'BE', 0, 'BEL', 56, 'BE.png', '5', 'en', '+32', 1, 1000),
(22, 'Belize', 'Белиз', 'BZ', 0, 'BLZ', 84, 'BZ.png', '22', 'en', '+501', 1, 1000),
(23, 'Benin', 'Бенин', 'BJ', 0, 'BEN', 204, 'BJ.png', '23', 'en', '+229', 1, 1000),
(24, 'Bermuda', 'Бермудские Острова', 'BM', 0, 'BMU', 60, 'BM.png', '24', 'en', '+1441', 1, 1000),
(25, 'Bhutan', 'Бутан', 'BT', 0, 'BTN', 64, 'BT.png', '25', 'en', '+975', 1, 1000),
(26, 'Bolivia', 'Боливия (Многонациональное Государство)', 'BO', 0, 'BOL', 68, 'BO.png', '26', 'en', '+591', 1, 1000),
(27, 'Bosnia and Herzegovina', 'Босния и Герцеговина', 'BA', 0, 'BIH', 70, 'BA.png', '27', 'en', '+387', 1, 1000),
(28, 'Botswana', 'Ботсвана', 'BW', 0, 'BWA', 72, 'BW.png', '28', 'en', '+267', 1, 1000),
(29, 'Bouvet Island', 'Остров Буве', 'BV', 0, 'BVT', 74, 'BV.png', '29', 'en', '+47', 1, 1000),
(30, 'Brazil', 'Бразилия', 'BR', 0, 'BRA', 76, 'BR.png', '30', 'en', '+55', 1, 1000),
(31, 'British Indian Ocean Territory', 'Британская территория Индийского океана', 'IO', 0, 'IOT', 86, 'IO.png', '4', 'en', '+246', 1, 1000),
(32, 'British Virgin Islands', 'Британская территория Индийского океана', 'VG', 0, 'VGB', 92, 'VG.png', '4', 'en', '+1284', 1, 1000),
(33, 'Brunei', 'Бруней-Даруссалам', 'BN', 0, 'BRN', 96, 'BN.png', '33', 'en', '+673', 1, 1000),
(34, 'Bulgaria', 'Болгария', 'BG', 0, 'BGR', 100, 'BG.png', '34', 'en', '+359', 1, 1000),
(35, 'Burkina Faso', 'Буркина-Фасо', 'BF', 0, 'BFA', 854, 'BF.png', '23', 'en', '+226', 1, 1000),
(36, 'Burundi', 'Бурунди', 'BI', 0, 'BDI', 108, 'BI.png', '36', 'en', '+257', 1, 1000),
(37, 'Cambodia', 'Камбоджа', 'KH', 0, 'KHM', 116, 'KH.png', '37', 'en', '+855', 1, 1000),
(38, 'Cameroon', 'Камерун', 'CM', 0, 'CMR', 120, 'CM.png', '38', 'en', '+237', 1, 1000),
(39, 'Canada', 'Канада', 'CA', 0, 'CAN', 124, 'CA.png', '39', 'en', '+1', 1, 1000),
(40, 'Cape Verde', 'Кабо-Верде', 'CV', 0, 'CPV', 132, 'CV.png', '40', 'en', '+238', 1, 1000),
(41, 'Cayman Islands', 'Каймановы острова\r\n', 'KY', 0, 'CYM', 136, 'KY.png', '41', 'en', '+1345', 1, 1000),
(42, 'Central African Republic', 'Центральноафриканская Республика', 'CF', 0, 'CAF', 140, 'CF.png', '38', 'en', '+236', 1, 1000),
(43, 'Chad', 'Чад', 'TD', 0, 'TCD', 148, 'TD.png', '38', 'en', '+235', 1, 1000),
(44, 'Chile', 'Чили', 'CL', 0, 'CHL', 152, 'CL.png', '44', 'en', '+56', 1, 1000),
(45, 'China', 'Китай', 'CN', 0, 'CHN', 156, 'CN.png', '45', 'en', '+86', 1, 1000),
(46, 'Christmas Island', 'Остров Рождества', 'CX', 0, 'CXR', 162, 'CX.png', '13', 'en', '+61', 1, 1000),
(47, 'Cocos Islands', 'Кокосовые острова', 'CC', 0, 'CCK', 166, 'CC.png', '13', 'en', '+61', 1, 1000),
(48, 'Colombia', 'Колумбия', 'CO', 0, 'COL', 170, 'CO.png', '48', 'en', '+57', 1, 1000),
(49, 'Comoros', 'Коморские острова', 'KM', 0, 'COM', 174, 'KM.png', '49', 'en', '+269', 1, 1000),
(50, 'Cook Islands', 'Острова Кука', 'CK', 0, 'COK', 184, 'CK.png', '50', 'en', '+682', 1, 1000),
(51, 'Costa Rica', 'Коста-Рика', 'CR', 0, 'CRI', 188, 'CR.png', '51', 'en', '+506', 1, 1000),
(52, 'Croatia', 'Хорватия', 'HR', 0, 'HRV', 191, 'HR.png', '52', 'en', '+385', 1, 1000),
(53, 'Cuba', 'Куба', 'CU', 0, 'CUB', 192, 'CU.png', '53', 'en', '+53', 1, 1000),
(54, 'Cyprus', 'Кипр', 'CY', 0, 'CYP', 196, 'CY.png', '54', 'en', '+357', 1, 1000),
(55, 'Czech Republic', 'Чехия', 'CZ', 0, 'CZE', 203, 'CZ.png', '55', 'en', '+420', 1, 1000),
(56, 'Democratic Republic of the Congo', 'Демократическая Республика Конго', 'CD', 0, 'COD', 180, 'CD.png', '56', 'en', '+243', 1, 1000),
(57, 'Denmark', 'Дания', 'DK', 0, 'DNK', 208, 'DK.png', '57', 'en', '+45', 1, 1000),
(58, 'Djibouti', 'Джибути', 'DJ', 0, 'DJI', 262, 'DJ.png', '58', 'en', '+253', 1, 1000),
(59, 'Dominica', 'Доминика', 'DM', 0, 'DMA', 212, 'DM.png', '7', 'en', '+1767', 1, 1000),
(60, 'Dominican Republic', 'Доминиканская Республика', 'DO', 0, 'DOM', 214, 'DO.png', '60', 'en', '+1809', 1, 1000),
(61, 'East Timor', 'Восточный Тимор', 'TL', 0, 'TLS', 626, 'TL.png', '4', 'en', '+670', 1, 1000),
(62, 'Ecuador', 'Эквадор', 'EC', 0, 'ECU', 218, 'EC.png', '4', 'en', '+593', 1, 1000),
(63, 'Egypt', 'Египет', 'EG', 1, 'EGY', 818, 'EG.png', '63', 'en', '+20', 1, 2),
(64, 'El Salvador', 'Сальвадор', 'SV', 0, 'SLV', 222, 'SV.png', '64', 'en', '+503', 1, 1000),
(65, 'Equatorial Guinea', 'Экваториальная Гвинея', 'GQ', 0, 'GNQ', 226, 'GQ.png', '38', 'en', '+240', 1, 1000),
(66, 'Eritrea', 'Эритрея', 'ER', 0, 'ERI', 232, 'ER.png', '66', 'en', '+291', 1, 1000),
(67, 'Estonia', 'Эстония', 'EE', 0, 'EST', 233, 'EE.png', '67', 'en', '+372', 1, 1000),
(68, 'Ethiopia', 'Эфиопия', 'ET', 0, 'ETH', 231, 'ET.png', '68', 'en', '+251', 1, 1000),
(69, 'Falkland Islands', 'Фолклендские острова', 'FK', 0, 'FLK', 238, 'FK.png', '69', 'en', '+500', 1, 1000),
(70, 'Faroe Islands', 'Фарерские острова\r\n', 'FO', 0, 'FRO', 234, 'FO.png', '57', 'en', '+298', 1, 1000),
(71, 'Fiji', 'Фиджи', 'FJ', 0, 'FJI', 242, 'FJ.png', '71', 'en', '+679', 1, 1000),
(72, 'Finland', 'Финляндия', 'FI', 0, 'FIN', 246, 'FI.png', '5', 'en', '+358', 1, 1000),
(73, 'France', 'Франция', 'FR', 0, 'FRA', 250, 'FR.png', '5', 'en', '+33', 1, 1000),
(74, 'French Guiana', 'Французская Гвиана\r\n', 'GF', 0, 'GUF', 254, 'GF.png', '5', 'en', '+594', 1, 1000),
(75, 'French Polynesia', 'Французская Полинезия\r\n', 'PF', 0, 'PYF', 258, 'PF.png', '75', 'en', '+689', 1, 1000),
(76, 'French Southern Territories', 'Южные Французские Территории\r\n', 'TF', 0, 'ATF', 260, 'TF.png', '5', 'en', '+262', 1, 1000),
(77, 'Gabon', 'Габон', 'GA', 0, 'GAB', 266, 'GA.png', '38', 'en', '+241', 1, 1000),
(78, 'Gambia', 'Гамбия', 'GM', 0, 'GMB', 270, 'GM.png', '78', 'en', '+220', 1, 1000),
(79, 'Georgia', 'Грузия', 'GE', 1, 'GEO', 268, 'GE.png', '79', 'ka', '+995', 9, 1000),
(80, 'Germany', 'Германия', 'DE', 0, 'DEU', 276, 'DE.png', '5', 'en', '+49', 1, 1000),
(81, 'Ghana', 'Гана', 'GH', 0, 'GHA', 288, 'GH.png', '81', 'en', '+233', 1, 1000),
(82, 'Gibraltar', 'Гибралтар', 'GI', 0, 'GIB', 292, 'GI.png', '82', 'en', '+350', 1, 1000),
(83, 'Greece', 'Греция', 'GR', 0, 'GRC', 300, 'GR.png', '5', 'en', '+30', 1, 1000),
(84, 'Greenland', 'Гренландия', 'GL', 0, 'GRL', 304, 'GL.png', '57', 'en', '+299', 1, 1000),
(85, 'Grenada', 'Гренада', 'GD', 0, 'GRD', 308, 'GD.png', '7', 'en', '+1473', 1, 1000),
(86, 'Guadeloupe', 'Гваделупа', 'GP', 0, 'GLP', 312, 'GP.png', '5', 'en', '+590', 1, 1000),
(87, 'Guam', 'Гуам', 'GU', 0, 'GUM', 316, 'GU.png', '4', 'en', '+1671', 1, 1000),
(88, 'Guatemala', 'Гватемала', 'GT', 0, 'GTM', 320, 'GT.png', '88', 'en', '+502', 1, 1000),
(89, 'Guinea', 'Гвинея', 'GN', 0, 'GIN', 324, 'GN.png', '89', 'en', '+224', 1, 1000),
(90, 'Guinea-Bissau', 'Гвинея-Бисау', 'GW', 0, 'GNB', 624, 'GW.png', '23', 'en', '+245', 1, 1000),
(91, 'Guyana', 'Гайана', 'GY', 0, 'GUY', 328, 'GY.png', '91', 'en', '+592', 1, 1000),
(92, 'Haiti', 'Гаити', 'HT', 0, 'HTI', 332, 'HT.png', '92', 'en', '+509', 1, 1000),
(93, 'Heard Island and McDonald Islands', 'Острова Херд и Макдональд', 'HM', 0, 'HMD', 334, 'HM.png', '13', 'en', '+0', 1, 1000),
(94, 'Honduras', 'Гондурас', 'HN', 0, 'HND', 340, 'HN.png', '94', 'en', '+504', 1, 1000),
(95, 'Hong Kong', 'Гонконг', 'HK', 0, 'HKG', 344, 'HK.png', '95', 'en', '+852', 1, 1000),
(96, 'Hungary', 'Венгрия', 'HU', 0, 'HUN', 348, 'HU.png', '96', 'en', '+36', 1, 1000),
(97, 'Iceland', 'Исландия', 'IS', 0, 'ISL', 352, 'IS.png', '97', 'en', '+354', 1, 1000),
(98, 'India', 'Индия', 'IN', 0, 'IND', 356, 'IN.png', '98', 'en', '+91', 1, 1000),
(99, 'Indonesia', 'Индонезия', 'ID', 0, 'IDN', 360, 'ID.png', '99', 'en', '+62', 1, 1000),
(100, 'Iran', 'Иран', 'IR', 0, 'IRN', 364, 'IR.png', '100', 'en', '+98', 1, 1000),
(101, 'Iraq', 'Ирак', 'IQ', 0, 'IRQ', 368, 'IQ.png', '101', 'en', '+964', 1, 1000),
(102, 'Ireland', 'Ирландия', 'IE', 0, 'IRL', 372, 'IE.png', '5', 'en', '+353', 1, 1000),
(103, 'Israel', 'Израиль', 'IL', 0, 'ISR', 376, 'IL.png', '103', 'en', '+972', 1, 1000),
(104, 'Italy', 'Италия', 'IT', 0, 'ITA', 380, 'IT.png', '5', 'en', '+39', 1, 1000),
(105, 'Ivory Coast', 'Кот-д\'Ивуар', 'CI', 0, 'CIV', 384, 'CI.png', '23', 'en', '+225', 1, 1000),
(106, 'Jamaica', 'Ямайка', 'JM', 0, 'JAM', 388, 'JM.png', '106', 'en', '+1876', 1, 1000),
(107, 'Japan', 'Япония', 'JP', 0, 'JPN', 392, 'JP.png', '107', 'en', '+81', 1, 1000),
(108, 'Jordan', 'Иордания', 'JO', 0, 'JOR', 400, 'JO.png', '108', 'en', '+962', 1, 1000),
(109, 'Kazakhstan', 'Казахстан', 'KZ', 0, 'KAZ', 398, 'KZ.png', '109', 'en', '+7', 1, 1000),
(110, 'Kenya', 'Кения', 'KE', 0, 'KEN', 404, 'KE.png', '110', 'en', '+254', 1, 1000),
(111, 'Kiribati', 'Кирибати', 'KI', 0, 'KIR', 296, 'KI.png', '13', 'en', '+686', 1, 1000),
(112, 'Kuwait', 'Кувейт', 'KW', 0, 'KWT', 414, 'KW.png', '112', 'en', '+965', 1, 1000),
(113, 'Kyrgyzstan', 'Кыргызстан', 'KG', 0, 'KGZ', 417, 'KG.png', '113', 'en', '+996', 1, 1000),
(114, 'Laos', 'Лаос', 'LA', 0, 'LAO', 418, 'LA.png', '114', 'en', '+856', 1, 1000),
(115, 'Latvia', 'Латвия', 'LV', 0, 'LVA', 428, 'LV.png', '115', 'en', '+371', 1, 1000),
(116, 'Lebanon', 'Ливан', 'LB', 0, 'LBN', 422, 'LB.png', '116', 'en', '+961', 1, 1000),
(117, 'Lesotho', 'Лесото', 'LS', 0, 'LSO', 426, 'LS.png', '117', 'en', '+266', 1, 1000),
(118, 'Liberia', 'Либерия', 'LR', 0, 'LBR', 430, 'LR.png', '118', 'en', '+231', 1, 1000),
(119, 'Libya', 'Ливия', 'LY', 0, 'LBY', 434, 'LY.png', '119', 'en', '+218', 1, 1000),
(120, 'Liechtenstein', 'Лихтенштейн', 'LI', 0, 'LIE', 438, 'LI.png', '120', 'en', '+423', 1, 1000),
(121, 'Lithuania', 'Литва', 'LT', 0, 'LTU', 440, 'LT.png', '121', 'en', '+370', 1, 1000),
(122, 'Luxembourg', 'Люксембург', 'LU', 0, 'LUX', 442, 'LU.png', '5', 'en', '+352', 1, 1000),
(123, 'Macao', 'Macao', 'MO', 0, 'MAC', 446, 'MO.png', '123', 'en', '+853', 1, 1000),
(124, 'Macedonia', 'Македония', 'MK', 0, 'MKD', 807, 'MK.png', '124', 'en', '+389', 1, 1000),
(125, 'Madagascar', 'Мадагаскар', 'MG', 0, 'MDG', 450, 'MG.png', '125', 'en', '+261', 1, 1000),
(126, 'Malawi', 'Малави', 'MW', 0, 'MWI', 454, 'MW.png', '126', 'en', '+265', 1, 1000),
(127, 'Malaysia', 'Малайзия', 'MY', 0, 'MYS', 458, 'MY.png', '127', 'en', '+60', 1, 1000),
(128, 'Maldives', 'Мальдивские Острова', 'MV', 0, 'MDV', 462, 'MV.png', '128', 'en', '+960', 1, 1000),
(129, 'Mali', 'Мали', 'ML', 0, 'MLI', 466, 'ML.png', '23', 'en', '+223', 1, 1000),
(130, 'Malta', 'Мальта', 'MT', 0, 'MLT', 470, 'MT.png', '130', 'en', '+356', 1, 1000),
(131, 'Marshall Islands', 'Маршалловы острова\r\n', 'MH', 0, 'MHL', 584, 'MH.png', '4', 'en', '+692', 1, 1000),
(132, 'Martinique', 'Мартиника', 'MQ', 0, 'MTQ', 474, 'MQ.png', '5', 'en', '+596', 1, 1000),
(133, 'Mauritania', 'Мавритания', 'MR', 0, 'MRT', 478, 'MR.png', '133', 'en', '+222', 1, 1000),
(134, 'Mauritius', 'Маврикий', 'MU', 0, 'MUS', 480, 'MU.png', '134', 'en', '+230', 1, 1000),
(135, 'Mayotte', 'Майотта', 'YT', 0, 'MYT', 175, 'YT.png', '5', 'en', '+262', 1, 1000),
(136, 'Mexico', 'Мексика', 'MX', 0, 'MEX', 484, 'MX.png', '136', 'en', '+52', 1, 1000),
(137, 'Micronesia', 'Микронезия', 'FM', 0, 'FSM', 583, 'FM.png', '4', 'en', '+691', 1, 1000),
(138, 'Moldova', 'Молдова', 'MD', 0, 'MDA', 498, 'MD.png', '138', 'en', '+373', 1, 1000),
(139, 'Monaco', 'Монако', 'MC', 0, 'MCO', 492, 'MC.png', '5', 'en', '+377', 1, 1000),
(140, 'Mongolia', 'Монголия', 'MN', 0, 'MNG', 496, 'MN.png', '140', 'en', '+376', 1, 1000),
(141, 'Montserrat', 'Монсеррат', 'MS', 0, 'MSR', 500, 'MS.png', '7', 'en', '+1664', 1, 1000),
(142, 'Morocco', 'Марокко', 'MA', 0, 'MAR', 504, 'MA.png', '142', 'en', '+212', 1, 1000),
(143, 'Mozambique', 'Мозамбик', 'MZ', 0, 'MOZ', 508, 'MZ.png', '143', 'en', '+258', 1, 1000),
(144, 'Myanmar', 'Мьянма', 'MM', 0, 'MMR', 104, 'MM.png', '144', 'en', '+95', 1, 1000),
(145, 'Namibia', 'Намибия', 'NA', 0, 'NAM', 516, 'NA.png', '145', 'en', '+264', 1, 1000),
(146, 'Nauru', 'Науру', 'NR', 0, 'NRU', 520, 'NR.png', '13', 'en', '+674', 1, 1000),
(147, 'Nepal', 'Непал', 'NP', 0, 'NPL', 524, 'NP.png', '147', 'en', '+977', 1, 1000),
(148, 'Netherlands', 'Нидерланды', 'NL', 0, 'NLD', 528, 'NL.png', '5', 'en', '+31', 1, 1000),
(149, 'Netherlands Antilles', 'Нидерландские Антильские острова\r\n', 'AN', 0, 'ANT', 530, 'AN.png', '149', 'en', '+599', 1, 1000),
(150, 'New Caledonia', 'Новая Каледония', 'NC', 0, 'NCL', 540, 'NC.png', '75', 'en', '+687', 1, 1000),
(151, 'New Zealand', 'Новая Зеландия', 'NZ', 0, 'NZL', 554, 'NZ.png', '50', 'en', '+64', 1, 1000),
(152, 'Nicaragua', 'Никарагуа', 'NI', 0, 'NIC', 558, 'NI.png', '152', 'en', '+505', 1, 1000),
(153, 'Niger', 'Нигер', 'NE', 0, 'NER', 562, 'NE.png', '23', 'en', '+227', 1, 1000),
(154, 'Nigeria', 'Нигерия', 'NG', 0, 'NGA', 566, 'NG.png', '154', 'en', '+234', 1, 1000),
(156, 'Norfolk Island', 'Остров Норфолк', 'NF', 0, 'NFK', 574, 'NF.png', '13', 'en', '+672', 1, 1000),
(157, 'North Korea', 'Северная Корея', 'KP', 0, 'PRK', 408, 'KP.png', '157', 'en', '+850', 1, 1000),
(158, 'Northern Mariana Islands', 'Северные Марианские острова\r\n', 'MP', 0, 'MNP', 580, 'MP.png', '4', 'en', '+1670', 1, 1000),
(159, 'Norway', 'Норвегия', 'NO', 0, 'NOR', 578, 'NO.png', '29', 'en', '+47', 1, 1000),
(160, 'Oman', 'Оман', 'OM', 0, 'OMN', 512, 'OM.png', '160', 'en', '+968', 1, 1000),
(161, 'Pakistan', 'Пакистан', 'PK', 0, 'PAK', 586, 'PK.png', '161', 'en', '+92', 1, 1000),
(162, 'Palau', 'Палау', 'PW', 0, 'PLW', 585, 'PW.png', '4', 'en', '+680', 1, 1000),
(163, 'Palestinian Territory', 'Палестинская территория', 'PS', 0, 'PSE', 275, 'PS.png', '103', 'en', '+970', 1, 1000),
(164, 'Panama', 'Панама', 'PA', 0, 'PAN', 591, 'PA.png', '164', 'en', '+507', 1, 1000),
(165, 'Papua New Guinea', 'Папуа-Новая Гвинея', 'PG', 0, 'PNG', 598, 'PG.png', '165', 'en', '+675', 1, 1000),
(166, 'Paraguay', 'Парагвай', 'PY', 0, 'PRY', 600, 'PY.png', '166', 'en', '+595', 1, 1000),
(167, 'Peru', 'Перу', 'PE', 0, 'PER', 604, 'PE.png', '167', 'en', '+51', 1, 1000),
(168, 'Philippines', 'Филиппины', 'PH', 0, 'PHL', 608, 'PH.png', '168', 'en', '+63', 1, 1000),
(169, 'Pitcairn', 'Питкэрн', 'PN', 0, 'PCN', 612, 'PN.png', '50', 'en', '+64', 1, 1000),
(170, 'Poland', 'Польша', 'PL', 0, 'POL', 616, 'PL.png', '170', 'en', '+48', 1, 1000),
(171, 'Portugal', 'Португалия', 'PT', 0, 'PRT', 620, 'PT.png', '5', 'en', '+351', 1, 1000),
(172, 'Puerto Rico', 'Пуэрто-Рико', 'PR', 0, 'PRI', 630, 'PR.png', '4', 'en', '+787', 1, 1000),
(173, 'Qatar', 'Катар', 'QA', 0, 'QAT', 634, 'QA.png', '173', 'en', '+974', 1, 1000),
(174, 'Republic of the Congo', 'Республика Конго', 'CG', 0, 'COG', 178, 'CG.png', '38', 'en', '+242', 1, 1000),
(176, 'Romania', 'Румыния', 'RO', 0, 'ROU', 642, 'RO.png', '176', 'en', '+40', 1, 1000),
(177, 'Russia', 'Россия', 'RU', 1, 'RUS', 643, 'RU.png', '177', 'ru', '+7', 10, 1000),
(178, 'Rwanda', 'Руанда', 'RW', 0, 'RWA', 646, 'RW.png', '178', 'en', '+250', 1, 1000),
(179, 'Saint Helena', 'Остров Святой Елены', 'SH', 0, 'SHN', 654, 'SH.png', '179', 'en', '+290', 1, 1000),
(180, 'Saint Kitts and Nevis', 'Сент-Китс и Невис', 'KN', 0, 'KNA', 659, 'KN.png', '7', 'en', '+1869', 1, 1000),
(181, 'Saint Lucia', 'Сент-Люсия', 'LC', 0, 'LCA', 662, 'LC.png', '7', 'en', '+1758', 1, 1000),
(182, 'Saint Pierre and Miquelon', 'Сен-Пьер и Микелон', 'PM', 0, 'SPM', 666, 'PM.png', '5', 'en', '+508', 1, 1000),
(183, 'Saint Vincent and the Grenadines', 'Сент-Винсент и Гренадины', 'VC', 0, 'VCT', 670, 'VC.png', '7', 'en', '+1784', 1, 1000),
(184, 'Samoa', 'Самоа', 'WS', 0, 'WSM', 882, 'WS.png', '184', 'en', '+685', 1, 1000),
(185, 'San Marino', 'Сан-Марино', 'SM', 0, 'SMR', 674, 'SM.png', '5', 'en', '+378', 1, 1000),
(186, 'Sao Tome and Principe', 'Сан-Томе и Принсипи', 'ST', 0, 'STP', 678, 'ST.png', '186', 'en', '+239', 1, 1000),
(187, 'Saudi Arabia', 'Саудовская Аравия', 'SA', 0, 'SAU', 682, 'SA.png', '187', 'en', '+966', 1, 1000),
(188, 'Senegal', 'Сенегал', 'SN', 0, 'SEN', 686, 'SN.png', '23', 'en', '+221', 1, 1000),
(189, 'Serbia and Montenegro', 'Сербия и Черногория', 'CS', 0, 'SCG', 891, 'CS.png', '189', 'en', '+381', 1, 1000),
(190, 'Seychelles', 'Сейшельские Острова', 'SC', 0, 'SYC', 690, 'SC.png', '190', 'en', '+248', 1, 1000),
(191, 'Sierra Leone', 'Сьерра-Леоне', 'SL', 0, 'SLE', 694, 'SL.png', '191', 'en', '+232', 1, 1000),
(192, 'Singapore', 'Сингапур', 'SG', 0, 'SGP', 702, 'SG.png', '192', 'en', '+65', 1, 1000),
(193, 'Slovakia', 'Словакия', 'SK', 0, 'SVK', 703, 'SK.png', '193', 'en', '+421', 1, 1000),
(194, 'Slovenia', 'Словения', 'SI', 0, 'SVN', 705, 'SI.png', '5', 'en', '+386', 1, 1000),
(195, 'Solomon Islands', 'Соломоновы Острова', 'SB', 0, 'SLB', 90, 'SB.png', '195', 'en', '+677', 1, 1000),
(196, 'Somalia', 'Сомали', 'SO', 0, 'SOM', 706, 'SO.png', '196', 'en', '+252', 1, 1000),
(197, 'South Africa', 'Южная Африка', 'ZA', 0, 'ZAF', 710, 'ZA.png', '197', 'en', '+27', 1, 1000),
(198, 'South Georgia and the South Sandwich Islands', 'Южная Георгия и Южные Сандвичевы Острова', 'GS', 0, 'SGS', 239, 'GS.png', '198', 'en', '+500', 1, 1000),
(199, 'South Korea', 'Южная Корея', 'KR', 0, 'KOR', 410, 'KR.png', '199', 'en', '+82', 1, 1000),
(200, 'Spain', 'Испания', 'ES', 0, 'ESP', 724, 'ES.png', '5', 'en', '+34', 1, 1000),
(201, 'Sri Lanka', 'Шри-Ланка', 'LK', 0, 'LKA', 144, 'LK.png', '201', 'en', '+94', 1, 1000),
(202, 'Sudan', 'Судан', 'SD', 0, 'SDN', 736, 'SD.png', '202', 'en', '+249', 1, 1000),
(203, 'Suriname', 'Суринам', 'SR', 0, 'SUR', 740, 'SR.png', '203', 'en', '+597', 1, 1000),
(204, 'Svalbard and Jan Mayen', 'Шпицберген и Ян-Майен', 'SJ', 0, 'SJM', 744, 'SJ.png', '29', 'en', '+47', 1, 1000),
(205, 'Swaziland', 'Свазиленд', 'SZ', 0, 'SWZ', 748, 'SZ.png', '205', 'en', '+268', 1, 1000),
(206, 'Sweden', 'Швеция', 'SE', 0, 'SWE', 752, 'SE.png', '206', 'en', '+46', 1, 1000),
(207, 'Switzerland', 'Швейцария', 'CH', 0, 'CHE', 756, 'CH.png', '120', 'en', '+41', 1, 1000),
(208, 'Syria', 'Сирия', 'SY', 1, 'SYR', 760, 'SY.png', '208', 'ar', '+963', 9, 1),
(209, 'Taiwan', 'Тайвань', 'TW', 0, 'TWN', 158, 'TW.png', '209', 'en', '+886', 1, 1000),
(210, 'Tajikistan', 'Таджикистан', 'TJ', 0, 'TJK', 762, 'TJ.png', '210', 'en', '+992', 1, 1000),
(211, 'Tanzania', 'Танзания', 'TZ', 0, 'TZA', 834, 'TZ.png', '211', 'en', '+255', 1, 1000),
(212, 'Thailand', 'Таиланд', 'TH', 0, 'THA', 764, 'TH.png', '212', 'en', '+66', 1, 1000),
(213, 'Togo', 'Того', 'TG', 0, 'TGO', 768, 'TG.png', '23', 'en', '+228', 1, 1000),
(214, 'Tokelau', 'Токелау', 'TK', 0, 'TKL', 772, 'TK.png', '50', 'en', '+690', 1, 1000),
(215, 'Tonga', 'Тонга', 'TO', 0, 'TON', 776, 'TO.png', '215', 'en', '+676', 1, 1000),
(216, 'Trinidad and Tobago', 'Тринидад и Тобаго', 'TT', 0, 'TTO', 780, 'TT.png', '216', 'en', '+1868', 1, 1000),
(217, 'Tunisia', 'Тунис', 'TN', 0, 'TUN', 788, 'TN.png', '217', 'en', '+216', 1, 1000),
(218, 'Turkey', 'Турция', 'TR', 0, 'TUR', 792, 'TR.png', '218', 'en', '+90', 1, 1000),
(219, 'Turkmenistan', 'Туркменистан', 'TM', 0, 'TKM', 795, 'TM.png', '219', 'en', '+993', 1, 1000),
(220, 'Turks and Caicos Islands', 'Теркс и Кайкос', 'TC', 0, 'TCA', 796, 'TC.png', '4', 'en', '+1649', 1, 1000),
(221, 'Tuvalu', 'Тувалу', 'TV', 0, 'TUV', 798, 'TV.png', '13', 'en', '+688', 1, 1000),
(222, 'U.S. Virgin Islands', 'Виргинские Острова (США)', 'VI', 0, 'VIR', 850, 'VI.png', '4', 'en', '+1340', 1, 1000),
(223, 'Uganda', 'Уганда', 'UG', 0, 'UGA', 800, 'UG.png', '223', 'en', '+256', 1, 1000),
(224, 'Ukraine', 'Украина', 'UA', 0, 'UKR', 804, 'UA.png', '224', 'en', '+380', 1, 1000),
(225, 'United Arab Emirates', 'Объединённые Арабские Эмираты', 'AE', 0, 'ARE', 784, 'AE.png', '225', 'en', '+971', 1, 1000),
(226, 'United Kingdom', 'Великобритания', 'GB', 0, 'GBR', 826, 'GB.png', '198', 'en', '+44', 1, 1000),
(227, 'United States', 'Соединенные Штаты Америки', 'US', 0, 'USA', 840, 'US.png', '4', 'en', '+1', 1, 1000),
(228, 'United States Minor Outlying Islands', 'Внешние малые острова США', 'UM', 0, 'UMI', 581, 'UM.png', '4', 'en', '+1', 1, 1000),
(229, 'Uruguay', 'Уругвай', 'UY', 0, 'URY', 858, 'UY.png', '229', 'en', '+598', 1, 1000),
(230, 'Uzbekistan', 'Узбекистан', 'UZ', 0, 'UZB', 860, 'UZ.png', '230', 'en', '+998', 1, 1000),
(231, 'Vanuatu', 'Вануату', 'VU', 0, 'VUT', 548, 'VU.png', '231', 'en', '+678', 1, 1000),
(232, 'Vatican', 'Ватикан', 'VA', 0, 'VAT', 336, 'VA.png', '5', 'en', '+379', 1, 1000),
(233, 'Venezuela', 'Венесуэла', 'VE', 0, 'VEN', 862, 'VE.png', '233', 'en', '+58', 1, 1000),
(234, 'Vietnam', 'Вьетнам', 'VN', 0, 'VNM', 704, 'VN.png', '234', 'en', '+84', 1, 1000),
(235, 'Wallis and Futuna', 'Уоллис и Футуна', 'WF', 0, 'WLF', 876, 'WF.png', '75', 'en', '+681', 1, 1000),
(236, 'Western Sahara', 'Западная Сахара', 'EH', 0, 'ESH', 732, 'EH.png', '142', 'en', '+212', 1, 1000),
(237, 'Yemen', 'Йемен', 'YE', 0, 'YEM', 887, 'YE.png', '237', 'en', '+967', 1, 1000),
(238, 'Zambia', 'Замбия', 'ZM', 0, 'ZMB', 894, 'ZM.png', '238', 'en', '+260', 1, 1000),
(239, 'Zimbabwe', 'Зимбабве', 'ZW', 0, 'ZWE', 716, 'ZW.png', '239', 'en', '+263', 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `ard_currency`
--

CREATE TABLE `ard_currency` (
  `idCurrency` int(3) UNSIGNED NOT NULL,
  `currencyCode` char(3) DEFAULT NULL,
  `currencyName` varchar(32) DEFAULT NULL,
  `currencySymbol` varchar(3) DEFAULT NULL,
  `useProject` int(11) NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_currency`
--

INSERT INTO `ard_currency` (`idCurrency`, `currencyCode`, `currencyName`, `currencySymbol`, `useProject`, `ordering`) VALUES
(1, 'AFN', 'Afghani', '؋', 0, 1),
(2, 'ALL', 'Lek', 'Lek', 0, 1),
(3, 'DZD', 'Dinar', NULL, 0, 1),
(4, 'USD', 'Dollar USD', '$', 1, 1),
(5, 'EUR', 'Euro', '€', 0, 1),
(6, 'AOA', 'Kwanza', 'Kz', 0, 1),
(7, 'XCD', 'Dollar', '$', 0, 1),
(10, 'ARS', 'Peso', '$', 0, 1),
(11, 'AMD', 'Dram AMD', '֏', 1, 1),
(12, 'AWG', 'Guilder', 'ƒ', 0, 1),
(13, 'AUD', 'Dollar', '$', 0, 1),
(16, 'BSD', 'Dollar', '$', 0, 1),
(17, 'BHD', 'Dinar', NULL, 0, 1),
(18, 'BDT', 'Taka', NULL, 0, 1),
(19, 'BBD', 'Dollar', '$', 0, 1),
(20, 'BYR', 'Ruble', 'p.', 0, 1),
(22, 'BZD', 'Dollar', 'BZ$', 0, 1),
(23, 'XOF', 'Franc', NULL, 0, 1),
(24, 'BMD', 'Dollar', '$', 0, 1),
(25, 'BTN', 'Ngultrum', NULL, 0, 1),
(26, 'BOB', 'Boliviano', '$b', 0, 1),
(27, 'BAM', 'Marka', 'KM', 0, 1),
(28, 'BWP', 'Pula', 'P', 0, 1),
(29, 'NOK', 'Krone', 'kr', 0, 1),
(30, 'BRL', 'Real', 'R$', 0, 1),
(33, 'BND', 'Dollar', '$', 0, 1),
(34, 'BGN', 'Lev', 'лв', 0, 1),
(36, 'BIF', 'Franc', NULL, 0, 1),
(37, 'KHR', 'Riels', '៛', 0, 1),
(38, 'XAF', 'Franc', 'FCF', 0, 1),
(39, 'CAD', 'Dollar', '$', 0, 1),
(40, 'CVE', 'Escudo', NULL, 0, 1),
(41, 'KYD', 'Dollar', '$', 0, 1),
(44, 'CLP', 'Peso', NULL, 0, 1),
(45, 'CNY', 'Yuan Renminbi', '¥', 0, 1),
(48, 'COP', 'Peso', '$', 0, 1),
(49, 'KMF', 'Franc', NULL, 0, 1),
(50, 'NZD', 'Dollar', '$', 0, 1),
(51, 'CRC', 'Colon', '₡', 0, 1),
(52, 'HRK', 'Kuna', 'kn', 0, 1),
(53, 'CUP', 'Peso', '₱', 0, 1),
(54, 'CYP', 'Pound', NULL, 0, 1),
(55, 'CZK', 'Koruna', 'Kč', 0, 1),
(56, 'CDF', 'Franc', NULL, 0, 1),
(57, 'DKK', 'Krone', 'kr', 0, 1),
(58, 'DJF', 'Franc', NULL, 0, 1),
(60, 'DOP', 'Peso', 'RD$', 0, 1),
(63, 'EGP', 'Pound', '£', 0, 1),
(64, 'SVC', 'Colone', '$', 0, 1),
(66, 'ERN', 'Nakfa', 'Nfk', 0, 1),
(67, 'EEK', 'Kroon', 'kr', 0, 1),
(68, 'ETB', 'Birr', NULL, 0, 1),
(69, 'FKP', 'Pound', '£', 0, 1),
(71, 'FJD', 'Dollar', '$', 0, 1),
(75, 'XPF', 'Franc', NULL, 0, 1),
(78, 'GMD', 'Dalasi', 'D', 0, 1),
(79, 'GEL', 'Lari', '₾', 1, 1),
(81, 'GHC', 'Cedi', '¢', 0, 1),
(82, 'GIP', 'Pound', '£', 0, 1),
(88, 'GTQ', 'Quetzal', 'Q', 0, 1),
(89, 'GNF', 'Franc', NULL, 0, 1),
(91, 'GYD', 'Dollar', '$', 0, 1),
(92, 'HTG', 'Gourde', 'G', 0, 1),
(94, 'HNL', 'Lempira', 'L', 0, 1),
(95, 'HKD', 'Dollar', '$', 0, 1),
(96, 'HUF', 'Forint', 'Ft', 0, 1),
(97, 'ISK', 'Krona', 'kr', 0, 1),
(98, 'INR', 'Rupee', '₹', 0, 1),
(99, 'IDR', 'Rupiah', 'Rp', 0, 1),
(100, 'IRR', 'Rial', '﷼', 0, 1),
(101, 'IQD', 'Dinar', NULL, 0, 1),
(103, 'ILS', 'Shekel', '₪', 0, 1),
(106, 'JMD', 'Dollar', '$', 0, 1),
(107, 'JPY', 'Yen', '¥', 0, 1),
(108, 'JOD', 'Dinar', NULL, 0, 1),
(109, 'KZT', 'Tenge', 'лв', 0, 1),
(110, 'KES', 'Shilling', NULL, 0, 1),
(112, 'KWD', 'Dinar', NULL, 0, 1),
(113, 'KGS', 'Som', 'лв', 0, 1),
(114, 'LAK', 'Kip', '₭', 0, 1),
(115, 'LVL', 'Lat', 'Ls', 0, 1),
(116, 'LBP', 'Pound', '£', 0, 1),
(117, 'LSL', 'Loti', 'L', 0, 1),
(118, 'LRD', 'Dollar', '$', 0, 1),
(119, 'LYD', 'Dinar', NULL, 0, 1),
(120, 'CHF', 'Franc', 'CHF', 0, 1),
(121, 'LTL', 'Litas', 'Lt', 0, 1),
(123, 'MOP', 'Pataca', 'MOP', 0, 1),
(124, 'MKD', 'Denar', 'ден', 0, 1),
(125, 'MGA', 'Ariary', NULL, 0, 1),
(126, 'MWK', 'Kwacha', 'MK', 0, 1),
(127, 'MYR', 'Ringgit', 'RM', 0, 1),
(128, 'MVR', 'Rufiyaa', 'Rf', 0, 1),
(130, 'MTL', 'Lira', NULL, 0, 1),
(133, 'MRO', 'Ouguiya', 'UM', 0, 1),
(134, 'MUR', 'Rupee', '₨', 0, 1),
(136, 'MXN', 'Peso', '$', 0, 1),
(138, 'MDL', 'Leu', NULL, 0, 1),
(140, 'MNT', 'Tugrik', '₮', 0, 1),
(142, 'MAD', 'Dirham', NULL, 0, 1),
(143, 'MZN', 'Meticail', 'MT', 0, 1),
(144, 'MMK', 'Kyat', 'K', 0, 1),
(145, 'NAD', 'Dollar', '$', 0, 1),
(147, 'NPR', 'Rupee', '₨', 0, 1),
(149, 'ANG', 'Guilder', 'ƒ', 0, 1),
(152, 'NIO', 'Cordoba', 'C$', 0, 1),
(154, 'NGN', 'Naira', '₦', 0, 1),
(157, 'KPW', 'Won', '₩', 0, 1),
(160, 'OMR', 'Rial', '﷼', 0, 1),
(161, 'PKR', 'Rupee', '₨', 0, 1),
(164, 'PAB', 'Balboa', 'B/.', 0, 1),
(165, 'PGK', 'Kina', NULL, 0, 1),
(166, 'PYG', 'Guarani', 'Gs', 0, 1),
(167, 'PEN', 'Sol', 'S/.', 0, 1),
(168, 'PHP', 'Peso', 'Php', 0, 1),
(170, 'PLN', 'Zloty', 'zł', 0, 1),
(173, 'QAR', 'Rial', '﷼', 0, 1),
(176, 'RON', 'Leu', 'lei', 0, 1),
(177, 'RUB', 'Ruble', '₽', 1, 1),
(178, 'RWF', 'Franc', NULL, 0, 1),
(179, 'SHP', 'Pound', '£', 0, 1),
(184, 'WST', 'Tala', 'WS$', 0, 1),
(186, 'STD', 'Dobra', 'Db', 0, 1),
(187, 'SAR', 'Rial', '﷼', 0, 1),
(189, 'RSD', 'Dinar', 'Дин', 0, 1),
(190, 'SCR', 'Rupee', '₨', 0, 1),
(191, 'SLL', 'Leone', 'Le', 0, 1),
(192, 'SGD', 'Dollar', '$', 0, 1),
(193, 'SKK', 'Koruna', 'Sk', 0, 1),
(195, 'SBD', 'Dollar', '$', 0, 1),
(196, 'SOS', 'Shilling', 'S', 0, 1),
(197, 'ZAR', 'Rand', 'R', 0, 1),
(198, 'GBP', 'Pound', '£', 0, 1),
(199, 'KRW', 'Won', '₩', 0, 1),
(201, 'LKR', 'Rupee', '₨', 0, 1),
(202, 'SDD', 'Dinar', NULL, 0, 1),
(203, 'SRD', 'Dollar', '$', 0, 1),
(205, 'SZL', 'Lilangeni', NULL, 0, 1),
(206, 'SEK', 'Krona', 'kr', 0, 1),
(208, 'SYP', 'Pound', '£', 0, 1),
(209, 'TWD', 'Dollar', 'NT$', 0, 1),
(210, 'TJS', 'Somoni', NULL, 0, 1),
(211, 'TZS', 'Shilling', NULL, 0, 1),
(212, 'THB', 'Baht', '฿', 0, 1),
(215, 'TOP', 'Pa\'anga', 'T$', 0, 1),
(216, 'TTD', 'Dollar', 'TT$', 0, 1),
(217, 'TND', 'Dinar', NULL, 0, 1),
(218, 'TRY', 'Lira', 'YTL', 0, 1),
(219, 'TMM', 'Manat', 'm', 0, 1),
(223, 'UGX', 'Shilling', NULL, 0, 1),
(224, 'UAH', 'Hryvnia', '₴', 0, 1),
(225, 'AED', 'Dirham', NULL, 0, 1),
(229, 'UYU', 'Peso', '$U', 0, 1),
(230, 'UZS', 'Som', 'лв', 0, 1),
(231, 'VUV', 'Vatu', 'Vt', 0, 1),
(233, 'VEF', 'Bolivar', 'Bs', 0, 1),
(234, 'VND', 'Dong', '₫', 0, 1),
(237, 'YER', 'Rial', '﷼', 0, 1),
(238, 'ZMK', 'Kwacha', 'ZK', 0, 1),
(239, 'ZWD', 'Dollar', 'Z$', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ard_packages`
--

CREATE TABLE `ard_packages` (
  `idPackage` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '1 active 2 inactive',
  `priceMonthly` double NOT NULL,
  `priceYearly` double NOT NULL,
  `deviceLimitCount` int(11) DEFAULT 0,
  `userLimitCount` int(11) NOT NULL DEFAULT 0,
  `useEmailNotify` int(1) NOT NULL DEFAULT 0,
  `useAnalytics` int(1) NOT NULL DEFAULT 0,
  `useApi` int(1) NOT NULL DEFAULT 0,
  `employeeLimitCount` int(11) NOT NULL,
  `branchLimitCount` int(11) NOT NULL,
  `idCurrency` int(11) NOT NULL,
  `ordering` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_packages`
--

INSERT INTO `ard_packages` (`idPackage`, `name`, `status`, `priceMonthly`, `priceYearly`, `deviceLimitCount`, `userLimitCount`, `useEmailNotify`, `useAnalytics`, `useApi`, `employeeLimitCount`, `branchLimitCount`, `idCurrency`, `ordering`) VALUES
(1, 'Micro', 1, 10000, 110000, 10, 10, 0, 0, 0, 10, 2, 11, 1),
(2, 'Small', 1, 15000, 165000, 5, 5, 0, 0, 0, 50, 2, 11, 2),
(3, 'Standard', 1, 20000, 220000, 30, 30, 1, 0, 0, 1000, 10, 11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ard_roles`
--

CREATE TABLE `ard_roles` (
  `idRole` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createDate` timestamp NULL DEFAULT current_timestamp(),
  `updateDate` timestamp NULL DEFAULT NULL,
  `idKey` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_roles`
--

INSERT INTO `ard_roles` (`idRole`, `name`, `createDate`, `updateDate`, `idKey`) VALUES
(1, 'Super Admin', '2020-09-21 07:07:40', NULL, NULL),
(2, 'Admin', '2020-09-21 07:07:40', NULL, NULL),
(3, 'Operator', '2020-09-21 07:07:40', NULL, NULL),
(4, 'Driver', '2020-09-21 07:07:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ard_users`
--

CREATE TABLE `ard_users` (
  `idUser` int(11) NOT NULL,
  `idKey` varchar(50) DEFAULT NULL,
  `idClient` int(11) NOT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phoneCode` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `idRole` int(2) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 active 2 inactive',
  `idPolicy` int(11) DEFAULT NULL,
  `isEmailConfirmed` int(1) NOT NULL DEFAULT 0 COMMENT '1 yes 0 no',
  `token` varchar(255) DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `updateDate` timestamp NULL DEFAULT NULL,
  `isOnline` int(1) DEFAULT 1,
  `lang` varchar(2) DEFAULT 'en',
  `direction` varchar(3) NOT NULL DEFAULT 'ltr',
  `icon` varchar(255) DEFAULT NULL,
  `idBranch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_users`
--

INSERT INTO `ard_users` (`idUser`, `idKey`, `idClient`, `fullName`, `email`, `phoneCode`, `phoneNumber`, `login`, `password`, `idRole`, `status`, `idPolicy`, `isEmailConfirmed`, `token`, `createDate`, `updateDate`, `isOnline`, `lang`, `direction`, `icon`, `idBranch`) VALUES
(54, '47bef1b08b89fbb256b57f44fcab366e', 60, 'Khatchig Nazarian', 'nazariank@yahoo.com', '+374', '94775309', 'nazariank@yahoo.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1, 1, 1, '49e852b60dfc74c9205167701224bb74', '2021-04-12 21:39:44', '2021-05-25 23:32:34', 1, 'ar', 'ltr', NULL, 1),
(72, 'd3c8f73fe015a9fcba1458ab608c67aa', 60, 'mano', 'vipmano55@gmail.com', '+374', '55228582', 'vipmano55@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 2, 1, 1, 1, NULL, '2021-05-22 22:19:56', '2021-05-22 22:20:46', 1, 'ar', 'ltr', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ard_users_token`
--

CREATE TABLE `ard_users_token` (
  `idToken` int(11) NOT NULL,
  `idKey` varchar(50) DEFAULT NULL,
  `device` varchar(10) DEFAULT NULL,
  `deviceKey` varchar(255) DEFAULT NULL COMMENT 'macAddress',
  `accessToken` varchar(32) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `expireDate` timestamp NULL DEFAULT NULL,
  `updateDate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ard_users_token`
--

INSERT INTO `ard_users_token` (`idToken`, `idKey`, `device`, `deviceKey`, `accessToken`, `idUser`, `idClient`, `createDate`, `expireDate`, `updateDate`) VALUES
(281, '409395aeb5447f33a80484fe02ba4f7c', 'web', NULL, 'c7972966c674fa774c372242ff8bbd60', 72, 60, '2021-05-24 21:45:23', '2021-06-01 00:57:23', '2021-05-25 00:57:23'),
(307, '294fdc30409a67346bbb30c57ef9a5fb', 'web', NULL, 'b75b15befbf0b8d0832cb98e61b36dad', 54, 60, '2021-06-07 20:05:59', '2021-06-14 21:06:49', '2021-06-07 21:06:49');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ard_users`
-- (See below for the actual view)
--
CREATE TABLE `view_ard_users` (
`clientIdKey` varchar(50)
,`idUser` int(11)
,`idKey` varchar(50)
,`idClient` int(11)
,`fullName` varchar(255)
,`login` varchar(255)
,`email` varchar(255)
,`phoneCode` varchar(255)
,`phoneNumber` varchar(255)
,`idRole` int(2)
,`icon` varchar(255)
,`status` int(1)
,`idPolicy` int(11)
,`lang` varchar(2)
,`idBranch` int(11)
,`direction` varchar(3)
,`roleName` varchar(255)
,`clientStatus` int(3)
,`clientName` varchar(255)
,`clientLegalName` varchar(255)
,`clientPhoneCode` varchar(255)
,`clientPhoneNumber` varchar(255)
,`clientTin` varchar(255)
,`clientEmail` varchar(255)
,`clientIcon` varchar(255)
,`clientPackage` int(11)
,`clientidCountry` int(11)
,`clientidCurrency` int(11)
,`clientCountryName` varchar(200)
,`clientCurrencyCode` char(3)
,`clientCurrencySymbol` varchar(3)
,`clientPackageName` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `view_ard_users`
--
DROP TABLE IF EXISTS `view_ard_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ard_users`  AS SELECT `c`.`idKey` AS `clientIdKey`, `u`.`idUser` AS `idUser`, `u`.`idKey` AS `idKey`, `u`.`idClient` AS `idClient`, `u`.`fullName` AS `fullName`, `u`.`login` AS `login`, `u`.`email` AS `email`, `u`.`phoneCode` AS `phoneCode`, `u`.`phoneNumber` AS `phoneNumber`, `u`.`idRole` AS `idRole`, `u`.`icon` AS `icon`, `u`.`status` AS `status`, `u`.`idPolicy` AS `idPolicy`, `u`.`lang` AS `lang`, `u`.`idBranch` AS `idBranch`, `u`.`direction` AS `direction`, `r`.`name` AS `roleName`, `c`.`status` AS `clientStatus`, `c`.`name` AS `clientName`, `c`.`nameLegal` AS `clientLegalName`, `c`.`phoneCode` AS `clientPhoneCode`, `c`.`phoneNumber` AS `clientPhoneNumber`, `c`.`tin` AS `clientTin`, `c`.`email` AS `clientEmail`, `c`.`icon` AS `clientIcon`, `c`.`idPackage` AS `clientPackage`, `c`.`idCountry` AS `clientidCountry`, `c`.`idCurrency` AS `clientidCurrency`, `cou`.`countryName` AS `clientCountryName`, `cur`.`currencyCode` AS `clientCurrencyCode`, `cur`.`currencySymbol` AS `clientCurrencySymbol`, `p`.`name` AS `clientPackageName` FROM (((((`ard_users` `u` left join `ard_clients` `c` on(`u`.`idClient` = `c`.`idClient`)) left join `ard_packages` `p` on(`p`.`idPackage` = `c`.`idPackage`)) left join `ard_countries` `cou` on(`cou`.`idCountry` = `c`.`idCountry`)) left join `ard_currency` `cur` on(`cur`.`idCurrency` = `cou`.`idCurrency`)) left join `ard_roles` `r` on(`r`.`idRole` = `u`.`idRole`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ard_clients`
--
ALTER TABLE `ard_clients`
  ADD PRIMARY KEY (`idClient`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `ard_countries`
--
ALTER TABLE `ard_countries`
  ADD PRIMARY KEY (`idCountry`);

--
-- Indexes for table `ard_currency`
--
ALTER TABLE `ard_currency`
  ADD PRIMARY KEY (`idCurrency`);

--
-- Indexes for table `ard_packages`
--
ALTER TABLE `ard_packages`
  ADD PRIMARY KEY (`idPackage`);

--
-- Indexes for table `ard_roles`
--
ALTER TABLE `ard_roles`
  ADD PRIMARY KEY (`idRole`);

--
-- Indexes for table `ard_users`
--
ALTER TABLE `ard_users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Indexes for table `ard_users_token`
--
ALTER TABLE `ard_users_token`
  ADD PRIMARY KEY (`idToken`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ard_clients`
--
ALTER TABLE `ard_clients`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `ard_countries`
--
ALTER TABLE `ard_countries`
  MODIFY `idCountry` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `ard_currency`
--
ALTER TABLE `ard_currency`
  MODIFY `idCurrency` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `ard_packages`
--
ALTER TABLE `ard_packages`
  MODIFY `idPackage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ard_roles`
--
ALTER TABLE `ard_roles`
  MODIFY `idRole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ard_users`
--
ALTER TABLE `ard_users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `ard_users_token`
--
ALTER TABLE `ard_users_token`
  MODIFY `idToken` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
